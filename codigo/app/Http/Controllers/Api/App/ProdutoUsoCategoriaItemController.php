<?php

namespace App\Http\Controllers\Api\App;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\ProdutoUsoCategoriaItem;

class ProdutoUsoCategoriaItemController extends Controller
{  
    /**
     *
     * @return \Illuminate\Http\Response
     */
    public function buscar(Request $requisicao, $campo, $valor = null)
    {
        if (! in_array($campo, ['descricao'])) {
            return '';
        }
        
        
        if (! $requisicao->ajax()) {
            //return '';
        }
        
        if ($requisicao->has('q')) {
            $valor = $requisicao->input('q');
        }
        
        $busca = ProdutoUsoCategoriaItem::select(['id', 'descricao'])->orderBy('descricao', 'ASC')
            ->where($campo, 'like', "%$valor%")->get();
        
        $resultados = [
            'total' => $busca->count(),
            'itens' => $busca,
        ];

        return $resultados;
    }
}
