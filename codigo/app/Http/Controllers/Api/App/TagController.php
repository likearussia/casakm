<?php

namespace App\Http\Controllers\Api\App;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Tag;

class TagController extends Controller
{  
    /**
     *
     * @return \Illuminate\Http\Response
     */
    public function buscar(Request $requisicao, $campo, $valor = null)
    {
        if (! in_array($campo, ['tag'])) {
            return '';
        }
        
        
        if (! $requisicao->ajax()) {
            //return '';
        }
        
        if ($requisicao->has('q')) {
            $valor = $requisicao->input('q');
        }
        
        $busca = Tag::select(['id', 'tag'])->orderBy('tag', 'ASC')->where($campo, 'like', "%$valor%")->get();
        
        $resultados = [
            'total' => $busca->count(),
            'tags' => $busca,
        ];

        return $resultados;
    }
}
