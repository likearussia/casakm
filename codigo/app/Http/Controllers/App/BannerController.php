<?php

namespace App\Http\Controllers\App;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Banner;

class BannerController extends Controller
{
    private $menu_ativo = 'app::banner';
    
    protected function index()
    {
        $dados = [];
        
        $banners = Banner::orderBy('created_at', 'DESC')->paginate(10);
        
        $dados['menu_ativo'] = $this->menu_ativo;
        $dados['areas']      = Banner::$areas;
        $dados['banners']    = $banners;
        
        return view('app.banner.index', $dados);
    }
    
    protected function criar()
    {
        $dados = [];
        
        $dados['menu_ativo']        = $this->menu_ativo;
        $dados['banner']            = null;        
        $dados['areas']             = Banner::$areas;
        $dados['diretorio_imagens'] = Banner::$diretorio_imagens;
        
        return view('app.banner.criar', $dados);
    }
    
    protected function editar($id)
    {
        $dados = [];
        
        $dados['menu_ativo']        = $this->menu_ativo;
        $dados['banner']            = Banner::findOrFail($id);        
        $dados['areas']             = Banner::$areas;
        $dados['diretorio_imagens'] = Banner::$diretorio_imagens;
        
        return view('app.banner.editar', $dados);
    }
    
    protected function deletar($id)
    {
        $banner = Banner::findOrFail($id);
        $banner->delete();
        
        flash()->success('Publicação deletada com sucesso');
        
        return redirect()->back();
    }
    
     /**
     * 
     * @param Request $requisicao
     * @return type
     */
    protected function validar(Request &$requisicao)
    {
        // #TODO max = max_upload_size ?
        $this->validate($requisicao, [
            'banner' => 'mimes:jpg,jpeg|max:1024',
        ]);
    }


    /**
     * Insere ou atualiza um banner
     * 
     * Caso $banner seja informado, será uma atualização.
     * 
     * @param object $requisicao Illuminate\Http\Request
     * @param int $id do banner
     * @return redirect
     */
    protected function salvar(Request $requisicao, $id = null)
    {
        $this->validar($requisicao);
        
        if ($id) {
            $banner = Banner::findOrFail($id);
        } else {
            $banner = null;
        }
        
        $input = $requisicao->input();
        // Remove dados que não serão usados diretamente pelos métodos create e update do model Post
        unset($input['_token']);

        // Lida com as imagens
        // #TODO deletar imagens antigas e cujo processo de salvamento do banner falhou
        if ($requisicao->hasFile('banner')) {
            try {
                $banner_imagem = $requisicao->file('banner');
                $nome_banner = sha1(str_random('20')) . '.' . $banner_imagem->guessClientExtension();
                $requisicao->file('banner')->move(public_path(Banner::$diretorio_imagens), $nome_banner);
                $input['banner'] = $nome_banner;
            }
            catch (\Exception $e) {
                flash()->error('Erro: ' . $e->getMessage());
                return redirect()->back()->withInput();
            }
        }
        
        // Lida com checkboxes
        $input['link_nova_aba'] = ($requisicao->has('link_nova_aba') ? true : false);
        
        // Atualiza ou cria um banner
        if ($banner) {
            try {
                $banner->update($input);
                flash()->success('Banner atualizado com sucesso');
            } catch (QueryException $e) {
                $exception = $e->getPrevious();
                flash()->error('Erro ao atualizar o banner. Detalhes: ' . $exception->getMessage());
                return redirect()->back()->withInput();
            }
        } else {
            try {
                $banner = Banner::create($input);
                flash()->success('Banner criado com sucesso');
            } catch (QueryException $e) {
                $exception = $e->getPrevious();
                flash()->error('Erro ao cadastrar o banner. Detalhes: ' . $exception->getMessage());
                return redirect()->back()->withInput();
            }
        }
        
        return redirect(route('app::banner::index'));
    }
}
