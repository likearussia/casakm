<?php

namespace App\Http\Controllers\App;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Database\QueryException;
use App\Models\Download;

class DownloadController extends Controller
{
    private $menu_ativo = 'app::downloads';
    
    public function index()
    {
        $dados = [];
        
        $query = Download::orderBy('nome', 'ASC');
        
        $dados['menu_ativo']     = $this->menu_ativo;
        $dados['downloads']      = $query->paginate(10);
        $dados['categorias']     = Download::$categorias;
        $dados['path_arquivos']  = storage_path(Download::$diretorio_arquivos);
        
        return view('app.download.index', $dados);
    }
    
    protected function criar(Request $requisicao)
    {
        $dados = [];
        
        if ($requisicao->isMethod('post')) {
            $input = $requisicao->input();
            unset($input['_token']);
            $input['slug'] = $this->gerarSlug($input['nome']);
            
            // Lida com o arquivo
            // #TODO deletar arquivos antigos e cujo processo de salvamento falhou
            if ($requisicao->hasFile('arquivo')) {
                try {
                    $arquivo = $requisicao->file('arquivo');
                    $nome_arquivo = str_slug($arquivo->getClientOriginalName()) . '-' . str_random('5') . '.' .
                                        $arquivo->guessClientExtension();
                    $requisicao->file('arquivo')->move(storage_path(Download::$diretorio_arquivos), $nome_arquivo);
                    $input['arquivo'] = $nome_arquivo;
                }
                catch (\Exception $e) {
                    flash()->error('Erro: ' . $e->getMessage());
                    return redirect()->back()->withInput();
                }
            }
            
            // Lida com checkboxes
            $input['eh_restrito'] = ($requisicao->has('eh_restrito') ? true : false);
            
            Download::create($input);
            
            flash()->success('Download cadastrado com sucesso');
            return redirect(route('app::download::index'));
        }
        
        $dados['menu_ativo']     = $this->menu_ativo;
        $dados['categorias']     = Download::$categorias;
        $dados['maximo_post']    = $this->getTamanhoMaximoPost();
        $dados['maximo_arquivo'] = $this->getTamanhoMaximoArquivo();
        
        return view('app.download.criar', $dados);
    }
    
    protected function editar(Request $requisicao, $id)
    {
        $dados = [];
        
        $dados['download'] = Download::findOrFail($id);
        
        if ($requisicao->isMethod('post')) {
            $input = $requisicao->input();
            unset($input['_token']);
            $input['slug'] = $this->gerarSlug($input['nome'], $dados['download']);
            
            // Lida com o arquivo
            // #TODO deletar arquivos antigos e cujo processo de salvamento falhou
            if ($requisicao->hasFile('arquivo')) {
                try {
                    $arquivo = $requisicao->file('arquivo');
                    $nome_arquivo = str_slug($arquivo->getClientOriginalName()) . '-' . str_random('5') . '.' .
                                        $arquivo->guessClientExtension();
                    $requisicao->file('arquivo')->move(storage_path(Download::$diretorio_arquivos), $nome_arquivo);
                    $input['arquivo'] = $nome_arquivo;
                }
                catch (\Exception $e) {
                    flash()->error('Erro: ' . $e->getMessage());
                    return redirect()->back()->withInput();
                }
            }
            
            // Lida com checkboxes
            $input['eh_restrito'] = ($requisicao->has('eh_restrito') ? true : false);
        
            $dados['download']->update($input);
            
            flash()->success('Download editado com sucesso');
            return redirect(route('app::download::index'));
        }
        
        $dados['menu_ativo']     = $this->menu_ativo;
        $dados['categorias']     = Download::$categorias;
        $dados['maximo_post']    = $this->getTamanhoMaximoPost();
        $dados['maximo_arquivo'] = $this->getTamanhoMaximoArquivo();
        
        return view('app.download.editar', $dados);
    }
    
    protected function deletar($id)
    {
        $download = Download::findOrFail($id);
        $download->delete();
        
        flash()->success('Download deletado com sucesso');
        
        return redirect()->back();
    }
    
    protected function getTamanhoMaximoArquivo()
    {
        return ini_get('upload_max_filesize');
    }
    
    protected function getTamanhoMaximoPost()
    {
        return ini_get('post_max_size');
    }


    /**
     * Retorna uma slug única com no máximo 100 caracteres.
     * 
     * Em caso de atualização, informe a variável $download
     * 
     * @param string $nome
     * @param Post objeto post
     * @return string
     * @todo usar apenas um metodo global, ao inves de um em casa controller
     */
    protected function gerarSlug($nome = null, &$download = null)
    {
        if ($nome) {
            $slug_gerada = str_slug($nome, '-');
        } else {
            $slug_gerada = str_random(100);
        }
        $slug = substr($slug_gerada, 0, 100);
        
        $busca = Download::withTrashed()->ofSlug($slug)->first();
        if (! $busca ) {
            $ja_existe = false;
        } elseif ($download === null) {
            // É uma inserção e o registro já existe
            $ja_existe = true;
        } else {
            $ja_existe = ($busca->id === $download->id) ? false : true;
        }
            
        while($ja_existe === true) {
            $slug = substr(str_slug($nome, '-'), 0, 94) . '-' . str_random(5);
            
            $busca = Download::withTrashed()->ofSlug($slug)->first();
            if (! $busca ) {
                $ja_existe = false;
            } else {
                $ja_existe = ($busca->id === $download->id) ? false : true;
            }
        }
        
        return $slug;
    }
}
