<?php

namespace App\Http\Controllers\App;

use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    private $menu_ativo = 'app::inicio';
    
    public function index()
    {
        $dados = [];
        
        $dados['menu_ativo'] = $this->menu_ativo;
        
        return view('app.index', $dados);
    }
}
