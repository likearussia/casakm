<?php

namespace App\Http\Controllers\App;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Database\QueryException;
use App\Models\PostCategoria;

class PostCategoriaController extends Controller
{
    private $menu_ativo = 'app::blog';
    
    protected function index()
    {
        $dados = [];
        
        $categorias = PostCategoria::orderBy('created_at', 'DESC')->paginate(10);
        
        $dados['menu_ativo'] = $this->menu_ativo;
        $dados['categorias'] = $categorias;
        
        return view('app.blog.post.categoria.index', $dados);
    }
    
    protected function criar(Request $requisicao)
    {
        $dados = [];
        
        if ($requisicao->isMethod('post')) {
            $input = $requisicao->input();
            unset($input['_token']);
            $input['slug'] = $this->gerarSlug($input['nome']);
            
            PostCategoria::create($input);
            
            flash()->success('Categoria criada com sucesso');
            return redirect(route('app::blog::post::categoria::index'));
        }
        
        $dados['menu_ativo'] = $this->menu_ativo;
        
        return view('app.blog.post.categoria.criar', $dados);
    }
    
    protected function editar(Request $requisicao, $id)
    {
        $dados = [];
        
        $dados['post_categoria'] = PostCategoria::findOrFail($id);
        
        if ($requisicao->isMethod('post')) {
            $input = $requisicao->input();
            unset($input['_token']);
            $input['slug'] = $this->gerarSlug($input['nome'], $dados['post_categoria']);
            
            $dados['post_categoria']->update($input);
            
            flash()->success('Categoria editada com sucesso');
            return redirect(route('app::blog::post::categoria::index'));
        }
        
        $dados['menu_ativo'] = $this->menu_ativo;
        
        return view('app.blog.post.categoria.editar', $dados);
    }
    
    protected function deletar($id)
    {
        $categoria = PostCategoria::findOrFail($id);
        $categoria->delete();
        
        flash()->success('Categoria deletada com sucesso');
        
        return redirect()->back();
    }
    
    /**
     * Retorna uma slug única com no máximo 100 caracteres.
     * 
     * Em caso de atualização, informe a variável $post_categoria
     * 
     * @param string $nome
     * @param Post objeto post
     * @return string
     * @todo usar apenas um metodo global, ao inves de um em casa controller
     */
    protected function gerarSlug($nome = null, &$post_categoria = null)
    {
        if ($nome) {
            $slug_gerada = str_slug($nome, '-');
        } else {
            $slug_gerada = str_random(100);
        }
        $slug = substr($slug_gerada, 0, 100);
        
        $busca = PostCategoria::withTrashed()->ofSlug($slug)->first();
        if (! $busca ) {
            $ja_existe = false;
        } elseif ($post_categoria === null) {
            // É uma inserção e o registro já existe
            $ja_existe = true;
        } else {
            $ja_existe = ($busca->id === $post_categoria->id) ? false : true;
        }
            
        while($ja_existe === true) {
            $slug = substr(str_slug($nome, '-'), 0, 94) . '-' . str_random(5);
            
            $busca = PostCategoria::withTrashed()->ofSlug($slug)->first();
            if (! $busca ) {
                $ja_existe = false;
            } else {
                $ja_existe = ($busca->id === $post_categoria->id) ? false : true;
            }
        }
        
        return $slug;
    }
}
