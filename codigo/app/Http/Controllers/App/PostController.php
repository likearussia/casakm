<?php

namespace App\Http\Controllers\App;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Database\QueryException;
use App\Models\Post;
use App\Models\PostTag;
use App\Models\PostCategoria;
use Image;
use File;
class PostController extends Controller
{
    private $menu_ativo = 'app::blog';
    
    protected function index()
    {
        $dados = [];
        
        $posts = Post::orderBy('created_at', 'DESC')->paginate(10);
        
        $dados['menu_ativo'] = $this->menu_ativo;
        $dados['posts']      = $posts;
        
        return view('app.blog.post.index', $dados);
    }
    
    protected function criar(Request $requisicao)
    {
        $dados = [];
        
        if ($requisicao->isMethod('post')) {
            //dd($requisicao->all());
            $validar = $this->validar($requisicao);
            if ($validar !== true) {
                return $validar;
            }
            $salvar = $this->salvar($requisicao);
            if ($salvar !== true) {
                return $salvar;
            }
            flash()->success('Post criado com sucesso');
            return redirect(route('app::blog::post::index'));
        }
        
        $dados['menu_ativo']        = $this->menu_ativo;
        $dados['diretorio_imagens'] = Post::$diretorio_imagens;
        $dados['tags']              = [];
        $dados['categorias']        = PostCategoria::listar();
        
        return view('app.blog.post.criar', $dados);
    }
    
    protected function editar(Request $requisicao, $id)
    {
        $dados = [];
        
        $dados['post'] = Post::findOrFail($id);
        
        if ($requisicao->isMethod('post')) {
            //dd($requisicao->all());
            $validar = $this->validar($requisicao);
            if ($validar !== true) {
                return $validar;
            }
            $salvar = $this->salvar($requisicao, $dados['post']);
            if ($salvar !== true) {
                return $salvar;
            }
            flash()->success('Post atualizado com sucesso');
            return redirect(route('app::blog::post::index'));
        }
        
        $dados['menu_ativo']        = $this->menu_ativo;
        $dados['diretorio_imagens'] = Post::$diretorio_imagens;
        $dados['tags']              = PostTag::listarTags($id);
        $dados['categorias']        = PostCategoria::listar();
        
        return view('app.blog.post.editar', $dados);
    }
    
    protected function deletar($id)
    {
        $post = Post::findOrFail($id);
        $post->delete();
        
        flash()->success('Publicação deletada com sucesso');
        
        return redirect()->back();
    }
    
    /**
     * 
     * @param Request $requisicao
     * @return type
     */
    protected function validar(Request &$requisicao)
    {
        // Imagens
        // #FIXME não funciona
        // #TODO max = max_upload_size ?
        $imagens = [
            'imagem_destaque' => $requisicao->file('imagem'),
        ];
        $validacao = Validator::make($imagens, [
            'imagem_destaque' => 'mimes:png|size:1024',
        ]);
        
        if ($validacao->fails()) {
            return redirect()
                    ->back()
                    ->withErrors($validacao)
                    ->withInput();
        }
        
        return true;
    }


    /**
     * Insere ou atualiza um post.
     * 
     * Caso $post seja informado, será uma atualização.
     * 
     * @param object $dados Illuminate\Http\Request
     * @param object $post objeto da classe App\Models\Post
     * @return mixed retornará true em caso de sucesso ou redirect()->back() em caso de erro
     */
    protected function salvar(Request &$requisicao, $post = null)
    {
        $input = $requisicao->input();
        // Remove dados que não serão usados diretamente pelos métodos create e update do model Post
        unset($input['_token']);
        unset($input['tags']);

        // Lida com as imagens
        // #TODO deletar imagens antigas e cujo processo de salvamento do post falhou
        if ($requisicao->hasFile('imagem_destaque')) {
            try {
                $imagem = $requisicao->file('imagem_destaque');
                $nome_imagem = sha1(str_random('20')) . '.' . $imagem->guessClientExtension();
                $requisicao->file('imagem_destaque')->move(public_path(Post::$diretorio_imagens), $nome_imagem);
                $input['imagem_destaque'] = $nome_imagem;
            }
            catch (\Exception $e) {
                flash()->error('Erro: ' . $e->getMessage());
                return redirect()->back()->withInput();
            }
        }
        
        $width = intval($input['wimage']);
        $height = intval($input['himage']);
        $x = intval($input['ximage']);
        $y = intval($input['yimage']);

        if (!empty($input['simage'])) {
            $finalName = str_replace("tmp_uploads/", "", $input['simage']);
            $img = Image::make(public_path() . '/' . $input['simage'])->crop($width, $height, $x, $y)->save(public_path() . '/miniaturas/' .$finalName);

            $input['miniatura'] = $finalName;
            #TODO - excluir umagem da pasta tmp 
            File::delete($input['simage']);
            unset($input['simage'],$input['himage'],$input['ximage'],$input['yimage'],$input['wimage']);
        
        }
        unset($input['simage'],$input['himage'],$input['ximage'],$input['yimage'],$input['wimage']);
        
        
        // Lida com checkboxes
        $input['permitir_comentarios'] = ($requisicao->has('permitir_comentarios') ? true : false);
        
        // Atualiza ou cria um post
        if ($post) {
            try {
                $input['slug'] = $this->gerarSlug($input['titulo'], $post);
                
                $post->update($input);
            } catch (QueryException $e) {
                $exception = $e->getPrevious();
                flash()->error('Erro ao atualizar o post. Detalhes: ' . $exception->getMessage());
                return redirect()->back()->withInput();
            }
        } else {
            try {
                $input['slug'] = $this->gerarSlug($input['titulo']);
                $post = Post::create($input);
            } catch (QueryException $e) {
                $exception = $e->getPrevious();
                flash()->error('Erro ao criar o post. Detalhes: ' . $exception->getMessage());
                return redirect()->back()->withInput();
            }
        }
        
        if (is_array($requisicao->input('tags'))) {
            PostTag::atualizar($post->id, array_values($requisicao->input('tags')));
        }
        
        return true;
    }
    
    /**
     * Retorna uma slug única com no máximo 100 caracteres.
     * 
     * Em caso de atualização, informe a variável $post
     * 
     * @param string $titulo
     * @param Post objeto post
     * @return string
     * @todo usar apenas um metodo global, ao inves de um em casa controller
     */
    protected function gerarSlug($titulo = null, &$post = null)
    {
        if ($titulo) {
            $slug_gerada = str_slug($titulo, '-');
        } else {
            $slug_gerada = str_random(100);
        }
        $slug = substr($slug_gerada, 0, 100);
        
        $busca = Post::withTrashed()->ofSlug($slug)->first();
        if (! $busca ) {
            $ja_existe = false;
        } elseif ($post === null) {
            // É uma inserção e o registro já existe
            $ja_existe = true;
        } else {
            $ja_existe = ($busca->id === $post->id) ? false : true;
        }
            
        while($ja_existe === true) {
            $slug = substr(str_slug($titulo, '-'), 0, 94) . '-' . str_random(5);
            
            $busca = Post::withTrashed()->ofSlug($slug)->first();
            if (! $busca ) {
                $ja_existe = false;
            } else {
                $ja_existe = ($busca->id === $post->id) ? false : true;
            }
        }
        
        return $slug;
    }
}
