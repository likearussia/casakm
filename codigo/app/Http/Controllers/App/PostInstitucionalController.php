<?php

namespace App\Http\Controllers\App;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Database\QueryException;
use App\Models\PostInstitucional;

class PostInstitucionalController extends Controller
{
    private $menu_ativo = 'app::institucional';
    
    protected function index()
    {
        $dados = [];
        
        $posts = PostInstitucional::orderBy('id')->paginate(10);
        
        $dados['menu_ativo'] = $this->menu_ativo;
        $dados['posts']      = $posts;
        
        return view('app.institucional.index', $dados);
    }
    
    protected function criar(Request $requisicao)
    {
        return '';
    }
    
    protected function editar(Request $requisicao, $id)
    {
        $dados = [];
        
        $dados['post'] = PostInstitucional::findOrFail($id);
        
        if ($requisicao->isMethod('post')) {
            $validar = $this->validar($requisicao);
            if ($validar !== true) {
                return $validar;
            }
            $salvar = $this->salvar($requisicao, $dados['post']);
            if ($salvar !== true) {
                return $salvar;
            }
            flash()->success('Publicação atualizada com sucesso');
            return redirect(route('app::institucional::index'));
        }
        
        $dados['menu_ativo']        = $this->menu_ativo;
        $dados['diretorio_imagens'] = PostInstitucional::$diretorio_imagens;
        
        return view('app.institucional.editar', $dados);
    }
    
    protected function deletar($id)
    {
        $post = PostInstitucional::findOrFail($id);
        $post->delete();
        
        flash()->success('Publicação deletada com sucesso');
        
        return redirect()->back();
    }
    
     /**
     * 
     * @param Request $requisicao
     * @return type
     */
    protected function validar(Request &$requisicao)
    {
        // Imagens
        // #TODO max = max_upload_size ?
        $imagens = [
            'banner' => $requisicao->file('banner'),
        ];
        $validacao = Validator::make($imagens, [
            'banner' => 'mimes:jpg,jpeg|max:3096',
        ]);
        
        if ($validacao->fails()) {
            return redirect()
                    ->back()
                    ->withErrors($validacao)
                    ->withInput();
        }
        
        return true;
    }


    /**
     * Insere ou atualiza uma publicação
     * 
     * Caso $post seja informado, será uma atualização.
     * 
     * @param object $dados Illuminate\Http\Request
     * @param object $post objeto da classe App\Models\PostInstitucional
     * @return mixed retornará true em caso de sucesso ou redirect()->back() em caso de erro
     */
    protected function salvar(Request &$requisicao, $post = null)
    {
        $input = $requisicao->input();
        // Remove dados que não serão usados diretamente pelos métodos create e update do model Post
        unset($input['_token']);

        // Lida com as imagens
        // #TODO deletar imagens antigas e cujo processo de salvamento do post falhou
        if ($requisicao->hasFile('banner')) {
            try {
                $banner = $requisicao->file('banner');
                $nome_banner = sha1(str_random('20')) . '.' . $banner->guessClientExtension();
                $requisicao->file('banner')->move(public_path(PostInstitucional::$diretorio_imagens), $nome_banner);
                $input['banner'] = $nome_banner;
            }
            catch (\Exception $e) {
                flash()->error('Erro: ' . $e->getMessage());
                return redirect()->back()->withInput();
            }
        }
        
        // Atualiza ou cria um post
        if ($post) {
            try {
                // Não irei atualizar slugs para manter o legado. #TODO mudar isso quando for possível criar posts
                //$input['slug'] = $this->gerarSlug($input['titulo'], $post);
                $post->update($input);
            } catch (QueryException $e) {
                $exception = $e->getPrevious();
                flash()->error('Erro ao atualizar a publicação. Detalhes: ' . $exception->getMessage());
                return redirect()->back()->withInput();
            }
        } else {
            try {
                $input['slug'] = $this->gerarSlug($input['titulo']);
                $post = PostInstitucional::create($input);
            } catch (QueryException $e) {
                $exception = $e->getPrevious();
                flash()->error('Erro ao criar a publicação. Detalhes: ' . $exception->getMessage());
                return redirect()->back()->withInput();
            }
        }
        
        return true;
    }
    
    /**
     * Retorna uma slug única com no máximo 100 caracteres.
     * 
     * Em caso de atualização, informe a variável $post
     * 
     * @param string $titulo
     * @param Post objeto post
     * @return string
     * @todo usar apenas um metodo global, ao inves de um em casa controller
     */
    protected function gerarSlug($titulo = null, &$post = null)
    {
        if ($titulo) {
            $slug_gerada = str_slug($titulo, '-');
        } else {
            $slug_gerada = str_random(100);
        }
        $slug = substr($slug_gerada, 0, 100);
        
        $busca = PostInstitucional::withTrashed()->ofSlug($slug)->first();
        if (! $busca ) {
            $ja_existe = false;
        } elseif ($post === null) {
            // É uma inserção e o registro já existe
            $ja_existe = true;
        } else {
            $ja_existe = ($busca->id === $post->id) ? false : true;
        }
            
        while($ja_existe === true) {
            $slug = substr(str_slug($titulo, '-'), 0, 94) . '-' . str_random(5);
            
            $busca = PostInstitucional::withTrashed()->ofSlug($slug)->first();
            if (! $busca ) {
                $ja_existe = false;
            } else {
                $ja_existe = ($busca->id === $post->id) ? false : true;
            }
        }
        
        return $slug;
    }
}
