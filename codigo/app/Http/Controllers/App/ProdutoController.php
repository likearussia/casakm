<?php

namespace App\Http\Controllers\App;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Produto;
use App\Models\ProdutoMarca;
use App\Models\ProdutoUso;
use App\Models\ProdutoVersao;
use Validator;
use Illuminate\Database\QueryException;

class ProdutoController extends Controller
{
    private $menu_ativo = 'app::produto';
    
    protected function index()
    {
        $dados = [];
        
        $produtos = Produto::orderBy('nome', 'ASC')->paginate(10);
        
        $dados['menu_ativo'] = $this->menu_ativo;
        $dados['produtos']   = $produtos;
        
        return view('app.produto.index', $dados);
    }
    
    protected function criar(Request $requisicao)
    {
        $dados = [];
        
        if ($requisicao->isMethod('post')) {
            $salvar = $this->salvar($requisicao);
            if ($salvar !== true) {
                return $salvar;
            }
            flash()->success('Produto criado com sucesso');
            return redirect(route('app::produto::index'));
        }
        
        $dados['menu_ativo']                = $this->menu_ativo;
        $dados['produto_ordem_maxima']      = Produto::max('ordem') + 1;
        $dados['marcas']                    = ProdutoMarca::listar();
        $dados['situacoes']                 = Produto::$situacoes;
        $dados['usos']                      = [];
        $dados['versoes']                   = [];
        $dados['versao_maxima']             = ProdutoVersao::max('id');
        $dados['diretorio_imagens']         = Produto::$diretorio_imagens;
        $dados['diretorio_imagens_banners'] = Produto::$diretorio_imagens_banners;
        $dados['diretorio_imagens_packs']   = Produto::$diretorio_imagens_packs;
        $dados['diretorio_imagens_versoes'] = ProdutoVersao::$diretorio_imagens;
        
        return view('app.produto.criar', $dados);
    }
    
    protected function editar(Request $requisicao, $id)
    {
        $dados = [];
        
        $dados['produto'] = Produto::findOrFail($id);
        
        if ($requisicao->isMethod('post')) {
            $salvar = $this->salvar($requisicao, $dados['produto']);
            if ($salvar !== true) {
                return $salvar;
            }
            flash()->success('Produto atualizado com sucesso');
            return redirect(route('app::produto::index'));
        }
        
        $dados['menu_ativo']                = $this->menu_ativo;
        $dados['produto_ordem_maxima']      = Produto::max('ordem') + 1;
        $dados['marcas']                    = ProdutoMarca::listar();
        $dados['situacoes']                 = Produto::$situacoes;
        $dados['usos']                      = Produto::listarUsos($id);
        $dados['versoes']                   = ProdutoVersao::obterVersoes($id);
        $dados['versao_maxima']             = ProdutoVersao::max('id');
        $dados['diretorio_imagens']         = Produto::$diretorio_imagens;
        $dados['diretorio_imagens_banners'] = Produto::$diretorio_imagens_banners;
        $dados['diretorio_imagens_packs']   = Produto::$diretorio_imagens_packs;
        $dados['diretorio_imagens_versoes'] = ProdutoVersao::$diretorio_imagens;
        
        return view('app.produto.editar', $dados);
    }
    
    protected function deletar($id)
    {
        $produto = Produto::findOrFail($id);
        $produto->delete();
        
        flash()->success('Produto deletado com sucesso');
        
        return redirect()->back();
    }
    
    /**
     * 
     * @param Request $requisicao
     * @return type
     * @todo fazer funcionar e ver se uso tamanho maximo do upload igual ao max_upload_size
     */
    protected function validar(Request &$requisicao)
    {
        // Imagens
        $imagens = [
            'imagem' => $requisicao->file('imagem'),
            'imagem_pack' => $requisicao->file('imagem_pack'),
            'imagem_banner' => $requisicao->file('imagem_banner'),
        ];
        $validacao = Validator::make($imagens, [
            'imagem' => 'mimes:png|size:1024',
            'imagem_pack' => 'mimes:png|size:1024',
            'imagem_banner' => 'mimes:png|size:1024',
        ]);
        
        if ($validacao->fails()) {
            return redirect()
                    ->back()
                    ->withErrors($validacao)
                    ->withInput();
        }
        
        return true;
    }


    /**
     * Insere ou atualiza um produto.
     * 
     * Caso $produto seja informado, será uma atualização.
     * 
     * @param object $dados Illuminate\Http\Request
     * @param object $produto objeto da classe App\Models\Produto
     * @return mixed retornará true em caso de sucesso ou redirect()->back() em caso de erro
     */
    protected function salvar(Request &$requisicao, $produto = null)
    {
        $input = $requisicao->input();
        // Remove dados que não serão usados diretamente pelos métodos create e update do model Produto
        unset($input['_token']);
        unset($input['usos']);
        unset($input['versoes']);

        // Lida com as imagens
        // #TODO deletar imagens antigas e cujo processo de salvamento do produto falhou
        if ($requisicao->hasFile('imagem')) {
            try {
                $imagem = $requisicao->file('imagem');
                $nome_imagem = sha1(str_random('20')) . '.' . $imagem->guessClientExtension();
                $requisicao->file('imagem')->move(public_path(Produto::$diretorio_imagens), $nome_imagem);
                $input['imagem'] = $nome_imagem;
            }
            catch (\Exception $e) {
                flash()->error('Erro: ' . $e->getMessage());
                return redirect()->back()->withInput();
            }
        }
        if ($requisicao->hasFile('imagem_banner')) {
            try {
                $imagem_banner = $requisicao->file('imagem_banner');
                $nome_imagem = sha1(str_random('20')) . '.' . $imagem_banner->guessClientExtension();
                $requisicao->file('imagem_banner')
                                ->move(public_path(Produto::$diretorio_imagens_banners), $nome_imagem);
                $input['imagem_banner'] = $nome_imagem;
            }
            catch (\Exception $e) {
                flash()->error('Erro: ' . $e->getMessage());
                return redirect()->back()->withInput();
            }
        }
        if ($requisicao->hasFile('imagem_pack')) {
            try {
                $imagem_pack = $requisicao->file('imagem_pack');
                $nome_imagem = sha1(str_random('20')) . '.' . $imagem_pack->guessClientExtension();
                $requisicao->file('imagem_pack')->move(public_path(Produto::$diretorio_imagens_packs), $nome_imagem);
                $input['imagem_pack'] = $nome_imagem;
            }
            catch (\Exception $e) {
                flash()->error('Erro: ' . $e->getMessage());
                return redirect()->back()->withInput();
            }
        }
        
        // Atualiza ou cria um produto
        if ($produto) {
            try {
                $input['slug'] = $this->gerarSlug($input['nome'], $produto);
                $produto->update($input);
            } catch (QueryException $e) {
                $exception = $e->getPrevious();
                flash()->error('Erro ao atualizar o produto. Detalhes: ' . $exception->getMessage());
                return redirect()->back()->withInput();
            }
        } else {
            try {
                $input['slug'] = $this->gerarSlug($input['nome']);
                $produto = Produto::create($input);
            } catch (QueryException $e) {
                $exception = $e->getPrevious();
                flash()->error('Erro ao criar o produto. Detalhes: ' . $exception->getMessage());
                return redirect()->back()->withInput();
            }
        }
        
        if (is_array($requisicao->input('usos'))) {
            ProdutoUso::atualizar($produto->id, array_values($requisicao->input('usos')));
        }
        
        if (is_array($requisicao->input('versoes'))) {
            $atualiza_versoes = $this->atualizaVersoes($produto->id, $requisicao);
            if ($atualiza_versoes !== true) {
                return $atualiza_versoes;
            }
        }
        
        return true;
    }
    
    /**
     * Atualiza as versões de um dado produto.
     * 
     * Este método é utilizado pois são vários usos para um mesmo produto.
     * 
     * @param int $id id do produto
     * @param Request $requisicao
     * @fixme nao deleta arquivos antigos
     */
    public static function atualizaVersoes($id, Request $requisicao)
    {
        $arquivos = $requisicao->file('versoes');
        if ($arquivos === null) {
            $arquivos = [];
        }
        
        // Para cada bloco preenchido
        $versoes = $requisicao->input('versoes');
        foreach ($versoes as $chave => $item) {
            $dados = [
                'nome' => $item['nome'],
                'produto_id' => $id,
            ];
            
            // deleta os registros que não existem mais no formulario enviado
            $versoes_cadastradas = ProdutoVersao::where('produto_id', $id)->get()->toArray();
            $ids_cadastrados     = array_column($versoes_cadastradas, 'id');
            $ids_enviados        = array_keys($versoes);
            $ids_para_deletar    = array_values(array_diff($ids_cadastrados, $ids_enviados));
            //dump($ids_para_deletar);
            if ($ids_para_deletar) {
                ProdutoVersao::whereIn('id', $ids_para_deletar)->delete();
            }
            
            $busca = ProdutoVersao::where('id', $chave)->first();
            
            if ($busca) {
                // estou atualizando uma versão existente
                $operacao = 'update';
            } else {
                // estou criando uma versão
                $operacao = 'create';
            }
            
            // Se esta versão tem um arquivo que se chama imagem
            $upload = $requisicao->file('versoes.' . $chave);
            if ($upload && isset($upload['imagem'])) {
                $imagem = $upload['imagem'];
                try {
                    $nome_imagem = sha1(str_random('20')) . '.' . $imagem->guessClientExtension();
                    $imagem->move(public_path(ProdutoVersao::$diretorio_imagens), $nome_imagem);
                    $dados['imagem'] = $nome_imagem;
                }
                catch (\Exception $e) {
                    flash()->error('Erro: ' . $e->getMessage());
                    return redirect()->back()->withInput();
                }
            }
            
            if ($operacao == 'create') {
                ProdutoVersao::create($dados);
            } else {
                ProdutoVersao::where('id', $busca->id)->update($dados);
            }
        }
        
        return true;
    }


    /**
     * Retorna uma slug única com no máximo 100 caracteres.
     * 
     * Em caso de atualização, informe a variável $produto
     * 
     * @param string $titulo
     * @param Produto objeto produto
     * @return string
     * @todo usar apenas um metodo global, ao inves de um em casa controller
     */
    protected function gerarSlug($titulo = null, &$produto = null)
    {
        if ($titulo) {
            $slug_gerada = str_slug($titulo, '-');
        } else {
            $slug_gerada = str_random(100);
        }
        $slug = substr($slug_gerada, 0, 100);
        
        $busca = Produto::withTrashed()->ofSlug($slug)->first();
        if (! $busca ) {
            $ja_existe = false;
        } elseif ($produto === null) {
            // É uma inserção e o registro já existe
            $ja_existe = true;
        } else {
            $ja_existe = ($busca->id === $produto->id) ? false : true;
        }
            
        while($ja_existe === true) {
            $slug = substr(str_slug($titulo, '-'), 0, 94) . '-' . str_random(5);
            
            $busca = Produto::withTrashed()->ofSlug($slug)->first();
            if (! $busca ) {
                $ja_existe = false;
            } else {
                $ja_existe = ($busca->id === $produto->id) ? false : true;
            }
        }
        
        return $slug;
    }
}
