<?php

namespace App\Http\Controllers\App;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use Illuminate\Database\QueryException;
use App\Models\Tag;

class TagController extends Controller
{
    private $menu_ativo = 'app::blog';
    
    protected function index()
    {
        $dados = [];
        
        $tags = Tag::orderBy('created_at', 'DESC')->paginate(10);
        
        $dados['menu_ativo'] = $this->menu_ativo;
        $dados['tags'] = $tags;
        
        return view('app.blog.post.tag.index', $dados);
    }
    
    protected function criar(Request $requisicao)
    {
        $dados = [];
        
        if ($requisicao->isMethod('post')) {
            $input = $requisicao->input();
            unset($input['_token']);
            $input['slug'] = $this->gerarSlug($input['tag']);
            
            Tag::create($input);
            
            flash()->success('Tag criada com sucesso');
            return redirect(route('app::blog::post::tag::index'));
        }
        
        $dados['menu_ativo'] = $this->menu_ativo;
        
        return view('app.blog.post.tag.criar', $dados);
    }
    
    protected function editar(Request $requisicao, $id)
    {
        $dados = [];
        
        $dados['tag'] = Tag::findOrFail($id);
        
        if ($requisicao->isMethod('post')) {
            $input = $requisicao->input();
            unset($input['_token']);
            $input['slug'] = $this->gerarSlug($input['tag'], $dados['tag']);
            
            $dados['tag']->update($input);
            
            flash()->success('Tag editada com sucesso');
            return redirect(route('app::blog::post::tag::index'));
        }
        
        $dados['menu_ativo'] = $this->menu_ativo;
        
        return view('app.blog.post.tag.editar', $dados);
    }
    
    protected function deletar($id)
    {
        $tag = Tag::findOrFail($id);
        $tag->delete();
        
        flash()->success('Tag deletada com sucesso');
        
        return redirect()->back();
    }
    
    /**
     * Retorna uma slug única com no máximo 100 caracteres.
     * 
     * Em caso de atualização, informe a variável $tag
     * 
     * @param string $nome
     * @param Post objeto post
     * @return string
     * @todo usar apenas um metodo global, ao inves de um em casa controller
     */
    protected function gerarSlug($nome = null, &$tag = null)
    {
        if ($nome) {
            $slug_gerada = str_slug($nome, '-');
        } else {
            $slug_gerada = str_random(100);
        }
        $slug = substr($slug_gerada, 0, 100);
        
        $busca = Tag::withTrashed()->ofSlug($slug)->first();
        if (! $busca ) {
            $ja_existe = false;
        } elseif ($tag === null) {
            // É uma inserção e o registro já existe
            $ja_existe = true;
        } else {
            $ja_existe = ($busca->id === $tag->id) ? false : true;
        }
            
        while($ja_existe === true) {
            $slug = substr(str_slug($nome, '-'), 0, 94) . '-' . str_random(5);
            
            $busca = Tag::withTrashed()->ofSlug($slug)->first();
            if (! $busca ) {
                $ja_existe = false;
            } else {
                $ja_existe = ($busca->id === $tag->id) ? false : true;
            }
        }
        
        return $slug;
    }
}
