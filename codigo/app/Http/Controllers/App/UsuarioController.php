<?php

namespace App\Http\Controllers\App;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Usuario;

class UsuarioController extends Controller
{
    private $menu_ativo = 'app::usuario';
    
    protected function index()
    {
        $dados = [];
        
        $usuarios = Usuario::orderBy('created_at', 'DESC')->paginate(10);
        
        $dados['menu_ativo'] = $this->menu_ativo;
        $dados['usuarios'] = $usuarios;
        
        return view('app.usuario.index', $dados);
    }
    
    protected function criar(Request $requisicao)
    {
        $dados = [];
        
        if ($requisicao->isMethod('post')) {
            $input = $requisicao->input();
            unset($input['_token']);
            unset($input['senha']);
            unset($input['senha_confirmation']);
            
            $this->validate($requisicao, [
                'nome' => 'required|max:100',
                'email' => 'required|unique:usuarios|max:100',
                'senha' => 'required|min:5|confirmed',
            ]);
            
            $input['password'] = bcrypt($requisicao->input('senha'));
            
            Usuario::create($input);
            
            flash()->success('Usuario criado com sucesso');
            return redirect(route('app::usuario::index'));
        }
        
        $dados['menu_ativo'] = $this->menu_ativo;
        
        return view('app.usuario.criar', $dados);
    }
    
    protected function editar(Request $requisicao, $id)
    {
        $dados = [];
        
        $dados['usuario'] = Usuario::findOrFail($id);
        
        if ($requisicao->isMethod('post')) {
            $input = $requisicao->input();
            unset($input['_token']);
            unset($input['senha']);
            unset($input['senha_confirmation']);
            
            if ($requisicao->has('senha')) {
                unset($input['senha']);
                unset($input['senha_confirmation']);

                $this->validate($requisicao, [
                    'nome' => 'required|max:100',
                    'email' => 'required|max:100',
                    'senha' => 'required|min:5|confirmed',
                ]);
                $input['password'] = bcrypt($requisicao->input('senha'));
            }
            
            $dados['usuario']->update($input);
            
            flash()->success('Usuario editado com sucesso');
            return redirect(route('app::usuario::index'));
        }
        
        $dados['menu_ativo'] = $this->menu_ativo;
        
        return view('app.usuario.editar', $dados);
    }
    
    protected function deletar($id)
    {
        $usuario = Usuario::findOrFail($id);
        $usuario->delete();
        
        flash()->success('Usuario deletado com sucesso');
        
        return redirect()->back();
    }
}
