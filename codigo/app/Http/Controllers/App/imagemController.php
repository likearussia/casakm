<?php

namespace App\Http\Controllers\App;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class imagemController extends Controller
{
    public function upload(Request $request){
        
        $imagem = $request->file('caminho');
        $nome_imagem = sha1(str_random('20')) . '.' . $imagem->guessClientExtension();
        $request->file('caminho')->move(public_path('tmp_uploads'), $nome_imagem);
        echo $nome_imagem;
        
    }
}