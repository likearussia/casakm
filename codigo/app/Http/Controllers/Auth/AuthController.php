<?php

namespace App\Http\Controllers\Auth;

use App\Models\Usuario;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
#
use Illuminate\Http\Request;
use Auth;
use Hash;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->redirectTo = route('app::inicio');
        
        $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
    }

    /**
     * Exibe o formulário de login.
     * O nome deste método é igual ao nome utilizado por padrão pelo laravel
     * 
     * @return View
     */
    protected function showFormLogin()
    {
        return view('auth.login');
    }
    
    /**
     * Executa a lógica de login;
     * O nome deste método é igual ao nome utilizado por padrão pelo laravel, porém
     * executa toda a lógica de login.
     * 
     * @param Request $requisicao
     * @return Response redirect
     */
    protected function login(Request $requisicao)
    {
        $this->validate($requisicao, [
            'inputUser' => 'required',
            'inputPassword' => 'required',
        ]);
        
        $usuario = $requisicao->input('inputUser');
        $senha   = $requisicao->input('inputPassword');
        $lembrar = ($requisicao->has('remember') ? true : false);
        
        if (Auth::attempt(['email' => $usuario, 'password' => $senha], $lembrar)) {
            // Usuário autenticado
            $requisicao->session()->put('eh_admin', true);
            return redirect()->intended(route('app::inicio'));
        }
        
        flash()->error('Usuário e/ou senha incorretos');
        return redirect()->back();
    }


    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     * 
     * No padrão do laravel o equivalente deste método é o create.
     *
     * @param  Request
     * @return Usuario
     */
    protected function criar(Request $requisicao)
    {
        return User::create([
            'name' => $requisicao->input('nome'),
            'email' => $requisicao->input('email'),
            'password' => bcrypt($requisicao->input('password')),
        ]);
    }
    
    protected function editar($id)
    {
        $usuario = Usuario::findOrFail($id);
    }


    /**
     * Desloga o usuário.
     * O nome deste método é igual ao nome utilizado por padrão pelo laravel.
     * 
     * @return Response redirect
     */
    protected function logout()
    {
        Auth::logout();
        return redirect(url('/'));
    }
}
