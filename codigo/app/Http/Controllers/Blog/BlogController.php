<?php

namespace App\Http\Controllers\Blog;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Post;
use App\Models\PostCategoria;
use App\Models\Tag;
use App\Services\MapaSiteService;

class BlogController extends Controller
{
	
	
    public function index()
    {
        $dados = [];
        
        $query = Post::orderBy('created_at', 'DESC');
        
        $dados['menu_ativo']        = 'blog';
        $dados['posts']             = $query->paginate(9);
        $dados['categorias']        = PostCategoria::orderBy('nome', 'ASC')->get();
        $dados['tags']              = Tag::orderBy('tag', 'ASC')->get();
        $dados['diretorio_imagens'] = Post::$diretorio_imagens;
        $dados['mapa_site']         = MapaSiteService::rodape();
        
        return view('blog.index', $dados);
    }
    
    public function indexPorCategoria($slug)
    {
        $dados = [];
        
        $query = PostCategoria::ofSlug($slug)
                    ->join('posts', 'posts.post_categoria_id', '=', 'post_categorias.id')
                    ->select(['posts.titulo', 'posts.slug', 'posts.texto', 'posts.imagem_destaque'])->where("posts.deleted_at", null)
                    ->orderBy('posts.created_at', 'DESC');
        
        $dados['menu_ativo']           = 'blog';
        $dados['posts']                = $query->paginate(9);
        $dados['categorias']           = PostCategoria::orderBy('nome', 'ASC')->get();
        $dados['tags']                 = Tag::orderBy('tag', 'ASC')->get();
        $dados['menu_categoria_ativo'] = $slug;
        $dados['diretorio_imagens']    = Post::$diretorio_imagens;
        $dados['mapa_site']            = MapaSiteService::rodape();
        
        return view('blog.index', $dados);
    }
    
    public function indexPorTag($slug)
    {
        $dados = [];
        
        $query = Tag::ofSlug($slug)
                    ->join('posts_tags', 'posts_tags.tag_id', '=', 'tags.id')
                    ->join('posts', 'posts.id','=', 'posts_tags.post_id')
                    ->select(['posts.titulo', 'posts.slug', 'posts.texto', 'posts.imagem_destaque'])
                    ->groupBy('posts_tags.post_id')
                    ->orderBy('posts.created_at', 'DESC');
        
        $dados['menu_ativo']           = 'blog';
        $dados['posts']                = $query->paginate(9);
        $dados['categorias']           = PostCategoria::orderBy('nome', 'ASC')->get();
        $dados['tags']                 = Tag::orderBy('tag', 'ASC')->get();
        $dados['menu_tag_ativo']       = $slug;
        $dados['diretorio_imagens']    = Post::$diretorio_imagens;
        $dados['mapa_site']            = MapaSiteService::rodape();
        
        return view('blog.index', $dados);
    }


    public function buscar(Request $requisicao, $termo = null)
    {
        $dados = [];
        
        if (empty($termo)) {
            $termo = $requisicao->input('termo');
        }
        
        if (empty($termo)) {
            abort('404');
        }
        
       
        // Sem order by esta query irá ordernar por relevância
        $query_posts    = Post::whereRaw("MATCH(posts.titulo, posts.texto) AGAINST(?)", array($termo));
        
        $dados['posts']                  = $query_posts->take(12)->paginate(12);
        $dados['total_resultados']       = count($dados['posts']);
        $dados['diretorio_imagens_post'] = Post::$diretorio_imagens;
        $dados['mapa_site']              = MapaSiteService::rodape();
        $dados['menu_ativo']             = 'blog';
        
        return view('blog.buscar', $dados);
    }
}