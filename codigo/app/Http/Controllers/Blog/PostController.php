<?php

namespace App\Http\Controllers\Blog;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Post;
use App\Models\PostTag;
use App\Models\Tag;
use App\Services\MapaSiteService;

class PostController extends Controller
{
    public function visualizar($slug)
    {
        $dados = [];
        
        $query = Post::ofSlug($slug);
        
        $dados['menu_ativo']        = 'blog';
        $dados['post']              = $query->firstOrFail();
        $dados['tags']              = Tag::orderBy('tag', 'ASC')->get();
        $dados['diretorio_imagens'] = Post::$diretorio_imagens;
        $dados['mapa_site']         = MapaSiteService::rodape();
        
        return view('blog.interna', $dados);
    }
}