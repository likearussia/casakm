<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Produto;
use App\Models\Post;
use App\Services\MapaSiteService;
use Illuminate\Pagination\LengthAwarePaginator;
use DB;

class BuscarController extends Controller
{
    public function index(Request $requisicao, $termo = null)
    {
        $dados = [];
        
        if (empty($termo)) {
            $termo = $requisicao->input('termo');
        }
        
        if (empty($termo)) {
            abort('404');
        }
        
        // Sem order by esta query irá ordernar por relevância
        // Referencias:
        // https://laracasts.com/discuss/channels/general-discussion/paginator-class-is-missing
        // https://laravel.com/docs/5.2/pagination#manually-creating-a-paginator
        // http://stackoverflow.com/questions/25338456/laravel-union-paginate-at-the-same-time
        $query_produtos = Produto::select(DB::raw('produtos.nome as titulo, produtos.descricao_curta as texto, '
                                                    . 'produtos.imagem as imagem, produtos.slug as slug, '
                                                    . '"produto" as tipo'))
                                    ->whereRaw("MATCH(produtos.nome, produtos.descricao_curta, "
                                        . "produtos.descricao_longa, produtos.caracteristicas, "
                                        . "produtos.informacoes_uso, produtos.informacoes_seguranca) AGAINST(?)",
                                        array($termo));
        // Sem order by esta query irá ordernar por relevância
        $query = Post::select(DB::raw('posts.titulo as titulo, posts.texto as texto, posts.imagem_destaque as imagem, '
                                        . 'posts.slug as slug, "post" as tipo'))
                                ->whereRaw("MATCH(posts.titulo, posts.texto) AGAINST(?)", array($termo))
                                ->union($query_produtos)
                                ->get();
        
        $pagina = $requisicao->input('page', 1);
        $por_pagina = 15;
        // Começar exibindo os itens deste núemro
        $offset = ($pagina * $por_pagina) - $por_pagina;
        // Itens que serão exibidos na página atual
        $itens = array_slice($query->toArray(), $offset, $por_pagina, true);
        
        $dados['total_resultados']   = count($query);
        $dados['resultados']         = new LengthAwarePaginator($itens, $dados['total_resultados'], $por_pagina,
                                                                    $pagina, ['path' => $requisicao->url()]);
        $dados['dir_imagem_produto'] = Produto::$diretorio_imagens;
        $dados['dir_imagem_post']    = Post::$diretorio_imagens;
        $dados['mapa_site']          = MapaSiteService::rodape();
        
        return view('site.buscar.index', $dados);
    }
}