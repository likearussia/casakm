<?php

namespace App\Http\Controllers\Site;

use Mail;
use App\Models\Contato;
use Illuminate\Http\Request;
use App\Services\MapaSiteService;
use App\Http\Controllers\Controller;

class ContatoController extends Controller
{
    protected $menu_ativo = 'site_contato';
    
    public function index()
    {
        $dados = [];
        
        $dados['menu_ativo'] = $this->menu_ativo;
        $dados['assuntos']   = Contato::$assuntos;
        $dados['mapa_site']  = MapaSiteService::rodape();
        
        return view('site.contato.index', $dados);
    }
    
    public function enviar(Request $requisicao)
    {
        $dados = [];
        $dados['form'] = $requisicao->input();
        
        /**
         * O endereço deve ter 50 caracateres, então divido entre logradouro e número.
         * No HTML tem limitação de caracteres para os campos que são limitados pelo sistema da CasaKm
         */
        $this->validate($requisicao, [
            'nome' => 'required|max:80',
            'endereco_logradouro' => 'max:45',
            'endereco_numero' => 'max:4',
            'endereco_complemento' => 'max:100',
            'endereco_bairro' => 'max:100',
            'cidade' => 'max:60',
            'uf' => 'max:2',
            'cep' => 'digits:8',
            'telefone' => 'max:20',
            'telefone_celular' => 'max:20',
            'email' => 'max:80',
            'mensagem' => 'max:4000',
        ]);
        
        // Tratando alguns dados
        $dados['form']['telefone']         = preg_replace('/[^0-9]/', '', $dados['form']['telefone']);
        $dados['form']['telefone_celular'] = preg_replace('/[^0-9]/', '', $dados['form']['telefone_celular']);
        
        $enviar = Mail::send(['text' => 'site.emails.fale_conosco'], ['dados' => $dados], function ($m) {
            $m->from(config('mail.endereco_origem'), 'Site CasaKm');
            $m->to(config('mail.destinatarios.sac'), 'SAC CasaKm')->subject('Site KM Casa');
        });
        
        if ($enviar) {
            //flash()->success('E-mail enviado com sucesso!');
            $requisicao->session()->flash('mensagem', 'E-mail enviado com sucesso!');
        } else {
            //flash()->error('Ocorreu um erro ao enviar a mensagem.');
            $requisicao->session()->flash('mensagem', 'Ocorreu um erro ao enviar a mensagem.');
        }
        
        return redirect()->back();
    }
}