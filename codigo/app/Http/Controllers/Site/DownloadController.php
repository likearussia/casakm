<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Download;
use App\Models\EquipeVenda;
use App\Services\MapaSiteService;

class DownloadController extends Controller
{
    private $menu_ativo = 'site_downloads';
    
    public function index(Request $requisicao)
    {   
        $dados = [];
        
        $dados['esta_logado'] = $requisicao->session()->get('download_restrito', false);
        
        $query = Download::orderBy("categoria")->orderBy('id');
        if ($dados['esta_logado'] == false) {
            $query->where('eh_restrito', 0);
        }
        
        $dados['menu_ativo']         = $this->menu_ativo;
        $dados['downloads']          = $query->get();
        $dados['diretorio_arquivos'] = Download::$diretorio_arquivos;
        $dados['diretorio_imagens']  = Download::$diretorio_imagens;
        $dados['mapa_site']          = MapaSiteService::rodape();
        
        return view('site.download.index', $dados);
    }
    
    /**
     * Login para acesso aos arquivos restritos
     */
    protected function login(Request $requisicao)
    {
        if (! $requisicao->isMethod('post') || ! $requisicao->has('codigo')) {
            return redirect()->back();
        }
        
        $busca = EquipeVenda::where('codigo', $requisicao->input('codigo'))->first();
        
        if (! $busca) {
            //flash()->error('Desculpe, o código informado está incorreto');
            $requisicao->session()->flash('mensagem', 'Desculpe, o código informado está incorreto');
            $requisicao->session()->put('download_restrito', false);
        } else {
            //flash()->success('Login efetuado com sucesso');
            $requisicao->session()->flash('mensagem', 'Login efetuado com sucesso');
            $requisicao->session()->put('download_restrito', true);
        }
        
        return redirect()->back();
    }

    /**
     * 
     * @param Request $requisicao
     */
    protected function logoff(Request $requisicao)
    {
        $requisicao->session()->put('download_restrito', false);
        return redirect(route('download::index'));
    }

    public function download(Request $requisicao, $slug)
    {   
        $download = Download::ofSlug($slug)->first();
        if (! $download) {
            abort('404');
        }
        
        // Se o usuário tiver feito login no formulário específico ou for administrador
        $pode_baixar = $requisicao->session()->get('download_restrito', false) ||
                            $requisicao->session()->get('eh_admin', false);
        
        if ($download->eh_restrito == 1 && $pode_baixar == false) {
            abort(403);
        }
        
        $path_arquivo = storage_path(Download::$diretorio_arquivos . '/' . $download->arquivo);
        return response()->download($path_arquivo, $download->arquivo);
    }
}
