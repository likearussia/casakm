<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Models\Post;
use App\Services\MapaSiteService;
use App\Models\Banner;

class HomeController extends Controller
{
    public function index()
    {
        $dados = [];
        
        $query = Post::orderBy('created_at', 'DESC')->limit(3);
        
        $query_banners = Banner::orderBy('created_at', 'DESC')->where('area', 'home');
        
        $dados['menu_ativo']           = '';
        $dados['banners_topo']         = $query_banners->get();
        $dados['dir_img_banner_topo']  = Banner::$diretorio_imagens;
        $dados['posts']                = $query->get();
        $dados['diretorio_imagens']    = Post::$diretorio_imagens;
        $dados['mapa_site']            = MapaSiteService::rodape();
        
        
        return view('site.home', $dados);
    }
}