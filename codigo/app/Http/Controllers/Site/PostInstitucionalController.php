<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use App\Models\PostInstitucional;
use App\Services\MapaSiteService;

class PostInstitucionalController extends Controller
{
    private $menu_ativo = 'site_institucional';
    
    public function sobre($slug)
    {   
        $dados = [];
        
        $query = PostInstitucional::ofSlug($slug)->orderBy('id');
        
        // O where serve apenas para não colocar a única 'página' sustentabilidade junto com este conteúdo.
        // #TODO melhorar isso quando for possível inserir páginas
        $itens_menu = PostInstitucional::select(['titulo', 'slug'])->where('id', '<', 7)->orderBy('id');
        
        $dados['menu_ativo']         = $this->menu_ativo;
        $dados['slug']               = $slug;
        $dados['conteudo']           = $query->first();
        $dados['itens_menu']         = $itens_menu->get();
        $dados['diretorio_imagens']  = PostInstitucional::$diretorio_imagens;
        $dados['mapa_site']          = MapaSiteService::rodape();
        
        return view('site.institucional.sobre', $dados);
    }
    
    public function pagina($slug)
    {   
        $dados = [];
        
        $query = PostInstitucional::ofSlug($slug)->orderBy('id');
        
        $dados['menu_ativo']         = "site_{$slug}";
        $dados['slug']               = $slug;
        $dados['conteudo']           = $query->first();
        $dados['diretorio_imagens']  = PostInstitucional::$diretorio_imagens;
        $dados['mapa_site']          = MapaSiteService::rodape();
        
        return view('site.institucional.pagina', $dados);
    }
}
