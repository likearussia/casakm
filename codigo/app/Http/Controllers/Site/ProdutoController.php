<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Produto;
use App\Models\ProdutoMarca;
use App\Models\ProdutoUsoCategoria;
use App\Models\ProdutoUsoCategoriaItem;
use App\Models\ProdutoVersao;
use App\Services\MapaSiteService;

class ProdutoController extends Controller
{
    protected $menu_ativo = 'site_produtos';
    
    public function index($uso = null)
    {   
        $dados = [];
        $menu_produto_ativo = 'index';
        
        $query_produtos = Produto::select('*');
        
        if ($uso) {
            $busca_tipo_uso = ProdutoUsoCategoriaItem::ofSlug($uso)->first();
            if (! $busca_tipo_uso) {
                abort('404');
            }
            //$query_produtos->usos->where('produtos_usos_categoria_item_id', $busca_tipo_uso->id);
            $query_produtos->join('produtos_usos', 'produtos_usos.produto_id', '=', 'produtos.id')
                ->where('produtos_usos.produtos_usos_categoria_item_id', $busca_tipo_uso->id);
            $menu_produto_ativo = $uso;
        }
        
        $dados['menu_ativo'] = $this->menu_ativo;
        $dados['menu_produto_ativo'] = $menu_produto_ativo;
        $dados['produtos'] = $query_produtos->take(9)->paginate(9);
        $dados['diretorio_imagens'] = Produto::$diretorio_imagens;
        
        $dados['marcas'] = ProdutoMarca::where('id', '>', 0)->get();
        $dados['categorias'] = ProdutoUsoCategoria::where('id', '>', 0)->get();
        $dados['mapa_site'] = MapaSiteService::rodape();
        
        return view('site.produto.index', $dados);
    }
    
    /**
     * Faz o mesmo que o método index() exceto que este filtra os produtos por marca.
     * 
     * Este método existe porque não encontrei um meio de usar o index()
     * deste controle passando parâmetros fixos pela rota, para identificar
     * se o filtro é por marca ou uso.
     * 
     * @param type $marca slug da marca
     * @return Response com View
     */
    public function indexMarca($marca)
    {
        $dados = [];
        
        $query_produtos = Produto::select('*');
        
        $busca_marca = ProdutoMarca::ofSlug($marca)->first();
        if (! $busca_marca) {
            abort('404');
        }
        $query_produtos->where('produto_marca_id', $busca_marca->id);
        
        $dados['menu_ativo'] = $this->menu_ativo;
        $dados['menu_produto_ativo'] = 'marcas';
        $dados['produtos'] = $query_produtos->take(9)->paginate(9);
        $dados['diretorio_imagens'] = Produto::$diretorio_imagens;
        
        $dados['marcas'] = ProdutoMarca::select('*')->get();
        $dados['categorias'] = ProdutoUsoCategoria::select('*')->get();
        $dados['mapa_site'] = MapaSiteService::rodape();
        
        return view('site.produto.index', $dados);
    }
    
    public function show($slug)
    {   
        $dados['produto'] = Produto::ofSlug($slug)->first();
        if (! $dados['produto']) {
            abort('404');
        }
        
        $dados['menu_ativo'] = $this->menu_ativo;
        $dados['diretorio_imagens'] = Produto::$diretorio_imagens;
        $dados['diretorio_imagens_banners'] = Produto::$diretorio_imagens_banners;
        $dados['diretorio_imagens_packs'] = Produto::$diretorio_imagens_packs;
        $dados['diretorio_imagens_versoes'] = ProdutoVersao::$diretorio_imagens;
        $dados['versoes'] = $dados['produto']->versoes;
        $dados['itens_uso'] = ProdutoUsoCategoriaItem::select('*')->get(); //gambi
        $dados['similares'] = Produto::where('produto_marca_id', $dados['produto']->produto_marca_id)
            ->take(4)->get();
        $dados['mapa_site'] = MapaSiteService::rodape();
        
        return view('site.produto.visualizar', $dados);
    }
}