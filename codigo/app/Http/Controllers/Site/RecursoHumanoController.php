<?php

namespace App\Http\Controllers\Site;

use Mail;
use App\Models\RecursoHumano;
use Illuminate\Http\Request;
use App\Services\MapaSiteService;
use App\Http\Controllers\Controller;

class RecursoHumanoController extends Controller
{
    protected $menu_ativo = 'site_rh';
    
    public function index()
    {
        $dados = [];
        
        $dados['menu_ativo']    = $this->menu_ativo;
        $dados['departamentos'] = RecursoHumano::$departamentos;
        $dados['mapa_site']     = MapaSiteService::rodape();
        
        return view('site.recursoshumanos.contato', $dados);
    }
    
    public function enviar(Request $requisicao)
    {
        $dados = [];
        $dados['form'] = $requisicao->input();
        
        $this->validate($requisicao, [
            'nome' => 'required|max:80',
            'endereco_logradouro' => 'max:45',
            'endereco_numero' => 'max:4',
            'endereco_complemento' => 'max:100',
            'endereco_bairro' => 'max:100',
            'cidade' => 'max:60',
            'uf' => 'max:2',
            'cep' => 'digits:8',
            'telefone' => 'max:20',
            'telefone_celular' => 'max:20',
            'email' => 'max:80',
            'mensagem' => 'max:4000',
        ]);
        
        $enviar = Mail::send(['text' => 'site.emails.fale_conosco'], ['dados' => $dados], function ($m) use ($requisicao) {
            $m->from(config('mail.endereco_origem'), 'Site CasaKm');
            $m->to(config('mail.destinatarios.rh'), 'RH CasaKm')->subject('RH - via site');
            if ($requisicao->hasFile('curriculo') && $requisicao->file('curriculo')->isValid()) {
                $m->attach($requisicao->file('curriculo')->getPathname(), [
                    'as' => $requisicao->file('curriculo')->getClientOriginalName(),
                    //'mime' => $requisicao->file('curriculo')->getMimeType(),
                ]);
            }
        });
        
        if ($enviar) {
            //flash()->success('E-mail enviado com sucesso!');
            $requisicao->session()->flash('mensagem', 'E-mail enviado com sucesso!');
        } else {
            //flash()->error('Ocorreu um erro ao enviar a mensagem.');
            $requisicao->session()->flash('mensagem', 'Ocorreu um erro ao enviar a mensagem.');
        }
        
        return redirect()->back();
    }
}