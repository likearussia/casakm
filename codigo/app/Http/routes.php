<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::group([], function () {
    Route::get('/', [
        'uses' => 'Site\HomeController@index', 'as' => 'site::index'
    ]);
    
    // Institucional - nível para páginas únicas
    Route::get('/pagina/{slug}', [
        'uses' => 'Site\PostInstitucionalController@pagina', 'as' => 'site::institucional::pagina'
    ]);
    
    // Institucional - nível para subpáginas do sobre
    Route::get('/sobre/{slug}', [
        'uses' => 'Site\PostInstitucionalController@sobre', 'as' => 'site::institucional::sobre'
    ]);

    // Institucional - Recursos humanos
    Route::get('/recursos-humanos', [
        'uses' => 'Site\RecursoHumanoController@index', 'as' => 'rh_inicio'
    ]);
    Route::post('/recursos-humanos', [
        'uses' => 'Site\RecursoHumanoController@enviar', 'as' => 'rh_enviar'
    ]);

    // Institucional - Contato
    Route::get('/contato', [
        'uses' => 'Site\ContatoController@index', 'as' => 'contato_inicio'
    ]);
    Route::post('/contato', [
        'uses' => 'Site\ContatoController@enviar', 'as' => 'contato_enviar'
    ]);
});

// App\Http\Controllers\Site
Route::group(['namespace' => 'Site'], function () {
    // Busca
    Route::match(['get', 'post'], '/buscar/{termo?}', [
        'uses' => 'BuscarController@index', 'as' => 'site_buscar'
    ]);
    

    // Produtos
    Route::group(['prefix' => 'produtos', 'as' => 'produto::'], function () {
        Route::get('/', [
            'uses' => 'ProdutoController@index', 'as' => 'index'
        ]);
        Route::get('/{slug}', [
            'uses' => 'ProdutoController@show', 'as' => 'ver'
        ]);
        Route::get('/uso/{item}', [
            'uses' => 'ProdutoController@index', 'as' => 'por_uso'
        ]);
        Route::get('/marca/{marca}', [
            'uses' => 'ProdutoController@indexMarca', 'as' => 'por_marca'
        ]);
    });
    
    // Downloads
    Route::group(['as'=> 'download::'], function () {
        Route::get('/downloads', [
            'uses' => 'DownloadController@index', 'as' => 'index'
        ]);
        Route::post('/downloads/login', [
            'uses' => 'DownloadController@login', 'as' => 'login'
        ]);
        Route::get('/downloads/logoff', [
            'uses' => 'DownloadController@logoff', 'as' => 'logoff'
        ]);
        Route::get('/downloads/{slug}', [
            'uses' => 'DownloadController@download', 'as' => 'download'
        ]);
    });
});

// App\Http\Controllers\Blog
Route::group(['namespace' => 'Blog', 'prefix' => 'casa-em-dia', 'as' => 'blog::'], function () {
    Route::get('/', [
        'uses' => 'BlogController@index', 'as' => 'index'
    ]);
      Route::get('/busca', [
            'uses' => 'BlogController@buscar', 'as' => 'buscar'
        ]);
    Route::group(['as' => 'post::'], function () {
        Route::get('/{slug}', [
            'uses' => 'PostController@visualizar', 'as' => 'visualizar'
        ]);
        Route::get('/categoria/{slug}', [
            'uses' => 'BlogController@indexPorCategoria', 'as' => 'categoria::index'
        ]);
        Route::get('/tag/{slug}', [
            'uses' => 'BlogController@indexPorTag', 'as' => 'tag::index'
        ]);
    });
});


// App\Http\Controllers\Auth
//Route::auth();
Route::group(['namespace' => 'Auth', 'prefix' => '_auth'], function () {
    Route::match(['get', 'head'], '/login', [
        'uses' => 'AuthController@showFormLogin', 'as' => 'app::login'
    ]);
    Route::post('/login', [
        'uses' => 'AuthController@login', 'as' => 'app::post_login'
    ]);
    Route::match(['get', 'head'], '/logout', [
        'uses' => 'AuthController@logout', 'as' => 'app::logout'
    ]);
    
    Route::post('/password/email', [
        'uses' => 'PasswordController@sendResetLinkEmail', 'as' => 'app::send_reset_password'
    ]);
    Route::post('/password/reset', [
        'uses' => 'PasswordController@reset', 'as' => 'app::reset_password'
    ]);
    Route::get('/password/reset/{token?}', [
        'uses' => 'PasswordController@showResetForm'
    ]);
});

// App\Http\Controllers\App
Route::group(['namespace' => 'App', 'as' => 'app::', 'prefix' => '_app', 'middleware' => ['auth']], function () {
    // Início
    Route::match(['get', 'post'], '/', [
        'uses' => 'IndexController@index', 'as' => 'inicio'
    ]);
    
    Route::post('/upImagem',[
        'uses' => 'imagemController@upload', 'as' => 'imagem'
    ]);
    
    // Banners
    Route::group(['as'=> 'banner::', 'prefix' => 'banner'], function () {
        Route::get('/', [
            'uses' => 'BannerController@index', 'as' => 'index'
        ]);
        Route::get('/criar', [
            'uses' => 'BannerController@criar', 'as' => 'criar'
        ]);
        Route::get('/editar/{id}', [
            'uses' => 'BannerController@editar', 'as' => 'editar'
        ]);
        Route::post('/salvar/{id?}', [
            'uses' => 'BannerController@salvar', 'as' => 'salvar'
        ]);
        Route::get('/deletar/{id}', [
            'uses' => 'BannerController@deletar', 'as' => 'deletar'
        ]);
    });
    
    // Institucional
    Route::group(['as'=> 'institucional::', 'prefix' => 'institucional'], function () {
        Route::get('/', [
            'uses' => 'PostInstitucionalController@index', 'as' => 'index'
        ]);
        Route::match(['get', 'post'], '/criar', [
            'uses' => 'PostInstitucionalController@criar', 'as' => 'criar'
        ]);
        Route::match(['get', 'post'], '/editar/{id}', [
            'uses' => 'PostInstitucionalController@editar', 'as' => 'editar'
        ]);
        Route::get('/deletar/{id}', [
            'uses' => 'PostInstitucionalController@deletar', 'as' => 'deletar'
        ]);
    });
    
    // Blog
    Route::group(['as'=> 'blog::', 'prefix' => 'blog'], function () {
        Route::group(['as'=> 'post::', 'prefix' => 'posts'], function () {
            Route::get('/', [
                'uses' => 'PostController@index', 'as' => 'index'
            ]);
            Route::match(['get', 'post'], '/criar', [
                'uses' => 'PostController@criar', 'as' => 'criar'
            ]);
            Route::match(['get', 'post'], '/editar/{id}', [
                'uses' => 'PostController@editar', 'as' => 'editar'
            ]);
            Route::get('/deletar/{id}', [
                'uses' => 'PostController@deletar', 'as' => 'deletar'
            ]);
            Route::group(['as'=> 'categoria::', 'prefix' => 'categorias'], function () {
                Route::get('/', [
                    'uses' => 'PostCategoriaController@index', 'as' => 'index'
                ]);
                Route::match(['get', 'post'], '/criar', [
                    'uses' => 'PostCategoriaController@criar', 'as' => 'criar'
                ]);
                Route::match(['get', 'post'], '/editar/{id}', [
                    'uses' => 'PostCategoriaController@editar', 'as' => 'editar'
                ]);
                Route::get('/deletar/{id}', [
                    'uses' => 'PostCategoriaController@deletar', 'as' => 'deletar'
                ]);
            });
            Route::group(['as'=> 'tag::', 'prefix' => 'tags'], function () {
                Route::get('/', [
                    'uses' => 'TagController@index', 'as' => 'index'
                ]);
                Route::match(['get', 'post'], '/criar', [
                    'uses' => 'TagController@criar', 'as' => 'criar'
                ]);
                Route::match(['get', 'post'], '/editar/{id}', [
                    'uses' => 'TagController@editar', 'as' => 'editar'
                ]);
                Route::get('/deletar/{id}', [
                    'uses' => 'TagController@deletar', 'as' => 'deletar'
                ]);
            });
        });
    });
    
    // Produtos
    Route::group(['as'=> 'produto::', 'prefix' => 'produtos'], function () {
        Route::get('/', [
            'uses' => 'ProdutoController@index', 'as' => 'index'
        ]);
        Route::match(['get', 'post'], '/criar', [
            'uses' => 'ProdutoController@criar', 'as' => 'criar'
        ]);
        Route::match(['get', 'post'], '/editar/{id}', [
            'uses' => 'ProdutoController@editar', 'as' => 'editar'
        ]);
        Route::get('/deletar/{id}', [
                'uses' => 'ProdutoController@deletar', 'as' => 'deletar'
            ]);
    });
    
    // Downloads
    Route::group(['as'=> 'download::', 'prefix' => 'downloads'], function () {
        Route::get('/', [
            'uses' => 'DownloadController@index', 'as' => 'index'
        ]);
        Route::match(['get', 'post'], '/criar', [
            'uses' => 'DownloadController@criar', 'as' => 'criar'
        ]);
        Route::match(['get', 'post'], '/editar/{id}', [
            'uses' => 'DownloadController@editar', 'as' => 'editar'
        ]);
        Route::get('/deletar/{id}', [
            'uses' => 'DownloadController@deletar', 'as' => 'deletar'
        ]);
    });
    
    // Usuários
    Route::group(['as'=> 'usuario::', 'prefix' => 'usuarios'], function () {
        Route::get('/', [
            'uses' => 'UsuarioController@index', 'as' => 'index'
        ]);
        Route::match(['get', 'post'], '/criar', [
            'uses' => 'UsuarioController@criar', 'as' => 'criar'
        ]);
        Route::match(['get', 'post'], '/editar/{id}', [
            'uses' => 'UsuarioController@editar', 'as' => 'editar'
        ]);
        Route::get('/{id}', [
            'uses' => 'UsuarioController@deletar', 'as' => 'deletar'
        ]);
    });
});

// App\Http\Controllers\Api
Route::group(['namespace' => 'Api', 'as' => 'api::', 'prefix' => 'api', 'middleware' => ['auth']], function () {
    // App
    Route::group(['namespace' => 'App', 'as'=> 'app::', 'prefix' => 'app'], function () {
        // Tag
        Route::group(['as'=> 'tag::', 'prefix' => 'tags'], function () {
            Route::get('/{campo}/{valor?}', [
                'uses' => 'TagController@buscar', 'as' => 'buscar'
            ]);
            Route::get('/criar/{valor?}', [
                'uses' => 'TagController@criar', 'as' => 'criar'
            ]);
        });
        
        // Produtos
        Route::group(['as'=> 'produto::', 'prefix' => 'produto'], function () {
            // Usos
            Route::group(['as'=> 'uso::', 'prefix' => 'uso'], function () {
                // Categorias
                Route::group(['as'=> 'categoria::', 'prefix' => 'categoria'], function () {
                    // Itens
                    Route::group(['as'=> 'item::', 'prefix' => 'item'], function () {
                        Route::get('/{campo}/{valor?}', [
                            'uses' => 'ProdutoUsoCategoriaItemController@buscar', 'as' => 'buscar'
                        ]);
                        Route::get('/criar/{valor?}', [
                            'uses' => 'ProdutoUsoCategoriaItemController@criar', 'as' => 'criar'
                        ]);
                    });
                });
            });
        });
    });
});
