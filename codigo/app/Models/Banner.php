<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Banner extends Model
{
    
    /**
     *
     * @var string
     */
    protected $table = "banners";
    
    protected $guarded = ['id'];
    
    /**
     * Obtem o valor do atributo 'created_at'
     *
     * @param  string  $value
     * @return string
     */
    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->format('d/m/Y H:i:s');
    }
    
    /**
     *
     * @var string
     */
    public static $diretorio_imagens = 'images/upload/banners';
    
    public static $areas = [
        'home'  => 'Página inicial',
    ];
    
    /**
     * Cria um scopo para que a query retorne apenas banners cuja área seja $area
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeOfArea($query, $area)
    {
        return $query->where($this->table . '.area', $area);
    }
}