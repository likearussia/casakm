<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Contato extends Model
{
    /**
     *
     * @var string
     */
    protected $table = null;
    
    public static $assuntos = [
        'INFORMAÇÃO'  => 'Informação',
        'RECLAMAÇÃO'  => 'Reclamação',
        'SOLICITAÇÃO' => 'Solicitação',
        'OPINIÃO'     => 'Opinião'
    ];
}