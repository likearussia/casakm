<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Download extends Model
{
    use SoftDeletes;
    
    /**
     *
     * @var string
     */
    protected $table = "downloads";
    
    protected $guarded = ['id', 'deleted_at'];
    
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
    
    /**
     * Obtem o valor do atributo 'created_at'
     *
     * @param  string  $value
     * @return string
     */
    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->format('d/m/Y H:i:s');
    }
    
    /**
     * Use com storage_files()
     * e.g.: storage_files(Download::$diretorio_arquivos);
     * @var string
     */
    public static $diretorio_arquivos = 'app/downloads';
    
    /**
     *
     * @var string
     */
    public static $diretorio_imagens = 'images/downloads';
    
    public static $categorias = [
        'logomarcas'  => 'Logomarcas',
        'embalagens'  => 'Embalagens',
        'catalogos'   => 'Catálogos',
        'laminas'     => 'Lâminas'
    ];
    
    /**
     * Cria um scopo para que a query retorne apenas downloads com a slug fornecida
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeOfSlug($query, $slug)
    {
        return $query->where($this->table . '.slug', $slug);
    }
}