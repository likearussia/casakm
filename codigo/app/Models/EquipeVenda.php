<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EquipeVenda extends Model
{
    use SoftDeletes;
    
    /**
     *
     * @var string
     */
    protected $table = "equipe_vendas";
    
    protected $guarded = ['id', 'deleted_at'];
    
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
    
    /**
     * Obtem o valor do atributo 'created_at'
     *
     * @param  string  $value
     * @return string
     */
    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->format('d/m/Y H:i:s');
    }
    
    /**
     * Cria um scopo para que a query retorne apenas resultados cujo codigo for igual a $codigo
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeOfCodigo($query, $codigo)
    {
        return $query->where($this->table . '.codigo', $codigo);
    }
}