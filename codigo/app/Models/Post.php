<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Post extends Model
{
    use SoftDeletes;
    
    /**
     *
     * @var string
     */
    protected $table = "posts";
    
    public static $diretorio_imagens = 'images/upload/blog';
    
    protected $guarded = ['deleted_at'];
    
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
    
    /**
     * Define o tipo de relacionamento com o model Tag
     * 
     * @return Eloquent
     */
    public function tags()
    {
        //return $this->hasManyThrough('App\Models\Tag', 'App\Models\PostTag', 'tag_id');
        return $this->hasMany('App\Models\PostTag', 'post_id');
    }

    /**
     * Cria um scopo para que a query retorne apenas posts com a slug fornecida
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeOfSlug($query, $slug)
    {
        return $query->where( $this->table . '.slug', $slug);
    }
    
    /**
     * Obtem o valor do atributo 'created_at'
     *
     * @param  string  $value
     * @return string
     */
    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->format('d/m/Y H:i:s');
    }
}