<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class PostCategoria extends Model
{
    use SoftDeletes;
    
    /**
     *
     * @var string
     */
    protected $table = "post_categorias";
    
    protected $guarded = ['id', 'deleted_at'];
    
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
    
    /**
     * Obtem o valor do atributo 'created_at'
     *
     * @param  string  $value
     * @return string
     */
    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->format('d/m/Y H:i:s');
    }
    
    /**
     * Cria um scopo para que a query retorne apenas categorias com a slug fornecida
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeOfSlug($query, $slug)
    {
        return $query->where( $this->table . '.slug', $slug);
    }
    
     /**
     * Retorna os dados em um formato mais útil ao método que irá listar os dados deste modelo.
     * 
     * @param bool $retornar_array se true irá retornar um array
     * @return mixed array ou Eloquent
     */
    public static function listar($retornar_array = true)
    {
        $query = PostCategoria::select('id', 'nome');
        
        if ($retornar_array) {
            $retorno = [];
            $resultados = $query->get();
            foreach ($resultados as $resultado) {
                $retorno[$resultado->id] = $resultado->nome;
            }
            return $retorno;
        } else {
            return $query->get();
        }
    }
}