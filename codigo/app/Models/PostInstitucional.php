<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class PostInstitucional extends Model
{
    use SoftDeletes;
    
    /**
     *
     * @var string
     */
    protected $table = "posts_institucionais";
    
    public static $diretorio_imagens = 'images/upload/institucional';
    
    protected $guarded = ['deleted_at'];
    
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Cria um scopo para que a query retorne apenas posts com a slug fornecida
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeOfSlug($query, $slug)
    {
        return $query->where( $this->table . '.slug', $slug);
    }
    
    /**
     * Obtem o valor do atributo 'created_at'
     *
     * @param  string  $value
     * @return string
     */
    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->format('d/m/Y H:i:s');
    }
}