<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class PostTag extends Model
{
    use SoftDeletes;
    
    /**
     *
     * @var string
     */
    protected $table = "posts_tags";
    
    protected $guarded = ['deleted_at'];
    
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
    
    /**
     * Define o tipo de relacionamento com o model PostTag
     * 
     * @return Eloquent
     */
    public function tag()
    {
        return $this->belongsTo('App\Models\Tag', 'tag_id');
    }
    
    /**
     * Define o tipo de relacionamento com o model Post
     * 
     * @return Eloquent
     */
    public function post()
    {
        return $this->belongsTo('App\Models\Post', 'post_id');
    }
    
    /**
     * Retorna as tags de um post em um formato mais útil ao método que irá listá-las.
     * 
     * @param int $id id da tag
     * @param bool $retornar_array se true irá retornar um array
     * @return mixed array ou Eloquent
     */
    public static function listarTags($id, $retornar_array = true)
    {
        $query = PostTag::join('tags', 'tags.id', '=', 'posts_tags.tag_id')
            ->join('posts', 'posts.id', '=', 'posts_tags.post_id')
            ->select('tags.id', 'tags.tag')
            ->where('posts.id', '=', $id)
            ->orderBy('tags.tag');
        
        if ($retornar_array) {
            $retorno = [];
            $resultados = $query->get();
            foreach ($resultados as $resultado) {
                $retorno[$resultado->id] = $resultado->tag;
            }
            return $retorno;
        } else {
            return $query->get();
        }
    }
    
    /**
     * Atualiza as tags de um dado post.
     * 
     * Este método é utilizado pois são várias as tags para um mesmo produto.
     * 
     * @param int $id id do post
     * @param array $tags array contendo os ids das tags
     */
    public static function atualizar($id, $tags)
    {
        PostTag::where('post_id', $id)->delete();
        
        foreach ($tags as $tag) {
            PostTag::create([
                'post_id' => $id,
                'tag_id' => $tag,
            ]);
        }
        
        return true;
    }
}