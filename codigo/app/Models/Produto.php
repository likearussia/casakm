<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Produto;

class Produto extends Model
{
    use SoftDeletes;
    
    /**
     *
     * @var string
     */
    protected $table = "produtos";
    
    protected $guarded = ['id', 'deleted_at'];

    public static $diretorio_imagens = 'images/produtos';
    
    public static $diretorio_imagens_banners = 'images/produtos-banners';
    
    public static $diretorio_imagens_packs = 'images/produtos-packs';
    
    /**
     *
     * @var array contemos valores e descrições possíveis para o banco 'situacao' do Model/banco
     */
    public static $situacoes = [
        1 => 'Ativo',
        2 => 'Novo',
    ];
    
    /**
     * O método "booting" para o model, onde são determinadas ações globais para este model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        // Altero a ordem de listagem para todas as consultas onde a ordem não for especificada
        static::addGlobalScope('produtos-ordem', function(Builder $builder) {
            $builder->orderBy('produtos.ordem', 'ASC');
        });
    }
    
    /**
     * Define o relacionamento com o model ProdutoMarca
     * 
     * @return Eloquent
     */
    public function marca()
    {
        return $this->belongsTo('App\Models\ProdutoMarca', 'produto_marca_id');
    }
    
    /**
     * Define o relacionamento com o model ProdutoUso
     * 
     * @return Eloquent
     */
    public function usos()
    {
        return $this->hasMany('App\Models\ProdutoUso', 'produto_id');
    }
    
    /**
     * Define o relacionamento com o model ProdutoVersao
     * 
     * @return Eloquent
     */
    public function versoes()
    {
        return $this->hasMany('App\Models\ProdutoVersao', 'produto_id');
    }
    
    /**
     * Cria um scopo para que a query retorne apenas produtos com a slug fornecida
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeOfSlug($query, $slug)
    {
        return $query->where('slug', $slug);
    }
    
    /**
     * Cria um scopo para que a query retorne apenas produtos novos
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeNovo($query)
    {
        return $query->where('situacao', 2);
    }
    
    /**
     * Retorna os dados de uso do produto em um formato mais útil ao método que
     * irá listá-los.
     * 
     * @param int $id id do produto
     * @param bool $retornar_array se true irá retornar um array
     * @return mixed array ou Eloquent
     * @todo este método deve ficar no model ProdutoUso
     */
    public static function listarUsos($id, $retornar_array = true)
    {
        $query = Produto::join('produtos_usos', 'produtos.id', '=', 'produtos_usos.produto_id')
            ->join('produtos_usos_categoria_itens as item', 'produtos_usos.produtos_usos_categoria_item_id','=',
                'item.id')
            ->select('item.id', 'item.descricao')
            ->where('produtos.id', '=', $id)
            ->orderBy('item.descricao');
        
        if ($retornar_array) {
            $retorno = [];
            $resultados = $query->get();
            foreach ($resultados as $resultado) {
                $retorno[$resultado->id] = $resultado->descricao;
            }
            return $retorno;
        } else {
            return $query->get();
        }
    }
}