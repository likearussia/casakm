<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use App\Models\ProdutoMarca;

class ProdutoMarca extends Model
{
    /**
     *
     * @var string
     */
    protected $table = "produto_marcas";
    
    public static $diretorio_imagens = 'images/produtos-marcas';
    
    /**
     * O método "booting" para o model, onde são determinadas ações globais para este model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        // Altero a ordem de listagem para todas as consultas onde a ordem não for especificada
        static::addGlobalScope('produto_marcas-ordem', function(Builder $builder) {
            $builder->orderBy("produto_marcas.ordem", 'ASC');
        });
    }
    
    /**
     * Define o relacionamento com o model ProdutoMarca
     * 
     * @return Eloquent
     */
    public function produtos()
    {
        return $this->hasMany('App\Models\Produto', 'produto_marca_id');
    }
    
    /**
     * Cria um scopo para que a query retorne apenas marcas com a slug fornecida
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeOfSlug($query, $slug)
    {
        return $query->where('slug', $slug);
    }
    
    /**
     * Retorna os dados em um formato mais útil ao método que irá listar os dados deste modelo.
     * 
     * @param bool $retornar_array se true irá retornar um array
     * @return mixed array ou Eloquent
     */
    public static function listar($retornar_array = true)
    {
        $query = ProdutoMarca::select('id', 'nome');
        
        if ($retornar_array) {
            $retorno = [];
            $resultados = $query->get();
            foreach ($resultados as $resultado) {
                $retorno[$resultado->id] = $resultado->nome;
            }
            return $retorno;
        } else {
            return $query->get();
        }
    }
}