<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\ProdutoUso;

class ProdutoUso extends Model
{
    /**
     *
     * @var string
     */
    protected $table = "produtos_usos";
    
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;
    
    public $guarded = ['id'];
    
    /**
     * Define o relacionamento com o model Produto
     * 
     * @return Eloquent
     */
    public function produtos()
    {
        return $this->belongsTo('App\Models\Produto', 'produto_id');
    }
    
    /**
     * Define o relacionamento com o model ProdutoMarca
     * 
     * @return Eloquent
     */
    public function itens()
    {
        return $this->belongsTo('App\Models\ProdutoUsoCategoriaItem', 'produtos_usos_categoria_item_id');
    }
    
    /**
     * Atualiza os usos de um dado produto.
     * 
     * Este método é utilizado pois são vários usos para um mesmo produto.
     * 
     * @param int $id id do produto
     * @param array $itens array contendo os ids dos itens
     */
    public static function atualizar($id, $itens)
    {
        ProdutoUso::where('produto_id', $id)->delete();
        
        foreach ($itens as $item) {
            ProdutoUso::create([
                'produto_id' => $id,
                'produtos_usos_categoria_item_id' => $item,
            ]);
        }
        
        return true;
    }
}