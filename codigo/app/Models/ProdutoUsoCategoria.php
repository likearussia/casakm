<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class ProdutoUsoCategoria extends Model
{
    /**
     *
     * @var string
     */
    protected $table = "produtos_usos_categorias";
    
    /**
     * Define o relacionamento com o model ProdutoUsoCategoriaItem
     * 
     * @return Eloquent
     */
    public function itens()
    {
        return $this->hasMany('App\Models\ProdutoUsoCategoriaItem', 'produtos_usos_categoria_id');
        
    }
}