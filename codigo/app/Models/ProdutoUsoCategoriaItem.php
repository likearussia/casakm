<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class ProdutoUsoCategoriaItem extends Model
{
    /**
     *
     * @var string
     */
    protected $table = "produtos_usos_categoria_itens";
    
    /**
     * Define o relacionamento com o model ProdutoUsoCategoria
     * 
     * @return Eloquent
     */
    public function categoria()
    {
        return $this->belongsTo('App\Models\ProdutoUsoCategoria', 'produtos_usos_categoria_id');
        
    }
    
    /**
     * Cria um scopo para que a query retorne apenas itens (tipos de uso) com a slug fornecida
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeOfSlug($query, $slug)
    {
        return $query->where('slug', $slug);
    }
    
    public static $diretorio_imagens = 'images/produtos-usos-categorias-items';
}