<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use App\Models\ProdutoVersao;

class ProdutoVersao extends Model
{
    protected $table = "produtos_versoes";
    
    protected $guarded = ['id'];
    
    public $timestamps = false;
    
    public static $diretorio_imagens = 'images/produtos';
    
    /**
     * O método "booting" para o model, onde são determinadas ações globais para este model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        // Altero a ordem de listagem para todas as consultas onde a ordem não for especificada
        static::addGlobalScope('produtos-versoes-ordem', function(Builder $builder) {
            $builder->orderBy('produtos_versoes.ordem', 'ASC');
        });
    }
    
    /**
     * Define o relacionamento com o model Produto
     * 
     * @return Eloquent
     */
    public function produto()
    {
        return $this->belongsTo('App\Models\Produto', 'produto_id');
    }
    
    /**
     * Retorna as versões do produto em um formato mais útil ao método que irá exibí-los
     * 
     * @param int $id id do produto
     * @return Eloquent
     */
    public static function obterVersoes($id)
    {
        $query = ProdutoVersao::join('produtos', 'produtos.id', '=', 'produtos_versoes.produto_id')
            ->select('produtos_versoes.id', 'produtos_versoes.nome', 'produtos_versoes.imagem')
            ->where('produtos.id', '=', $id)
            ->orderBy('produtos_versoes.nome');
        
        return $query->get();
    }
}
