<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RecursoHumano extends Model
{
    /**
     *
     * @var string
     */
    protected $table = null;
    
    public static $departamentos = [
        'Administração de  Vendas' => 'Administração de  Vendas',
        'Administrativo' => 'Administrativo',
        'Almoxarifado' => 'Almoxarifado',
        'Assuntos Regulatórios' => 'Assuntos Regulatórios',
        'Comercial' => 'Comercial',
        'Compras' => 'Compras',
        'Contábil - Fiscal' => 'Contábil - Fiscal',
        'Controladoria' => 'Controladoria',
        'Customer Service' => 'Customer Service',
        'Engenharia' => 'Engenharia',
        'Garantia de Qualidade' => 'Garantia de Qualidade',
        'Jurídico' => 'Jurídico',
        'Logística' => 'Logistica',
        'Manutenção' => 'Manutenção',
        'Marketing' => 'Marketing',
        'Pesquisa e Desenvolvimento - Embalagem' => 'Pesquisa e Desenvolvimento - Embalagem',
        'Pesquisa e Desenvolvimento - Produto' => 'Pesquisa e Desenvolvimento - Produto',
        'Produção' => 'Produção',
        'Qualidade' => 'Qualidade',
        'Recursos Humanos' => 'Recursos Humanos',
        'SAC - Atendimento ao Consumidor' => 'SAC - Atendimento ao Consumidor',
        'TI - Tecnologia da Informação' => 'TI - Tecnologia da Informação',
        'Trade Marketing' => 'Trade Marketing',
    ];
}