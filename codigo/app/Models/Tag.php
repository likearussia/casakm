<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Tag extends Model
{
    use SoftDeletes;
    
    /**
     *
     * @var string
     */
    protected $table = "tags";
    
    protected $guarded = ['id', 'deleted_at'];
    
    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
    
    /**
     * Obtem o valor do atributo 'created_at'
     *
     * @param  string  $value
     * @return string
     */
    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->format('d/m/Y H:i:s');
    }
    
    /**
     * Cria um scopo para que a query retorne apenas categorias com a slug fornecida
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeOfSlug($query, $slug)
    {
        return $query->where( $this->table . '.slug', $slug);
    }
    
    /**
     * Define o tipo de relacionamento com o model PostTag
     * 
     * @return Eloquent
     */
    public function postsTags()
    {
        return $this->belongsTo('App\Models\PostTag', 'tag_id');
    }
}