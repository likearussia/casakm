<?php

namespace App\Services;

use App\Models\Produto;
use App\Models\ProdutoMarca;

class MapaSiteService
{
    public function __construct(Produto $produto) {
        $this->produto = $produto;
    }
    
    public static function rodape()
    {
        $dados = [];
        
        $dados['produtos'] = Produto::select(['nome', 'slug'])->get();
        $dados['marcas'] = ProdutoMarca::select(['nome', 'slug'])->get();
        
        return $dados;
    }
}