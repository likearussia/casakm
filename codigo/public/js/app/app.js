/**
 * Lógica global para a aplicação
 */

$(document).ready(function() {
    $.material.init();
    
    // Ativa o efeito criado pelo material.js de uma maneira que torna
    // mais fácil para o usuário ver a descrição do campo
    // Markup: form-group grupo-upload-imagem
    $('.grupo-upload-imagem')
        .mouseenter(function() {
            $(this).closest('.form-group').addClass('is-focused');
        })
        .mouseout(function() {
            $(this).closest('.form-group').removeClass('is-focused');
        });
    
    $('.dialogo-deletar').on('click', function(e) {
        e.preventDefault();
        
        href = $(this).attr('href');
        
        swal({
            title: 'Você tem certeza ?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#0fb2fc',
            cancelButtonColor: '#d41414',
            confirmButtonText: 'Sim',
            cancelButtonText: 'Cancelar'
        }).then(function() {
            if (href) {
                window.location.href = href;
            }
        });
    });
});