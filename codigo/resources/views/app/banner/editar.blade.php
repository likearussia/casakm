@extends('layouts.app')

@section('conteudo')
    <div class="container-fluid container-principal">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="conteudo">
                        <div class="page-header">
                            <h1>
                                <i class="icone-padrao material-icons">image</i>
                                Banner
                            </h1>
                            <ol class="breadcrumb">
                                <li><a href="{{ route('app::inicio') }}">Dashboard</a></li>
                                <li><a href="{{ route('app::banner::index') }}">Banners</a></li>
                                <li class="active">Editar</li>
                            </ol>
                        </div>

                        @include('shared.erro-validacao')
                        @include('flash::message')

                        {{ Form::model($banner, ['route' => ['app::banner::salvar', $banner->id], 'files' => true,
                                    'class' => 'form-horizontal']) }}
                            <div class="form-group">
                                <label for="banner" class="control-label">Imagem</label>
                                <div class="col-md-12 grupo-upload-imagem">
                                    <input readonly="" class="form-control" placeholder="Procurar..." type="text">
                                    <input id="banner" name="banner" multiple="" type="file">
                                    <span class="help-block">Selecione uma banner no formato <strong>JPG</strong> com a resolução de <strong>1900x463</strong> pixels.</span>
                                    <br/><br/>
                                    <img class="miniatura" src="{{ $diretorio_imagens . '/' . $banner->banner }}"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="texto" class="control-label">Texto</label>
                                {{ Form::textarea('texto', $banner->texto, ['class' => 'form-control', 'rows' => 1]) }}
                            </div>
                            <div class="form-group">
                                <label for="texto" class="control-label">Área</label>
                                {{ Form::select('area', $areas, $banner->area, ['placeholder' => 'Selecione', 'required', 'class' => 'form-control']) }}
                            </div>
                                
                            <div class="form-group">
                                <label for="link_texto" class="control-label">Texto do link</label>
                                {{ Form::textarea('link_texto', $banner->link_texto, ['class' => 'form-control', 'rows' => 1]) }}
                                <span class="help-block">A qual marca este produto pertence?</span>
                            </div>
                            <div class="form-group">
                                <label for="link_url" class="control-label">Link</label>
                                {{ Form::text('link_url', $banner->link_url, ['class' => 'form-control']) }}
                            </div>
                            <div class="form-group">
                                <div class="togglebutton">
                                    <label>
                                        @if ($banner->link_nova_aba == 1)
                                            <input checked="" name="link_nova_aba" type="checkbox"> Link abre em nova aba ?
                                        @else
                                            <input name="link_nova_aba" type="checkbox"> Link abre em nova aba ?
                                        @endif
                                    </label>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-primary btn-raised">Salvar</button>
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection