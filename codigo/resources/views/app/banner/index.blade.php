@extends('layouts.app')

@section('conteudo')
    <div class="container-fluid container-principal">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="conteudo">
                        <div class="page-header">
                            <h1>
                                <i class="icone-padrao material-icons">image</i>
                                Banners
                            </h1>
                            <ol class="breadcrumb">
                                <li><a href="{{ route('app::inicio') }}">Início</a></li>
                                <li class="active">Banners</li>
                            </ol>
                        </div>

                        @include('shared.erro-validacao')
                        @include('flash::message')
                        
                        <table class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th class="text-center">Criado em</th>
                                    <th class="text-center">Texto</th>
                                    <th class="text-center">Área</th>
                                    <th class="text-center">URL</th>
                                    <th colspan="2" class="text-center">Ações</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($banners as $banner)
                                    <tr>
                                        <td class="text-center">{{ $banner->created_at }}</td>
                                        <td class="text-center">
                                            <a href="{{ route('app::banner::editar', ['id' => $banner->id]) }}">
                                                {{ $banner->texto }}
                                            </a>
                                        </td>
                                        <td class="text-center">
                                            {{ $areas[$banner->area] }}
                                        </td>
                                        <td class="text-center">
                                            {{ $banner->link_url }}
                                        </td>
                                        <td class="text-right">
                                            <a href="{{ route('app::banner::editar', ['id' => $banner->id]) }}" title="Editar">
                                                <i class="icone-editar material-icons" title="Editar">edit</i>
                                            </a>
                                        </td>
                                        <td class="text-left">
                                            <a href="{{ route('app::banner::deletar', ['id' => $banner->id]) }}" title="Deletar" class="dialogo-deletar">
                                                <i class="icone-deletar material-icons" title="Deletar">delete</i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        
                        <div class="row">
                            <div class="col-md-12">
                                @if ($banners->total() > 0)
                                    Exibindo {{ $banners->count() }} de {{ $banners->total() }} banners encontrados.
                                @else
                                    Nenhum banner encontrado.
                                @endif
                            </div>
                        </div>
                        
                        <div class="paginacao text-center">
                            {!! $banners->links() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
