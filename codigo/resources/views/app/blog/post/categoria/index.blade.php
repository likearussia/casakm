@extends('layouts.app')

@section('conteudo')
    <div class="container-fluid container-principal">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="conteudo">
                        <div class="page-header">
                            <h1>
                                <i class="icone-padrao material-icons">forum</i>
                                Categorias
                            </h1>
                            <ol class="breadcrumb">
                                <li><a href="{{ route('app::inicio') }}">Início</a></li>
                                <li><a href="{{ route('app::blog::post::index') }}">Casa em dia</a></li>
                                <li class="active">Categorias</li>
                            </ol>
                        </div>

                        @include('flash::message')
                        
                        <table class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th class="text-center">Criada em</th>
                                    <th class="text-center">Nome</th>
                                    <th colspan="2" class="text-center">Ações</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($categorias as $categoria)
                                    <tr>
                                        <td class="text-center">{{ $categoria->created_at }}</td>
                                        <td class="text-center">
                                            <a href="{{ route('app::blog::post::categoria::editar', ['id' => $categoria->id]) }}">
                                                {{ $categoria->nome }}
                                            </a>
                                        </td>
                                        <td class="text-right">
                                            <a href="{{ route('app::blog::post::categoria::editar', ['id' => $categoria->id]) }}" title="Editar">
                                                <i class="icone-editar material-icons" title="Editar">edit</i>
                                            </a>
                                        </td>
                                        <td class="text-left">
                                            <a href="{{ route('app::blog::post::categoria::deletar', ['id' => $categoria->id]) }}" title="Deletar" class="dialogo-deletar">
                                                <i class="icone-deletar material-icons" title="Deletar">delete</i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        
                        <div class="row">
                            <div class="col-md-12">
                                @if ($categorias->total() > 0)
                                    Exibindo {{ $categorias->count() }} de {{ $categorias->total() }} categorias encontradas.
                                @else
                                    Nenhuma categoria encontrada.
                                @endif
                            </div>
                        </div>
                        
                        <div class="paginacao text-center">
                            {!! $categorias->links() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
