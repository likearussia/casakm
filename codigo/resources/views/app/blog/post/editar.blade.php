@extends('layouts.app')

@section('conteudo')
<meta name="csrf-token" content="{{ csrf_token() }}">
    <div class="container-fluid container-principal">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="conteudo">
                        <div class="page-header">
                            <h1>
                                <i class="icone-padrao material-icons">forum</i>
                                Blog
                            </h1>
                            <ol class="breadcrumb">
                                <li><a href="{{ route('app::inicio') }}">Dashboard</a></li>
                                <li><a href="{{ route('app::blog::post::index') }}">Casa em dia</a></li>
                                <li class="active">Editar publicação</li>
                            </ol>
                        </div>

                        @include('flash::message')
                    <form enctype="multipart/form-data" class="form-image">
                        
                            <div class="popup-crop col-md-12">
                                <div class='canvas-result'>
                                </div>
                                <div class="imagem_crop col-md-12" style="display: none">
                                    <img src="" class="image-uploaded col-md-11"/>
                                    <input class="uploaded-image" type="hidden" name="caminho-uploaded"/>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-primary cropp"  data-method="crop" title="Crop">
                                            <span class="docs-tooltip" data-toggle="tooltip" title="" data-original-title="cropper.crop()">
                                                <span class="fa fa-check"></span>
                                                cortar imagem
                                            </span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>

                        
                        {{ Form::model($post, ['route' => ['app::blog::post::editar', $post->id], 'files' => true,
                                    'class' => 'form-horizontal col-md-12']) }}
                            <fieldset>
                                <div class="form-group">
                                    <label for="titulo" class="control-label">Título</label>
                                    <div class="col-md-12">
                                        {{ Form::text('titulo', null, ['class' => 'form-control',
                                            'required' => 'required', 'autocomplete' => 'off',
                                            'placeholder' => 'Título']) }}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="imagem_destaque" class="control-label">Imagem destaque</label>
                                    <div class="col-md-12 grupo-upload-imagem">
                                        <input readonly="" class="form-control" placeholder="Procurar..." type="text">
                                        <input id="imagemDestque" name="imagem_destaque" multiple="" type="file">
                                        <span class="help-block">Selecione uma imagem no formato <strong>PNG</strong> com a resolução de <strong>500x300</strong> pixels.</span>
                                        <br/><br/>
                                        <img class="miniatura" src="{{ $diretorio_imagens . '/' . $post->imagem_destaque }}"/>
                                    </div>
                                </div>
                                <div class="image-def">
                                    <input type="hidden" name="simage" class="simage"/>
                                    <input type="hidden" name="ximage" class="ximage"/>
                                    <input type="hidden" name="yimage" class="yimage"/>
                                    <input type="hidden" name="wimage" class="wimage"/>
                                    <input type="hidden" name="himage" class="himage"/>
                                </div>
                                <div class="form-group">
                                    <label for="texto" class="control-label">Texto</label>
                                    {{ Form::textarea('texto', $post->texto, ['id' => 'texto',
                                        'class' => 'summernote']) }}
                                </div>
                                <div class="form-group">
                                    <label for="categoria" class="control-label">Categoria</label>
                                    <div class="col-md-12">
                                    {{ Form::select('post_categoria_id', $categorias, null,
                                                ['placeholder' => 'Categoria', 'id' => 'post_categoria_id',
                                                'class' => 'form-control', 'required']) }}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="tags" class="control-label">Tags</label>
                                    <div class="col-md-12">
                                        <select name="tags[]" id="tags" multiple="" class="form-control">
                                            @foreach ($tags as $id => $tag)
                                                <option selected value="{{ $id }}">{{ $tag }}</option>
                                            @endforeach
                                        </select>
                                        <span class="help-block">Tags ajudam a classificar suas publicações</span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <div class="togglebutton">
                                            <label>
                                                @if ($post->permitir_comentarios == 1)
                                                    <input checked="" name="permitir_comentarios" type="checkbox"> Permitir comentários ?
                                                @else
                                                    <input name="permitir_comentarios" type="checkbox"> Permitir comentários ?
                                                @endif
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <button type="submit" class="btn btn-primary btn-raised">Salvar</button>
                                    </div>
                                </div>
                            </fieldset>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection

@push('css')
    <link href="dist/summernote/summernote.css" rel="stylesheet" type="text/css">
    <link href="dist/selectize/css/selectize.css" rel="stylesheet" type="text/css">
@endpush

@push('scripts')
    <script src="dist/summernote/summernote.min.js"></script>
    <script src="dist/summernote/lang/summernote-pt-BR.min.js"></script>
    <script src="dist/selectize/js/standalone/selectize.min.js"></script>
    
    <script src="http://malsup.github.com/jquery.form.js"></script> 
    <link  href="/cropper/cropper.css" rel="stylesheet">
    <script src="/cropper/cropper.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            
$('.cropp').click(function () {
$('.canvas-result').html(" ");
$('.canvas-result').append($('.image-uploaded').cropper("getCroppedCanvas"));
$('.cropper-container').hide();
});
$('.image-upload').change(function () {

var data = $(this).val();
$('.form-image').ajaxForm({
type: 'post',
        headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: '{{route('app::imagem')}}',
        success: function (resp) {
        console.log(resp);
        setTimeout(function () {
        $('.txt-image').text(" ");
        $('.image-uploaded').attr('src', '');
        $('.image-uploaded').attr('src', 'tmp_uploads/' + resp);
        $('.simage').val('tmp_uploads/' + resp);
        $('.uploaded-image').val('tmp_uploads/' + resp);
        $('.imagem_crop').fadeIn();
        $('.image-uploaded').cropper({
        aspectRatio: 17.3 / 9,
        zoomable: false,
        movable: false,
                crop: function (e) {
                $('.ximage').val(e.x);
                $('.yimage').val(e.y);
                $('.wimage').val(e.width);
                $('.himage').val(e.height);
                }
        });
        }, 300)
        },
        error: function (resp) {
        console.log(resp);
        },
        beforeSend: function () {
        $('.popup-crop').append('<p class="txt-image">Carregando...</p>');
        }
}).submit();
});
            opcoes_summernote = {
                minHeight: 100,
                focus: false,
                placeholder: 'Escreva aqui...',
                toolbar: [
                    // [groupName, [list of button]]
                    ['style', ['bold', 'italic', 'underline', 'clear']],
                    ['font', ['strikethrough', 'superscript', 'subscript']],
                    ['fontsize', ['fontsize']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['height', ['height']],
                    ['insert', ['link', 'video']],
                    ['misc', ['fullscreen']]
                ],
                fontNames: [''],
                lang: 'pt-BR',
                callbacks: {
                    // workaround para habilitar o foco que é normalmente feito pelo material.js
                    onFocus: function() {
                        $(this).parent().addClass('is-focused');
                    },
                    onBlur: function() {
                        $(this).parent().removeClass('is-focused');
                    }
                }
            };
            var $elementoSummernote = $('.summernote');
            $elementoSummernote.summernote(opcoes_summernote);
            
            $('#tags').selectize({
                plugins: ['remove_button'],
                create: false,
                maxItems: null,
                delimiter: ',',
                valueField: 'id',
                labelField: 'tag',
                searchField: ['tag'],
                load: function(query, callback) {
                    if (!query.length) return callback();
                    $.ajax({
                        url:  '{{ route('api::app::tag::buscar', ['campo' => 'tag']) }}/' + encodeURIComponent(query),
                        type: 'GET',
                        error: function() {
                            callback();
                        },
                        success: function(response) {
                            callback(response.tags.slice(0, 10));
                        }
                    });
                },
                onOptionAdd: function(value, data) {
                    $.ajax({
                        url:  '{{ route('api::app::tag::criar') }}/' + encodeURIComponent(value),
                        type: 'GET',
                        error: function() {
                            alert('Erro ao criar a tag')
                        },
                        success: function(response) {
                            console.log(response);
                            return null;
                        },
                        done: function(a) {
                            console.log('done');
                            console.log(a);
                        }
                    });
                }
            });
        });
    </script>
@endpush
