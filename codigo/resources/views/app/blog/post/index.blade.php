@extends('layouts.app')

@section('conteudo')
    <div class="container-fluid container-principal">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="conteudo">
                        <div class="page-header">
                            <h1>
                                <i class="icone-padrao material-icons">forum</i>
                                Blog
                            </h1>
                            <ol class="breadcrumb">
                                <li><a href="{{ route('app::inicio') }}">Início</a></li>
                                <li class="active">Casa em dia</li>
                            </ol>
                        </div>

                        @include('flash::message')
                        
                        <table class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th class="text-center">Publicado em</th>
                                    <th class="text-center">Título</th>
                                    <th colspan="2" class="text-center">Ações</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($posts as $post)
                                    <tr>
                                        <td class="text-center">{{ $post->created_at }}</td>
                                        <td class="text-center">
                                            <a href="{{ route('app::blog::post::editar', ['id' => $post->id]) }}">
                                                {{ $post->titulo }}
                                            </a>
                                        </td>
                                        <td class="text-right">
                                            <a href="{{ route('app::blog::post::editar', ['id' => $post->id]) }}" title="Editar">
                                                <i class="icone-editar material-icons" title="Editar">edit</i>
                                            </a>
                                        </td>
                                        <td class="text-left">
                                            <a href="{{ route('app::blog::post::deletar', ['id' => $post->id]) }}" title="Deletar" class="dialogo-deletar">
                                                <i class="icone-deletar material-icons" title="Deletar">delete</i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        
                        <div class="row">
                            <div class="col-md-12">
                                @if ($posts->total() > 0)
                                    Exibindo {{ $posts->count() }} de {{ $posts->total() }} publicações encontradas.
                                @else
                                    Nenhuma publicação encontrada.
                                @endif
                            </div>
                        </div>
                        
                        <div class="paginacao text-center">
                            {!! $posts->links() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
