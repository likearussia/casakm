@extends('layouts.app')

@section('conteudo')
    <div class="container-fluid container-principal">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="conteudo">
                        <div class="page-header">
                            <h1>
                                <i class="icone-padrao material-icons">label</i>
                                Tag
                            </h1>
                            <ol class="breadcrumb">
                                <li><a href="{{ route('app::inicio') }}">Dashboard</a></li>
                                <li><a href="{{ route('app::blog::post::index') }}">Casa em dia</a></li>
                                <li><a href="{{ route('app::blog::post::tag::index') }}">Tags</a></li>
                                <li class="active">Nova</li>
                            </ol>
                        </div>

                        @include('flash::message')

                        {{ Form::open(['route' => ['app::blog::post::tag::criar'],
                                    'class' => 'form-horizontal']) }}
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label for="nome" class="control-label">Tag</label>
                                    {{ Form::text('tag', null, ['placeholder' => 'Tag', 'id' => 'tag',
                                                    'required', 'class' => 'form-control']) }}
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-primary btn-raised">Salvar</button>
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection