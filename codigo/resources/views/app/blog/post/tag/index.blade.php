@extends('layouts.app')

@section('conteudo')
    <div class="container-fluid container-principal">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="conteudo">
                        <div class="page-header">
                            <h1>
                                <i class="icone-padrao material-icons">label</i>
                                Tags
                            </h1>
                            <ol class="breadcrumb">
                                <li><a href="{{ route('app::inicio') }}">Início</a></li>
                                <li><a href="{{ route('app::blog::post::index') }}">Casa em dia</a></li>
                                <li class="active">Tags</li>
                            </ol>
                        </div>

                        @include('flash::message')
                        
                        <table class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th class="text-center">Criada em</th>
                                    <th class="text-center">Nome</th>
                                    <th colspan="2" class="text-center">Ações</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($tags as $tag)
                                    <tr>
                                        <td class="text-center">{{ $tag->created_at }}</td>
                                        <td class="text-center">
                                            <a href="{{ route('app::blog::post::tag::editar', ['id' => $tag->id]) }}">
                                                {{ $tag->tag }}
                                            </a>
                                        </td>
                                        <td class="text-right">
                                            <a href="{{ route('app::blog::post::tag::editar', ['id' => $tag->id]) }}" title="Editar">
                                                <i class="icone-editar material-icons" title="Editar">edit</i>
                                            </a>
                                        </td>
                                        <td class="text-left">
                                            <a href="{{ route('app::blog::post::tag::deletar', ['id' => $tag->id]) }}" title="Deletar" class="dialogo-deletar">
                                                <i class="icone-deletar material-icons" title="Deletar">delete</i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        
                        <div class="row">
                            <div class="col-md-12">
                                @if ($tags->total() > 0)
                                    Exibindo {{ $tags->count() }} de {{ $tags->total() }} tags encontradas.
                                @else
                                    Nenhuma tag encontrada.
                                @endif
                            </div>
                        </div>
                        
                        <div class="paginacao text-center">
                            {!! $tags->links() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
