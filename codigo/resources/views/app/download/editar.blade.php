@extends('layouts.app')

@section('conteudo')
    <div class="container-fluid container-principal">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="conteudo">
                        <div class="page-header">
                            <h1>
                                <i class="icone-padrao material-icons">cloud_download</i>
                                Download
                            </h1>
                            <ol class="breadcrumb">
                                <li><a href="{{ route('app::inicio') }}">Dashboard</a></li>
                                <li><a href="{{ route('app::download::index') }}">Downloads</a></li>
                                <li class="active">Editar</li>
                            </ol>
                        </div>

                        @include('flash::message')

                        {{ Form::model($download, ['class' => 'form-horizontal', 'files' => true]) }}
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label for="nome" class="control-label">Nome</label>
                                    {{ Form::text('nome', null, ['placeholder' => 'Nome', 'id' => 'nome',
                                                    'required', 'class' => 'form-control']) }}
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label for="categoria" class="control-label">Categoria</label>
                                    {{ Form::select('categoria', $categorias, null, ['placeholder' => 'Categoria',
                                                    'id' => 'categoria', 'required', 'class' => 'form-control']) }}
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label for="arquivo" class="control-label">Arquivo</label>
                                    <input readonly="" class="form-control" placeholder="Procurar..." type="text">
                                    <input id="arquivo" name="arquivo" multiple="" type="file">
                                    <p>
                                        Arquivo atual: {{ $download->arquivo }}
                                    </p>
                                    <span class="help-block">
                                        Por favor, observe que o arquivo mais as outras informações não devem passar de {{ $maximo_post }}.
                                    </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <div class="togglebutton">
                                        <label>
                                            @if ($download->eh_restrito == 1)
                                                <input checked="" name="eh_restrito" type="checkbox"> Restrito ?
                                            @else
                                                <input name="eh_restrito" type="checkbox"> Restrito ?
                                            @endif
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-primary btn-raised">Salvar</button>
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection