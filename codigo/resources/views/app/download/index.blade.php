@extends('layouts.app')

@section('conteudo')
    <?php 
    function human_filesize($bytes, $decimals = 2) {
        $sz = 'BKMGTP';
        $factor = floor((strlen($bytes) - 1) / 3);
        return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . @$sz[$factor];
    } ?>
    <div class="container-fluid container-principal">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="conteudo">
                        <div class="page-header">
                            <h1>
                                <i class="icone-padrao material-icons">cloud_download</i>
                                Downloads
                            </h1>
                            <ol class="breadcrumb">
                                <li><a href="{{ route('app::inicio') }}">Início</a></li>
                                <li class="active">Downloads</li>
                            </ol>
                        </div>

                        @include('flash::message')
                        
                        <table class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th class="text-center">Nome</th>
                                    <th class="text-center">Nome do arquivo</th>
                                    <th class="text-center">Categoria</th>
                                    <th class="text-center">Tamanho do arquivo</th>
                                    <th class="text-center">É restrito?</th>
                                    <th colspan="3" class="text-center">Ações</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($downloads as $download)
                                    <tr>
                                        <td class="text-center">
                                            <a href="{{ route('app::download::editar', ['id' => $download->id]) }}">
                                                {{ $download->nome }}
                                            </a>
                                        </td>
                                        <td class="text-center">
                                            {{ $download->arquivo }}
                                        </td>
                                        <td class="text-center">
                                            {{ $categorias[$download->categoria] }}
                                        </td>
                                        <td class="text-center">
                                            {{ human_filesize(filesize($path_arquivos . '/' . $download->arquivo)) }}
                                        </td>
                                        <td class="text-center">
                                            {{ ($download->eh_restrito == 1) ? 'Sim' : 'Não' }}
                                        </td>
                                        <td class="text-right">
                                            <a href="{{ route('download::download', ['slug' => $download->slug]) }}" title="Download">
                                                <i class="icone-padrao material-icons" title="Download">file_download</i>
                                            </a>
                                        </td>
                                        <td class="text-center">
                                            <a href="{{ route('app::download::editar', ['id' => $download->id]) }}" title="Editar">
                                                <i class="icone-editar material-icons" title="Editar">edit</i>
                                            </a>
                                        </td>
                                        <td class="text-left">
                                            <a href="{{ route('app::download::deletar', ['id' => $download->id]) }}" title="Deletar">
                                                <i class="icone-deletar material-icons" title="Deletar">delete</i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        
                        <div class="row">
                            <div class="col-md-12">
                                @if ($downloads->total() > 0)
                                    Exibindo {{ $downloads->count() }} de {{ $downloads->total() }} downloads encontrados.
                                @else
                                    Nenhum download encontrado.
                                @endif
                            </div>
                        </div>
                        
                        <div class="paginacao text-center">
                            {!! $downloads->links() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
