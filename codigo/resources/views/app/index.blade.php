@extends('layouts.app')

@section('conteudo')
    <div class="container-fluid container-principal">
       <div class="container">
           <div class="row">
               <div class="col-md-12">
                   <h1>Bem-vindo, {{ Auth::user()->nome }}!</h1>
               </div>
           </div>
           <div class="row">
               <div class="col-md-12">
                   <img class="img-responsive" src="images/borboleta.gif"/>
               </div>
           </div>
        </div>
    </div>
@endsection
