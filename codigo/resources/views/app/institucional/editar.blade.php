@extends('layouts.app')

@section('conteudo')
    <div class="container-fluid container-principal">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="conteudo">
                        <div class="page-header">
                            <h1>
                                <i class="icone-padrao material-icons">work</i>
                                Institucional
                            </h1>
                            <ol class="breadcrumb">
                                <li><a href="{{ route('app::inicio') }}">Dashboard</a></li>
                                <li><a href="{{ route('app::institucional::index') }}">Institucional</a></li>
                                <li class="active">Editar</li>
                            </ol>
                        </div>

                        @include('shared.erro-validacao')
                        @include('flash::message')

                        {{ Form::model($post, ['route' => ['app::institucional::editar', $post->id], 'files' => true,
                                    'class' => 'form-horizontal']) }}
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label for="titulo" class="control-label">Título</label>
                                    {{ Form::text('titulo', null, ['placeholder' => 'Título', 'id' => 'titulo',
                                                    'required', 'class' => 'form-control']) }}
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="banner" class="control-label">Banner</label>
                                <div class="col-md-12 grupo-upload-imagem">
                                    <input readonly="" class="form-control" placeholder="Procurar..." type="text">
                                    <input id="banner" name="banner" multiple="" type="file">
                                    <span class="help-block">Selecione uma banner no formato <strong>JPG</strong> com a resolução de <strong>1635x350</strong> pixels.</span>
                                    <br/><br/>
                                    <img class="miniatura" src="{{ $diretorio_imagens . '/' . $post->banner }}"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="texto" class="control-label">Texto</label>
                                {{ Form::textarea('texto', $post->texto, ['id' => 'texto',
                                    'class' => 'summernote']) }}
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-primary btn-raised">Salvar</button>
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('css')
    <link href="dist/summernote/summernote.css" rel="stylesheet" type="text/css">
@endpush

@push('scripts')
    <script src="dist/summernote/summernote.min.js"></script>
    <script src="dist/summernote/lang/summernote-pt-BR.min.js"></script>
    
    <script type="text/javascript">
        $(document).ready(function () {
            opcoes_summernote = {
                minHeight: 100,
                focus: false,
                toolbar: [
                    // [groupName, [list of button]]
                    ['style', ['bold', 'italic', 'underline', 'clear']],
                    ['font', ['strikethrough', 'superscript', 'subscript']],
                    ['fontsize', ['fontsize']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['height', ['height']],
                    ['insert', ['link', 'video', 'picture']],
                    ['misc', ['fullscreen']]
                ],
                fontNames: [''],
                lang: 'pt-BR',
                callbacks: {
                    // workaround para habilitar o foco que é normalmente feito pelo material.js
                    onFocus: function() {
                        $(this).parent().addClass('is-focused');
                    },
                    onBlur: function() {
                        $(this).parent().removeClass('is-focused');
                    }
                }
            };
            var $elementoSummernote = $('.summernote');
            $elementoSummernote.summernote(opcoes_summernote);
        });
    </script>
@endpush
