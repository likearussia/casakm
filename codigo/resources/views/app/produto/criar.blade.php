@extends('layouts.app')

@section('conteudo')
    <div class="container-fluid container-principal">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="conteudo">
                        <div class="page-header">
                            <h1>
                                <i class="icone-padrao material-icons">store</i>
                                Produtos
                            </h1>
                            <ol class="breadcrumb">
                                <li><a href="{{ route('app::inicio') }}">Dashboard</a></li>
                                <li><a href="{{ route('app::produto::index') }}">Produtos</a></li>
                                <li class="active">Editar</li>
                            </ol>
                        </div>

                        @include('flash::message')

                        <!-- Menu das abas -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li role="presentation" class="active"><a href="#aba-geral" aria-controls="aba-geral" role="tab" data-toggle="tab">Geral</a></li>
                            <li role="presentation"><a href="#aba-imagens" aria-controls="aba-imagens" role="tab" data-toggle="tab">Imagens</a></li>
                            <li role="presentation"><a href="#aba-versoes" aria-controls="aba-versoes" role="tab" data-toggle="tab">Versões</a></li>
                        </ul>

                        {{ Form::open(['route' => ['app::produto::criar'], 'files' => true,
                                    'class' => 'form-horizontal']) }}

                            <!-- Paines com abas -->
                            <div class="tab-content"> <!-- .tab-content -->
                                
                                <div role="tabpanel" class="tab-pane active" id="aba-geral"> <!-- #aba-geral -->
                                    <div class="form-group label-static">
                                        <label for="nome" class="control-label">Nome</label>
                                        <div class="col-md-12">
                                            {{ Form::text('nome', null, ['id' => 'nome', 'class' => 'form-control',
                                                        'required' => 'required', 'autocomplete' => 'off']) }}
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-4">
                                            <label for="produto_marca_id" class="control-label">Marca</label>
                                            {{ Form::select('produto_marca_id', $marcas, null,
                                                        ['placeholder' => 'Marca', 'id' => 'produto_marca_id',
                                                            'required', 'class' => 'form-control']) }}
                                            <span class="help-block">A qual marca este produto pertence?</span>
                                        </div>

                                        <div class="col-md-4">
                                            <label for="situacao" class="control-label">Situação</label>
                                            {{ Form::select('situacao', $situacoes, null,
                                                        ['placeholder' => 'Situação', 'id' => 'situacao',
                                                            'class' => 'form-control']) }}
                                            <span class="help-block">Possibitita, dentra outras coisas, marcar um produto como novo.</span>
                                        </div>

                                        <div class="col-md-4">
                                            <label for="ordem" class="control-label">Ordem</label>
                                            {{ Form::select('ordem', range(0 , $produto_ordem_maxima), null,
                                                        ['placeholder' => 'Ordem', 'id' => 'ordem',
                                                            'class' => 'form-control']) }}
                                            <span class="help-block">Ordem utilizada nas listagens de produtos.</span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="descricao_curta" class="control-label">Descrição curta</label> <br/>
                                        {{ Form::textarea('descricao_curta', null,
                                                    ['id' => 'descricao_curta', 'class' => 'summernote form-control']) }}
                                        <span class="help-block">Um resumo sobre o produto</span>
                                    </div>
                                    <div class="form-group">
                                        <label for="descricao_longa" class="control-label">Descrição longa</label> <br/>
                                        {{ Form::textarea('descricao_longa', null,
                                                    ['id' => 'descricao_longa', 'class' => 'summernote form-control']) }}
                                    </div>
                                    <div class="form-group">
                                        <label for="caracteristicas" class="control-label">Características</label> <br/>
                                        {{ Form::textarea('caracteristicas', null,
                                                    ['id' => 'caracteristicas', 'class' => 'summernote form-control']) }}
                                        <span class="help-block">Quais as características deste produto?</span>
                                    </div>
                                    <div class="form-group">
                                        <label for="informacoes_uso" class="control-label">Informações de uso</label> <br/>
                                        {{ Form::textarea('informacoes_uso', null,
                                                    ['id' => 'informacoes_uso', 'class' => 'summernote form-control']) }}
                                        <span class="help-block">Quais as informações a respeito do uso deste produto ?</span>
                                    </div>
                                    <div class="form-group">
                                        <label for="informacoes_seguranca" class="control-label">Informações de segurança</label> <br/>
                                        {{ Form::textarea('informacoes_seguranca', null,
                                                    ['id' => 'informacoes_seguranca', 'class' => 'summernote form-control']) }}
                                        <span class="help-block">Quais as informações de segurança deste produto ?</span>
                                    </div>
                                    <div class="form-group">
                                        <label for="usos" class="control-label">Usos</label>
                                        <div class="col-md-12">
                                            <select name="usos[]" id="usos" multiple="" class="form-control">
                                                @foreach ($usos as $id => $uso)
                                                <option selected value="{{ $id }}">{{ $uso }}</option>
                                                @endforeach
                                            </select>
                                            <span class="help-block">Onde este produto pode ser utilizado ?</span>
                                        </div>
                                    </div>
                                </div> <!-- /#aba-geral -->
                                
                                <div role="tabpanel" class="tab-pane" id="aba-imagens"> <!-- #aba-imagens -->
                                    <div class="form-group">
                                        <div class="col-md-4 grupo-upload-imagem">
                                            <label for="imagem" class="control-label">Imagem do produto</label>
                                            <input readonly="" class="form-control" placeholder="Procurar..." type="text">
                                            <input id="imagem" name="imagem" multiple="" type="file">
                                            <span class="help-block">
                                                Selecione uma imagem no formato <strong>PNG</strong> com a resolução de <strong>500x500</strong> pixels.
                                            </span>
                                            <br/><br/>
                                            {{-- <img class="miniatura-produto" alt="Imagem do produto" src="{{ $diretorio_imagens . '/' . $produto->imagem }}"/> --}}
                                        </div>

                                        <div class="col-md-4 grupo-upload-imagem">
                                            <label for="imagem_pack" class="control-label">Imagem para o pack</label>
                                            <input readonly="" class="form-control" placeholder="Procurar..." type="text">
                                            <input id="imagem_pack" name="imagem_pack" multiple="" type="file">
                                            <span class="help-block">
                                                Selecione uma imagem no formato <strong>PNG</strong> com a resolução de <strong>1000x500</strong> pixels.
                                            </span>
                                            <br/><br/>
                                            {{-- <img class="miniatura-produto-pack" alt="Imagem do pack" src="{{ $diretorio_imagens_packs . '/' . $produto->imagem_pack }}"/> --}}
                                        </div>

                                        <div class="col-md-4 grupo-upload-imagem">
                                            <label for="imagem_banner" class="control-label">Imagem para o banner</label>
                                            <input readonly="" class="form-control" placeholder="Procurar..." type="text">
                                            <input id="imagem_banner" name="imagem_banner" multiple="" type="file">
                                            <span class="help-block">
                                                Selecione uma imagem no formato <strong>PNG</strong> com a resolução de <strong>1635x492</strong> pixels.
                                            </span>
                                            <br/><br/>
                                            {{-- <img class="miniatura-produto-baner" alt="Imagem do banner" src="{{ $diretorio_imagens_banners . '/' . $produto->imagem_banner }}"/> --}}
                                        </div>
                                    </div>
                                </div> <!-- /#aba-imagens -->
                                
                                <div role="tabpanel" class="tab-pane" id="aba-versoes"> <!-- #aba-versoes -->
                                    {{-- Há um bug (testei no firefox) que faz com que o botão de add não funcione sem um texto aqui antes o.O --}}
                                    <h4>Editando versões do produto</h4>
                                    <div class="col-md-offset-11 col-md-1">
                                        <button title="Nova versão" class="versao_criar" type="button">
                                            <i class="material-icons icone-ok">add_circle</i>
                                        </button>
                                    </div>
                                    <div class="versoes_container">
                                    </div>
                                </div> <!-- /#aba-versoes -->
                                
                            </div> <!-- /.tab-content -->
                            <div class="form-group">
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-primary btn-raised">Salvar</button>
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- isto fica fora do formulário para não ser enviado --}}
    <input hidden name="versao_maxima" id="versao_maxima" value="{{ $versao_maxima }}" />
    <div id="versao_modelo" hidden>
        <div class="versao_container">
            <div class="form-group">
                <div class="col-md-12">
                    <h2>
                        <button title="Excluir esta versão" class="versao_deletar" type="button">
                            <i class="material-icons icone-deletar">close</i>
                        </button>
                        <span class="titulo-nova-versao">Nova versão</span>
                    </h2>
                    <label class="control-label">Nome</label>
                    <input type="text" class="form-control nome-nova-versao" name="versoes[][nome]">
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-12 grupo-upload-imagem">
                    <label class="control-label">Imagem</label>
                    <input readonly="" class="form-control" placeholder="Procurar..." type="text">
                    <input name="versoes[][imagem]" multiple="" type="file">
                    <span class="help-block">
                        Selecione uma imagem no formato <strong>PNG</strong> com a resolução de <strong>500x500</strong> pixels.
                    </span>
                </div>
            </div>
            <hr>
        </div>
    </div>
@endsection

@push('css')
    <link href="dist/summernote/summernote.css" rel="stylesheet" type="text/css">
    <link href="dist/selectize/css/selectize.css" rel="stylesheet" type="text/css">
@endpush

@push('scripts')
    <script src="dist/summernote/summernote.min.js"></script>
    <script src="dist/summernote/lang/summernote-pt-BR.min.js"></script>
    <script src="dist/selectize/js/standalone/selectize.min.js"></script>
    
    <script type="text/javascript">
        $(document).ready(function () {
            opcoes_summernote = {
                minHeight: 100,
                focus: false,
                toolbar: [
                    // [groupName, [list of button]]
                    ['style', ['bold', 'italic', 'underline', 'clear']],
                    ['font', ['strikethrough', 'superscript', 'subscript']],
                    ['fontsize', ['fontsize']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['height', ['height']],
                    ['insert', ['link', 'video']],
                    ['misc', ['fullscreen']]
                ],
                fontNames: [''],
                lang: 'pt-BR',
                callbacks: {
                    // workaround para habilitar o foco que é normalmente feito pelo material.js
                    onFocus: function() {
                        $(this).parent().addClass('is-focused');
                    },
                    onBlur: function() {
                        $(this).parent().removeClass('is-focused');
                    }
                }
            };
            var $elementoSummernote = $('.summernote');
            $elementoSummernote.summernote(opcoes_summernote);
            
            $('#usos').selectize({
                plugins: ['remove_button'],
                create: false,
                maxItems: null,
                
                valueField: 'id',
                labelField: 'descricao',
                searchField: ['descricao'],
                load: function(query, callback) {
                    if (!query.length) return callback();
                    $.ajax({
                        url:  '{{ route('api::app::produto::uso::categoria::item::buscar', ['campo' => 'descricao']) }}/' + encodeURIComponent(query),
                        type: 'GET',
                        error: function() {
                            callback();
                        },
                        success: function(response) {
                            callback(response.itens.slice(0, 10));
                        }
                    });
                }
            });
            
            $('.versao_deletar').on('click', function() {
                $(this).closest('.versao_container').remove();
            });
            
            $('.versao_criar').on('click', function() {
                var $versaoModelo  = $('#versao_modelo .versao_container');
                var $container     = $('.versoes_container');
                
                {{-- a versão máxima é apenas um número a partir do qual posso interar sem me preocupar com colisão de 
                        indices aqui no front. A cada nova versão o indice é incrementado em um. --}}
                $('#versao_maxima').val(parseInt($('#versao_maxima').val()) + 1);
                
                // $container.prepend($versaoModelo.show());
                var $novo_bloco = $versaoModelo.clone(true).show();
                
                {{-- Agora troco os indices do formulário para que cada nova versão fique com o mesmo valor
                        de indice e seja recebida corretamente pelo PHP --}}
                $novo_bloco.find('[name^="versoes"]').each(function() {
                    var $versao_maxima = $('#versao_maxima');
                    var nome  = $(this).attr('name');
                    nome_novo = nome.replace(/\[\]/g, '[' + $versao_maxima.val() + ']');
                    $(this).attr('name', nome_novo);
                });
                
                $container.prepend($novo_bloco);
            });
            
            $('.nome-nova-versao').keyup(function() {
                var valorAtual = $(this).val();
                if (valorAtual != '') {
                    $(this).closest('.col-md-12').find('.titulo-nova-versao').html(valorAtual);
                }
            });
            
        });
    </script>
@endpush
