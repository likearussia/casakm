@extends('layouts.app')

@section('conteudo')
    <div class="container-fluid container-principal">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="conteudo">
                        <div class="page-header">
                            <h1>
                                <i class="icone-padrao material-icons">store</i>
                                Produtos
                            </h1>
                            <ol class="breadcrumb">
                                <li><a href="{{ route('app::inicio') }}">Início</a></li>
                                <li class="active">Produtos</li>
                            </ol>
                        </div>

                        @include('flash::message')
                        
                        <table class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th class="text-center">Nome</th>
                                    <th class="text-center">Marca</th>
                                    <th colspan="2" class="text-center">Ações</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($produtos as $produto)
                                    <tr>
                                        <td class="text-center">
                                            <a href="{{ route('app::produto::editar', ['id' => $produto->id]) }}">
                                                {{ $produto->nome }}
                                            </a>
                                        </td>
                                        <td class="text-center">{{ $produto->marca->nome }}</td>
                                        <td class="text-right">
                                            <a href="{{ route('app::produto::editar', ['id' => $produto->id]) }}" title="Editar">
                                                <i class="icone-editar material-icons" title="Editar">edit</i>
                                            </a>
                                        </td>
                                        <td class="text-left">
                                            <a href="{{ route('app::produto::deletar', ['id' => $produto->id]) }}" title="Deletar" class="dialogo-deletar">
                                                <i class="icone-deletar material-icons" title="Deletar">delete</i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        
                        <div class="row">
                            <div class="col-md-12">
                                @if ($produtos->total() > 0)
                                    Exibindo {{ $produtos->count() }} de {{ $produtos->total() }} produtos encontrados.
                                @else
                                    Nenhum produto encontrado.
                                @endif
                            </div>
                        </div>
                        
                        <div class="paginacao text-center">
                            {!! $produtos->links() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
