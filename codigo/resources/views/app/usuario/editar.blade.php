@extends('layouts.app')

@section('conteudo')
    <div class="container-fluid container-principal">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="conteudo">
                        <div class="page-header">
                            <h1>
                                <i class="icone-padrao material-icons">people</i>
                                Usuário
                            </h1>
                            <ol class="breadcrumb">
                                <li><a href="{{ route('app::inicio') }}">Dashboard</a></li>
                                <li><a href="{{ route('app::usuario::index') }}">Usuários</a></li>
                                <li class="active">Editar</li>
                            </ol>
                        </div>

                        @include('shared.erro-validacao')
                        @include('flash::message')

                        {{ Form::model($usuario, ['class' => 'form-horizontal']) }}
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label for="nome" class="control-label">Nome</label>
                                    {{ Form::text('nome', null, ['placeholder' => 'Nome', 'id' => 'nome',
                                                    'required', 'class' => 'form-control']) }}
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label for="email" class="control-label">E-mail</label>
                                    {{ Form::email('email', null, ['placeholder' => 'E-mail', 'id' => 'email',
                                                    'required', 'class' => 'form-control']) }}
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6">
                                    <label for="senha" class="control-label">Senha</label>
                                    {{ Form::password('senha', ['placeholder' => 'Senha', 'id' => 'senha',
                                                        'class' => 'form-control']) }}
                                </div>
                                <div class="col-md-6">
                                    <label for="senha_confirmation" class="control-label">Confirmação de senha</label>
                                    {{ Form::password('senha_confirmation', ['placeholder' => 'Senha', 'id' => 'senha_confirmation',
                                                        'class' => 'form-control']) }}
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <button type="submit" class="btn btn-primary btn-raised">Salvar</button>
                                </div>
                            </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection