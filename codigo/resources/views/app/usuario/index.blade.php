@extends('layouts.app')

@section('conteudo')
    <div class="container-fluid container-principal">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="conteudo">
                        <div class="page-header">
                            <h1>
                                <i class="icone-padrao material-icons">people</i>
                                Usuários
                            </h1>
                            <ol class="breadcrumb">
                                <li><a href="{{ route('app::inicio') }}">Início</a></li>
                                <li class="active">Usuários</li>
                            </ol>
                        </div>

                        @include('shared.erro-validacao')
                        @include('flash::message')
                        
                        <table class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th class="text-center">Criado em</th>
                                    <th class="text-center">Nome</th>
                                    <th class="text-center">E-mail</th>
                                    <th colspan="2" class="text-center">Ações</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($usuarios as $usuario)
                                    <tr>
                                        <td class="text-center">{{ $usuario->created_at }}</td>
                                        <td class="text-center">
                                            <a href="{{ route('app::usuario::editar', ['id' => $usuario->id]) }}">
                                                {{ $usuario->nome }}
                                            </a>
                                        </td>
                                        <td class="text-center">{{ $usuario->email }}</td>
                                        <td class="text-right">
                                            <a href="{{ route('app::usuario::editar', ['id' => $usuario->id]) }}" title="Editar">
                                                <i class="icone-editar material-icons" title="Editar">edit</i>
                                            </a>
                                        </td>
                                        <td class="text-left">
                                            <a href="{{ route('app::usuario::deletar', ['id' => $usuario->id]) }}" title="Deletar" class="dialogo-deletar">
                                                <i class="icone-deletar material-icons" title="Deletar">delete</i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        
                        <div class="row">
                            <div class="col-md-12">
                                @if ($usuarios->total() > 0)
                                    Exibindo {{ $usuarios->count() }} de {{ $usuarios->total() }} usuários encontrados.
                                @else
                                    Nenhum usuário encontrado.
                                @endif
                            </div>
                        </div>
                        
                        <div class="paginacao text-center">
                            {!! $usuarios->links() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
