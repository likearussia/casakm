@extends('layouts.login')

@section('conteudo')
<div class="container">
    <div class="col-md-6 col-md-offset-3" id="app-login">
        <div class="well bs-component">
            @include('flash::message')
            <form class="form-horizontal" role="form" method="POST" action="{{ route('app::post_login') }}">
                {!! csrf_field() !!}
                <fieldset>
                    <legend>Entrar</legend>
                    <div class="form-group{{ $errors->has('inputUser') ? ' has-error' : '' }}">
                        <label for="inputUser" class="col-md-2 control-label">Usuário</label>

                        <div class="col-md-10">
                            <input type="text" class="form-control" id="inputUser" name="inputUser" placeholder="Usuário" value="{{ old('inputUser') }}">
                            
                            @if ($errors->has('inputUser'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('inputUser') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('inputPassword') ? ' has-error' : '' }}">
                        <label for="inputPassword" class="col-md-2 control-label">Senha</label>

                        <div class="col-md-10">
                            <input type="password" class="form-control" id="inputPassword" name="inputPassword" placeholder="Senha">
                            
                            @if ($errors->has('inputPassword'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('inputPassword') }}</strong>
                                </span>
                            @endif
                            
                            <br/>
                            <div class="togglebutton">
                              <label>
                                <input type="checkbox" name="remember"> Lembrar-me
                              </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-10 col-md-offset-2">
                            <button type="submit" class="btn btn-primary btn-raised">Entrar</button>
                            
                            <!--<a class="btn btn-link" href="{{ route('app::reset_password') }}">Esqueceu sua senha?</a>-->
                        </div>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
</div>
@endsection
