@extends('layouts.site')

@push('css')
    <link href="{{ asset('css/interna.css') }}" rel="stylesheet">
@endpush

@section('conteudo')
    <div id="content-banner" class="blog-p">
        <img src="{{ asset('images/banner-casa-em-dia.jpg') }}"/>
        <div id="nav-breadcrumb">
            <div class="centralizar">
                <div class="step-indicator">
                    <a class="step completed" href="{{url ('/')}}">Home</a>
                    <a class="step completed" href="{{url ('casa-em-dia')}}">Casa em dia</a>
                    <a class="step completed" href="#">Busca</a>
                    <a class="step step-rosa" href="#">{{Request::input('termo')}}</a>
                </div>
            </div>
        </div>
        <div id="search-busca">
            <div id="search-centralizar">
                <h2>Casa em dia</h2>
                <form method="get" action="{{ route('site_buscar') }}">
                    <input class="input-search" placeholder="Encontre a dica desejada" type="text" name="termo" id='buscar'><br/>
                </form>
                <p>Ex: Mancha de molho, Limpar a sala, Tirar mancha de roupa branca</p>
            </div>
        </div>
    </div>
    <div class="centralizar">
    
            <div id="content-busca">
            <div id="content-busca-left" class="rosa-color-02">
                <p>Resultado da busca</p>
            </div>
                <div id="content-busca-right" class="rosa-color">
                    <p>{{ $total_resultados }} RESULTADOS PARA: <span class="rosa-color-font">{{Request::input('termo')}}</span></p>
                </div>
                
            
            </div>
            <div id="content-left">

            </div>
            <div id="content-right" class="busca-box">
                <?php 
                    if ($total_resultados == 0) :
                        print '<p>Busca sem resultados</p>';
                    else :
                        $i = 1;
                      
                        
                        foreach ($posts as $post):
                            if ($i % 2 == 0) {
                                $classe = '02';
                            } else {
                                $classe = '01';
                            } ?>
                            <div class="content-produto-<?= $classe ?> box-produto">
                                <a href="{{ route('blog::post::visualizar', ['slug' => $post->slug]) }}">
                                <h1 class="color-h2">{{ str_limit(strip_tags($post->titulo), 40) }}</h1>
                                <div class="produto-imagem">
                                    <img src="{{ asset("$diretorio_imagens_post/{$post->imagem_destaque}") }}" alt="{{ $post->titulo }}"/>
                                </div>
                               </a>
                            </div>
                            <?php $i++;
                        endforeach;
                    endif; ?>
                
                {{-- Paginacao --}}
                {!! $posts->links() !!}
            </div>
        </div>

@endsection

@push('scripts')
    <script src="{{ asset('dist/js/interna.js') }}" type="text/javascript"></script>
@endpush
