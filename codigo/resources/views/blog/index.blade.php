@extends('layouts.site')

@push('css')
    <link href="{{ asset('css/interna.css') }}" rel="stylesheet">
@endpush

@section('conteudo')
    <div id="content-banner" class="blog-p">
        <img src="{{ asset('images/banner-casa-em-dia.jpg') }}"/>
        <div id="nav-breadcrumb">
            <div class="centralizar">
                <div class="step-indicator">
                    <a class="step completed" href="#">Home</a>
                    <a class="step step-rosa" href="#">Casa em dia</a>
                </div>
            </div>
        </div>
        <div id="search-busca">
            <div id="search-centralizar">
                <h2>Casa em dia</h2>
                <form method="get" action="{{ route('blog::buscar') }}">
                    <input class="input-search" placeholder="Encontre a dica desejada" type="text" name="termo" id='buscar'><br/>
                </form>
                <p>Ex: Mancha de molho, Limpar a sala, Tirar mancha de roupa branca</p>
            </div>
        </div>
    </div>
    <div class="centralizar">
        <div id="centralizar-box">
            Sabe aquele segredinho passado de mãe para a filha?
            Nós também temos algumas dicas para te ajudar com a sua casa, promovendo o bem-estar e o conforto que você merece.
            Confira as sugestões que preparamos da nossa casa para sua casa.
        </div>
        <div id="content-left">
            <div id="menu-blog">
                <h2>Categorias</h2>
                <ul>
                    @if (! isset($menu_categoria_ativo))
                        <li class="blog-menu-active">
                    @else
                        <li>
                    @endif
                    <a href="{{ route('blog::index') }}">Todas</a>
                    </li>

                    @foreach ($categorias as $categoria)
                        @if (isset($menu_categoria_ativo) && $menu_categoria_ativo == $categoria->slug)
                            <li class="blog-menu-active">
                        @else
                            <li>
                        @endif
                            <a href="{{ route('blog::post::categoria::index', ['slug' => $categoria->slug]) }}">{{ $categoria->nome }}</a>
                        </li>
                    @endforeach
                </ul>

                <br/>

               <!--- <h2>Tags</h2>
                <ul>
                    @if (! isset($menu_tag_ativo))
                        <li class="blog-menu-active">
                    @else
                        <li>
                    @endif
                    <a href="{{ route('blog::index') }}">Todas</a>
                    </li>

                    @foreach ($tags as $tag)
                        @if (isset($menu_tag_ativo) && $menu_tag_ativo == $tag->slug)
                            <li class="blog-menu-active">
                        @else
                            <li>
                        @endif
                            <a href="{{ route('blog::post::tag::index', ['slug' => $tag->slug]) }}">{{ $tag->tag }}</a>
                        </li>
                    @endforeach-->
                </ul>
            </div>
        </div>
        <div id="content-right">
            @for ($i = 0; $i < count($posts); $i++)
                <?php
                $post = $posts[$i];
                // As classes css terminam com 01, 02 e 03 e o $i%2 irá gerar números de 0 a 2, por isso incremento
                $mod = $i%2;
                $numero = str_pad(++$mod, 2, '0', STR_PAD_LEFT); ?>
                <div class="content-blog-{{ $numero }}">
                    <a href="{{ route('blog::post::visualizar', ['slug' => $post->slug]) }}">
                        <div class="blog-imagem">
                            <img src="{{ $diretorio_imagens . '/' . $post->imagem_destaque }}" alt="{{ $post->titulo }}"/>
                        </div>
                        <h1>{{ $post->titulo }}</h1>
                        <p>{{ str_limit(strip_tags($post->texto), 150) }}</p>
                    </a>
                </div>
            @endfor
            <ul class="pagination">
                {!! $posts->links() !!}
            </ul>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="{{ asset('dist/js/interna.js') }}" type="text/javascript"></script>
@endpush
