@extends('layouts.site')

@push('css')
    <link href="{{ asset('css/interna.css') }}" rel="stylesheet">
 
@endpush

@section('conteudo')
    <div id="content-banner">
        <div id="nav-breadcrumb">
            <div class="centralizar">
                <div class="step-indicator">
                    <a class="step completed" href="{{url('/')}}">Home</a>
                    <a class="step completed" href="{{ route('blog::index') }}">Casa em Dia</a>
                    <a class="step step-rosa" href="#">{{ $post->titulo }}</a>
                </div>
            </div>
        </div>
    </div>
    <div id="titulo-blog">
        <div id="titulo-centralizar">
            <h2>dicas</h2>
            <h1>{{ $post->titulo }}</h1>
            <?php $data = date_parse_from_format('d/m/Y H:i:s', $post->created_at); ?>
            <h1>{{ $data['day'] . '/' . $data['month'] . '/' . $data['year'] }}</h1>
        </div>
    </div>
    <article id="produtos-descricao">
        <div id="bg-content-left" class="bg-blog-left">
            <div id="content-left-centralizar">
                <p>
                <div class="banner-news">
                    <img src="{{ $diretorio_imagens . '/' . $post->imagem_destaque }}" alt="{{ $post->titulo }}"/>
                </div>
                <div id="casa-em-dia-post">
                    {!! $post->texto !!}
                </div>
                @if ($post->permitir_comentarios)
                    <div class="blog-coments">
                        <h3>COMENTÁRIOS</h3>
                        <div id="fb-root"></div>
                        <script>(function (d, s, id) {
                                var js, fjs = d.getElementsByTagName(s)[0];
                                if (d.getElementById(id))
                                    return;
                                js = d.createElement(s);
                                js.id = id;
                                js.src = "//connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v2.6&appId=202133116578155";
                                fjs.parentNode.insertBefore(js, fjs);
                            }(document, 'script', 'facebook-jssdk'));</script>
                        <div class="fb-comments" data-href="http://casakm.infra.ph2.com.br/como-lavar-as-roupinhas-do-seu-beb%C3%AA" data-width="100%" data-numposts="5"></div>
                    </div>
                @endif
                <!-- <div class="content">

                     <div id="galleria">
                         <a href="http://upload.wikimedia.org/wikipedia/commons/thumb/a/a2/Biandintz_eta_zaldiak_-_modified2.jpg/800px-Biandintz_eta_zaldiak_-_modified2.jpg">
                             <img
                             src="http://upload.wikimedia.org/wikipedia/commons/thumb/a/a2/Biandintz_eta_zaldiak_-_modified2.jpg/100px-Biandintz_eta_zaldiak_-_modified2.jpg",
                             data-big="http://upload.wikimedia.org/wikipedia/commons/thumb/a/a2/Biandintz_eta_zaldiak_-_modified2.jpg/1280px-Biandintz_eta_zaldiak_-_modified2.jpg"
                             data-title="Biandintz eta zaldiak"
                             data-description="Horses on Bianditz mountain, in Navarre, Spain."
                             >
                         </a>
                         <a href="http://upload.wikimedia.org/wikipedia/commons/thumb/6/6c/Athabasca_Rail_at_Brule_Lake.jpg/800px-Athabasca_Rail_at_Brule_Lake.jpg">
                             <img
                             src="http://upload.wikimedia.org/wikipedia/commons/thumb/6/6c/Athabasca_Rail_at_Brule_Lake.jpg/100px-Athabasca_Rail_at_Brule_Lake.jpg",
                             data-big="http://upload.wikimedia.org/wikipedia/commons/thumb/6/6c/Athabasca_Rail_at_Brule_Lake.jpg/1280px-Athabasca_Rail_at_Brule_Lake.jpg"
                             data-title="Athabasca Rail"
                             data-description="The Athabasca River railroad track at the mouth of Brulé Lake in Alberta, Canada."
                             >
                         </a>
                         <a href="http://upload.wikimedia.org/wikipedia/commons/thumb/1/1f/Back-scattering_crepuscular_rays_panorama_1.jpg/1280px-Back-scattering_crepuscular_rays_panorama_1.jpg">
                             <img
                             src="http://upload.wikimedia.org/wikipedia/commons/thumb/1/1f/Back-scattering_crepuscular_rays_panorama_1.jpg/100px-Back-scattering_crepuscular_rays_panorama_1.jpg",
                             data-big="http://upload.wikimedia.org/wikipedia/commons/thumb/1/1f/Back-scattering_crepuscular_rays_panorama_1.jpg/1400px-Back-scattering_crepuscular_rays_panorama_1.jpg"
                             data-title="Back-scattering crepuscular rays"
                             data-description="Picture of the day on Wikimedia Commons 26 September 2010."
                             >
                         </a>
                         <a href="http://upload.wikimedia.org/wikipedia/commons/thumb/7/75/Interior_convento_3.jpg/800px-Interior_convento_3.jpg">
                             <img
                             src="http://upload.wikimedia.org/wikipedia/commons/thumb/7/75/Interior_convento_3.jpg/120px-Interior_convento_3.jpg",
                             data-big="http://upload.wikimedia.org/wikipedia/commons/thumb/7/75/Interior_convento_3.jpg/1400px-Interior_convento_3.jpg"
                             data-title="Interior convento"
                             data-description="Interior view of Yuriria Convent, founded in 1550."
                             >
                         </a>
                         <a href="http://upload.wikimedia.org/wikipedia/commons/thumb/6/6b/Oxbow_Bend_outlook_in_the_Grand_Teton_National_Park.jpg/800px-Oxbow_Bend_outlook_in_the_Grand_Teton_National_Park.jpg">
                             <img
                             src="http://upload.wikimedia.org/wikipedia/commons/thumb/6/6b/Oxbow_Bend_outlook_in_the_Grand_Teton_National_Park.jpg/100px-Oxbow_Bend_outlook_in_the_Grand_Teton_National_Park.jpg",
                             data-big="http://upload.wikimedia.org/wikipedia/commons/thumb/6/6b/Oxbow_Bend_outlook_in_the_Grand_Teton_National_Park.jpg/1280px-Oxbow_Bend_outlook_in_the_Grand_Teton_National_Park.jpg"
                             data-title="Oxbow Bend outlook"
                             data-description="View over the Snake River to the Mount Moran with the Skillet Glacier."
                             >
                         </a>
                         <a href="http://upload.wikimedia.org/wikipedia/commons/thumb/c/c9/Hazy_blue_hour_in_Grand_Canyon.JPG/800px-Hazy_blue_hour_in_Grand_Canyon.JPG">
                             <img
                             src="http://upload.wikimedia.org/wikipedia/commons/thumb/c/c9/Hazy_blue_hour_in_Grand_Canyon.JPG/100px-Hazy_blue_hour_in_Grand_Canyon.JPG",
                             data-big="http://upload.wikimedia.org/wikipedia/commons/thumb/c/c9/Hazy_blue_hour_in_Grand_Canyon.JPG/1280px-Hazy_blue_hour_in_Grand_Canyon.JPG"
                             data-title="Hazy blue hour"
                             data-description="Hazy blue hour in Grand Canyon. View from the South Rim."
                             >
                         </a>
                         <a href="http://upload.wikimedia.org/wikipedia/commons/thumb/f/f4/2909_vallon_moy_res.jpg/800px-2909_vallon_moy_res.jpg">
                             <img
                             src="http://upload.wikimedia.org/wikipedia/commons/thumb/f/f4/2909_vallon_moy_res.jpg/100px-2909_vallon_moy_res.jpg",
                             data-big="http://upload.wikimedia.org/wikipedia/commons/thumb/f/f4/2909_vallon_moy_res.jpg/1280px-2909_vallon_moy_res.jpg"
                             data-title="Haute Severaisse valley"
                             data-description="View of Haute Severaisse valley and surrounding summits from the slopes of Les Vernets."
                             >
                         </a>
                         <a href="http://upload.wikimedia.org/wikipedia/commons/thumb/4/46/Bohinjsko_jezero_2.jpg/800px-Bohinjsko_jezero_2.jpg">
                             <img
                             src="http://upload.wikimedia.org/wikipedia/commons/thumb/4/46/Bohinjsko_jezero_2.jpg/100px-Bohinjsko_jezero_2.jpg",
                             data-big="http://upload.wikimedia.org/wikipedia/commons/thumb/4/46/Bohinjsko_jezero_2.jpg/1280px-Bohinjsko_jezero_2.jpg"
                             data-title="Bohinj lake"
                             data-description="Bohinj lake (Triglav National Park, Slovenia) in the morning."
                             >
                         </a>
                         <a href="http://upload.wikimedia.org/wikipedia/commons/thumb/3/32/Bowling_Balls_Beach_2_edit.jpg/800px-Bowling_Balls_Beach_2_edit.jpg">
                             <img
                             src="http://upload.wikimedia.org/wikipedia/commons/thumb/3/32/Bowling_Balls_Beach_2_edit.jpg/100px-Bowling_Balls_Beach_2_edit.jpg",
                             data-big="http://upload.wikimedia.org/wikipedia/commons/thumb/3/32/Bowling_Balls_Beach_2_edit.jpg/1280px-Bowling_Balls_Beach_2_edit.jpg"
                             data-title="Bowling Balls"
                             data-description="Mendocino county, California, USA."
                             >
                         </a>
                     </div>
                 </div>-->
                <script>
                    // Load the classic theme
                    Galleria.loadTheme('dist/js/galleria.classic.min.js');
                    // Initialize Galleria
                    Galleria.run('#galleria');
                </script>
            </div>
        </div>
        <div id="bg-content-right" class="bg-blog-right">
            <section class="right-box">
                <h2>Compartilhar</h2>
                <?php /* <ol>
                    <li><a onclick="abreFace()" class="social-button facebook"><img src="{{ asset('images/icon-social-facebook.png') }}" alt="facebook"/></a></li>
                    <li><a onclick="abregplus()" class="social-button google"><img src="{{ asset('images/icon-social-instagram.png') }}" alt="instagram"/></a></li>
                    <li><a onclick="abreTweet()" data-related="twitterdev" data-size="large" data-text="Nome noticia" data-count="none"  class="social-button twitter"><img src="{{ asset('images/icon-social-twitter.png') }}" alt="facebook"/></a></li>
                </ol> */?>

                                <!-- Buttons start here. Copy this ul to your document. -->
                <ol class="rrssb-buttons clearfix">
          
                  <li class="rrssb-facebook">
                    <!--  Replace with your URL. For best results, make sure you page has the proper FB Open Graph tags in header: https://developers.facebook.com/docs/opengraph/howtos/maximizing-distribution-media-content/ -->
                    <a href="https://www.facebook.com/sharer/sharer.php?u=http://casakm.sili.com.br/" class="popup">
                     <img src="{{ asset('images/icon-social-facebook.png') }}" alt="facebook"/>
                    </a>
                  </li>
                  <li class="rrssb-twitter">
                    <!-- Replace href with your Meta and URL information  -->
                    <a href="https://twitter.com/intent/tweet?text=Veja só essa dica incrível"
                    class="popup">
                      <img src="{{ asset('images/icon-social-twitter.png') }}" alt="facebook"/>
                    </a>
                  </li>
                  
                </ol>
                <!-- Buttons end here -->
            </section>
            <section class="right-box tag-section">
                @if (! empty($tags))
                    <h2>Tags</h2>
                    <ul class="tags-l">
                        @foreach ($tags as $tag)
                            <li>
                                <a href="{{ route('blog::post::tag::index', ['slug' => $tag->slug]) }}">{{ $tag->tag }}</a>
                            </li>
                        @endforeach
                    </ul>
               @endif
            </section>
        </div>
    </article>
@endsection

@push('scripts')
    <script src="{{ asset('dist/js/interna.js') }}" type="text/javascript"></script>
@endpush
