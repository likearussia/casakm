@extends('layouts.site')
@push('css')
<link href="{{ asset('css/interna.css') }}" rel="stylesheet">
@endpush

@section('borboleta')
<a href="{{url('/')}}"><img src="{{ asset('images/logo.png') }}" alt="logo"/></a>
@endsection

@section('conteudo')

<div class="container-error" >
    <script>
        //window.setTimeout("history.back(-2)", 5000);
    </script> 
    <h1>403</h1>
    <h2>Acesso negado</h2>
</div>
@endsection

@push('scripts-cabecalho')
<script>
    try {
        Typekit.load({async: true});
    } catch (e) {
    }
</script>
@endpush
@push('scripts')
<script>
    // modal busca
    $('.control').click(function () {
        $('body').addClass('mode-search');
        $('.input-search').focus();
    });
    $('.icon-close').click(function () {
        $('body').removeClass('mode-search');
    });

    // modal composição
    $('.composicao').click(function () {
        $('body').addClass('mode-composicao');
    });
    $('.icon-close').click(function () {
        $('body').removeClass('mode-composicao');
    });

    // carousel
    $('.slider-for').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        fade: true,
        dots: true,
        asNavFor: '.slider-nav'
    });
    $('.slider-nav').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        asNavFor: '.slider-for',
        arrows: false,
        centerMode: true,
        focusOnSelect: true,
        autoplay: true,
    });
    // carousel
    $(document).ready(function () {
        $("#owl-demo").owlCarousel({
            // Most important owl features
            items: 4,
            itemsDesktop: [1199, 3],
            itemsDesktopSmall: [979, 3],
            itemsTablet: [768, 2],
            itemsMobile: [479, 1],
            //Autoplay
            autoPlay: false, //Set AutoPlay to 3 seconds
            stopOnHover: true,
            //Basic Speeds
            slideSpeed: 200,
            paginationSpeed: 800,
            rewindSpeed: 1000,
            // Navigation
            navigation: false,
            navigationText: ["prev", "next"],
            rewindNav: true,
            scrollPerPage: false,
            //Pagination
            pagination: false,
            paginationNumbers: false,
            // CSS Styles
            baseClass: "owl-carousel",
            theme: "owl-theme",
            //Auto height
            autoHeight: false,
            //Mouse Events
            dragBeforeAnimFinish: true,
            mouseDrag: true,
            touchDrag: true,
            //Transitions
            transitionStyle: false,
        });
    });
    // menu
    $(function () {
        var Accordion = function (el, multiple) {
            this.el = el || {};
            this.multiple = multiple || false;
            // Variables privadas
            var links = this.el.find('.link');
            // Evento
            links.on('click', {el: this.el, multiple: this.multiple}, this.dropdown)
        }
        Accordion.prototype.dropdown = function (e) {
            var $el = e.data.el;
            $this = $(this),
                    $next = $this.next();
            $next.slideToggle();
            $this.parent().toggleClass('open');
            if (!e.data.multiple) {
                $el.find('.submenu').not($next).slideUp().parent().removeClass('open');
            }
            ;
        }
        var accordion = new Accordion($('#accordion'), false);
    });

</script>
@endpush
