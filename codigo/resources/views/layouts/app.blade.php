<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    @stack('meta')
    <base href="{{ url('/') }}/" />
    
    
    @hasSection("titulo")
        <title>@yield('titulo') - CasaKM</title>
    @else
        <title>CasaKM - administração</title>
    @endif
    

    <!-- Fontes -->
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700">
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/icon?family=Material+Icons">

    <!-- Estilos -->
    <link href="dist/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="dist/bootstrap-material-design/css/bootstrap-material-design.min.css" rel="stylesheet" type="text/css">
    <link href="dist/bootstrap-material-design/css/ripples.min.css" rel="stylesheet" type="text/css">
    <link href="dist/sweetalert2/sweetalert2.min.css" rel="stylesheet" type="text/css">
    <link href="css/app/app.css" rel="stylesheet" type="text/css">
    @stack('css')
    
    <!-- Scripts -->
    @stack('scripts-cabecalho')
</head>
<body id="app-layout">
    <nav class="navbar navbar-static-top">
        <div class="container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Alterar navegação</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="{{ route('app::inicio') }}">
                    <img src="{{ asset('images/app/logo_casakm.png') }}" alt="CasaKM" title="CasaKM"/>
                </a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Lado esquerdo da Navbar -->
                <ul class="nav navbar-nav">
                    <?php // para evitar erro fatal caso a variável não tenha sido definida no controller
                    (! isset($menu_ativo)) ? $menu_ativo = '' : ''; ?>
                    
                    @if ($menu_ativo == 'app::banner')
                    <li class="active dropdown">
                    @else
                    <li class="dropdown">
                    @endif
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <i class="icone-menu material-icons">image</i>
                            Banners <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="{{ route('app::banner::criar')}}">
                                    <i class="icone-menu material-icons">add_circle</i>
                                    Novo
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('app::banner::index') }}">
                                    <i class="icone-menu material-icons">list</i>
                                    Listar
                                </a>
                            </li>
                        </ul>
                    </li>
                    
                    @if ($menu_ativo == 'app::institucional')
                    <li class="active dropdown">
                    @else
                    <li class="dropdown">
                    @endif
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <i class="icone-menu material-icons">work</i>
                            Institucional <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="{{ route('app::institucional::index') }}">
                                    <i class="icone-menu material-icons">list</i>
                                    Listar
                                </a>
                            </li>
                        </ul>
                    </li>
                    
                    @if ($menu_ativo == 'app::blog')
                    <li class="active dropdown">
                    @else
                    <li class="dropdown">
                    @endif
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <i class="icone-menu material-icons">forum</i>
                            Casa em dia <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="dropdown-header">Publicações</li>
                            <li>
                                <a href="{{ route('app::blog::post::criar') }}">
                                    <i class="icone-menu material-icons">add_circle</i>
                                    Nova publicação
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('app::blog::post::index') }}">
                                    <i class="icone-menu material-icons">list</i>
                                    Listar
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li class="dropdown-header">Categorias</li>
                            <li>
                                <a href="{{ route('app::blog::post::categoria::criar') }}">
                                    <i class="icone-menu material-icons">add_circle</i>
                                    Criar
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('app::blog::post::categoria::index') }}">
                                    <i class="icone-menu material-icons">view_list</i>
                                    Listar
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li class="dropdown-header">Tags</li>
                            <li>
                                <a href="{{ route('app::blog::post::tag::criar') }}">
                                    <i class="icone-menu material-icons">add_circle</i>
                                    Criar
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('app::blog::post::tag::index') }}">
                                    <i class="icone-menu material-icons">label</i>
                                    Listar
                                </a>
                            </li>
                        </ul>
                    </li>
                    
                    @if ($menu_ativo == 'app::produto')
                    <li class="active dropdown">
                    @else
                    <li class="dropdown">
                    @endif
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <i class="icone-menu material-icons">store</i>
                            Produtos <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="{{ route('app::produto::criar')}}">
                                    <i class="icone-menu material-icons">add_circle</i>
                                    Novo
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('app::produto::index') }}">
                                    <i class="icone-menu material-icons">list</i>
                                    Listar
                                </a>
                            </li>
                        </ul>
                    </li>
                    
                    @if ($menu_ativo == 'app::downloads')
                    <li class="active dropdown">
                    @else
                    <li class="dropdown">
                    @endif
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <i class="icone-menu material-icons">cloud_download</i>
                            Downloads <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="{{ route('app::download::criar') }}">
                                    <i class="icone-menu material-icons">add_circle</i>
                                    Novo
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('app::download::index') }}">
                                    <i class="icone-menu material-icons">list</i>
                                    Listar
                                </a>
                            </li>
                        </ul>
                    </li>
                    
                    @if ($menu_ativo == 'app::usuario')
                    <li class="active dropdown">
                    @else
                    <li class="dropdown">
                    @endif
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <i class="icone-menu material-icons">people</i>
                            Usuários <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <a href="{{ route('app::usuario::criar') }}">
                                    <i class="icone-menu material-icons">add_circle</i>
                                    Novo
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('app::usuario::index') }}">
                                    <i class="icone-menu material-icons">list</i>
                                    Listar
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>

                <!-- Lado direito da Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            <i class="icone-menu material-icons">account_circle</i>
                            {{ Auth::user()->nome }} <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <a href="{{ route('app::usuario::editar', [Auth::user()->id]) }}">
                                    <i class="icone-menu material-icons">account_circle</i>Meu perfil
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('app::logout') }}">
                                    <i class="icone-menu material-icons">directions_walk</i>Sair
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    @yield('conteudo')
    
    <footer class="rodape text-center">
        © 2016 CasaKM - Todos os direitos reservados
    </footer>

    <!-- JavaScripts -->
    <script src="dist/jquery/jquery.min.js"></script>
    <script src="dist/bootstrap/js/bootstrap.min.js"></script>
    <script src="dist/bootstrap-material-design/js/material.min.js"></script>
    <script src="dist/bootstrap-material-design/js/ripples.min.js"></script>
    <script src="dist/sweetalert2/sweetalert2.min.js"></script>
    <script src="js/app/app.js"></script>
    @stack('scripts')
</body>
</html>
