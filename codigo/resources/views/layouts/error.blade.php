<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    @stack('meta')

    <title>Academia do pintor @yield('titulo')</title>
    <base href="{{ url('/') }}" />

    <!-- Fontes -->
    <link href="{{ asset('dist/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">

    <!-- Estilos -->
    <link href="{{ asset('dist/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css">
    @stack('css')
    
    <!-- Scripts -->
    @stack('cabecalho-js')
</head>
<body id="app-layout">'

    @yield('conteudo')

    <!-- JavaScripts -->
    <script src="{{ asset('dist/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('dist/bootstrap/js/bootstrap.min.js') }}"></script>
    @stack('js')
</body>
</html>
