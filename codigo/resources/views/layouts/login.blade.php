<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    @stack('meta')

    <title>CasaKM @yield('title')</title>
    <base href="{{ url('/') }}/" />

    <!-- Fontes -->
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700">
    <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/icon?family=Material+Icons">

    <!-- Estilos -->
    <link href="dist/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="dist/bootstrap-material-design/css/bootstrap-material-design.min.css" rel="stylesheet" type="text/css">
    <link href="dist/bootstrap-material-design/css/ripples.min.css" rel="stylesheet" type="text/css">
    <link href="css/app/login.css" rel="stylesheet" type="text/css">
    @stack('css')
    
    <!-- Scripts -->
    @stack('scripts-cabecalho')
</head>
<body id="app-layout">

    @yield('conteudo')

    <!-- JavaScripts -->
    <script src="dist/jquery/jquery.min.js"></script>
    <script src="dist/bootstrap/js/bootstrap.min.js"></script>
    <script src="dist/bootstrap-material-design/js/material.min.js"></script>
    <script src="dist/bootstrap-material-design/js/ripples.min.js"></script>
    <script src="js/app/app.js"></script>
    @stack('scripts')
</body>
</html>
