<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta http-equiv="cache-control" content="max-age=86400" />
        <meta http-equiv="Pragma" content="max-age=86400">
        <meta name="description" content="Bem-vindo à Casa KM, uma empresa com produtos que já conquistaram o reconhecimento do mercado com performance, limpeza, proteção e cuidado para limpeza geral." />
        <meta name="author" content="CasaKM">

        <!-- Chrome address bar color, Firefox OS and Opera -->
        <meta name="theme-color" content="#008fd9">
        <!-- Windows Phone address bar color -->
        <meta name="msapplication-navbutton-color" content="#008fd9">
        <!-- iOS Safari address bar color -->
        <meta name="apple-mobile-web-app-status-bar-style" content="#008fd9">

        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        @stack('meta')
        <title>Casa KM - Da nossa casa, para sua casa.</title>
        <base href="{{url('/')}}/">
        <!-- Bootstrap -->
        <link href="{{ asset('css/geral.css') }}" rel="stylesheet">
        <link rel="icon" type="image/png" href="{{ asset('images/iconfinder.png') }}">
        @stack('css')
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <script src="{{ asset('dist/js/jquery.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('dist/js/jquery.timelinr-0.9.6.js') }}"></script>
        <script src="{{ asset('dist/js/slick.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('dist/js/owl.carousel.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('dist/js/jquery-ui.js') }}" type="text/javascript"></script>
        <script src="{{ asset('dist/js/rrssb.min.js') }}" type="text/javascript"></script>

        <script>
              (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
              (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
              m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
              })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

              ga('create', 'UA-76368792-1', 'auto');
              ga('send', 'pageview');
        </script>
        @stack('scripts-cabecalho')
    </head>
    
    <body>
        
        <!-- header.php -->
        <header>
            <div id="header-color">
                <div id="color-01"></div>
                <div id="color-02"></div>
                <div id="color-03"></div>
            </div>
            <div class="centralizar">
              

                @hasSection("borboleta")
                @yield('borboleta')
                @else
                 <a href="{{url('/')}}"><img src="{{ asset('images/logo.gif') }}" alt="logo" class="logo"/></a>
                @endif


               
                <div id="nav-right">
                    <div id="nav-social">
                        <ol>
                            <li><a href="https://www.youtube.com/channel/UCLhEbNK5P17u_beGXLjZzrQ?nohtml5=False" target="_blank"><img src="{{ asset('images/icon-social-youtube.png') }}" alt="facebook"/></a></li>
                            <li><a href="https://www.facebook.com/casakmoficial" target="_blank"><img src="{{ asset('images/icon-social-facebook.png') }}" alt="facebook"/></a></li>
                        </ol>
                    </div>
                    <nav>
                    <a href="#" id="pull"><i class="fa fa-bars" aria-hidden="true"></i></a>
                        <ol>
                            <?php // para evitar erro fatal caso a variável não tenha sido definida no controller
                            (! isset($menu_ativo)) ? $menu_ativo = '' : ''; ?>
                            @if ($menu_ativo == 'site_institucional')
                            <li class="ativo">
                            @else
                            <li>
                            @endif
                                <a href="{{url('sobre/a-casakm')}}">Casa KM</a>
                            </li>
                            
                            @if ($menu_ativo == 'site_produtos')
                            <li class="ativo">
                            @else
                            <li>
                            @endif
                                <a href="{{route('produto::index')}}">Produtos</a>
                            </li>
                            
                            @if ($menu_ativo == 'blog')
                            <li class="ativo">
                            @else
                            <li>
                            @endif
                                <a href="{{url('casa-em-dia')}}">Casa em dia</a>
                            </li>
                            
                            @if ($menu_ativo == 'site_sustentabilidade')
                            <li class="ativo">
                            @else
                            <li>
                            @endif
                                <a href="{{ route('site::institucional::pagina', ['slug' => 'sustentabilidade']) }}">Sustentabilidade</a>
                            </li>
                            
                            @if ($menu_ativo == 'site_downloads')
                            <li class="ativo">
                            @else
                            <li>
                            @endif
                                <a href="{{ route('download::index') }}">Downloads</a>
                            </li>
                            
                            @if ($menu_ativo == 'site_rh')
                            <li class="ativo">
                            @else
                            <li>
                            @endif
                                <a href="{{url('recursos-humanos')}}">Recursos Humanos</a>
                            </li>
                            
                            @if ($menu_ativo == 'site_contato')
                            <li class="ativo">
                            @else
                            <li>
                            @endif
                                <a href="{{url('contato')}}">Contato</a>
                            </li>
                            
                            <li>
                                <div class='control' tabindex='1'>
                                    <div class='btn'></div>
                                     <i class="fa fa-search" aria-hidden="true"></i>

                                </div>
                                <i class="fa fa-times icon-close" aria-hidden="true"></i>
                                
                            </li>
                        </ol>
                        
                    </nav>
                </div>
            </div>
        </header>
        <div class='form'>
            <form method="get" action="{{ route('site_buscar') }}">
                <input class='input-search' placeholder='Digite a sua busca' type='text' name="termo" id='buscar'>
                <input type="submit" title="Buscar" value="Buscar" class="button-search">
            </form>
        </div>
        <script type='text/javascript'>
        document.getElementById('buscar').onkeydown = function(e){
        if(e.keyCode == 13){
        document.getElementById('buscar').submit();
        }
        };
        </script>
        <!-- /header.php -->
        @yield('conteudo')
        
        <footer>
            <div class="centralizar">
                <div id="footer-logo"></div>
                <div id="mapa-site">
                 @include('shared.menu-mapasite')
                </div>
                <div id="footer-social">
                    <h3>CASA KM REDES SOCIAIS</h3>
                    <ol>
                        <li><a href="https://www.youtube.com/channel/UCLhEbNK5P17u_beGXLjZzrQ" target="_blank"><img src="{{ asset('images/icon-footer-youtube.png') }}" alt="youtube"/>/casakm</a></li>
                          <li><a href="https://www.facebook.com/casakmoficial" target="_blank"><img src="{{ asset('images/icon-footer-facebook.png') }}" alt="facebook"/>/casakmoficial</a></li>
                    </ol>
                </div>
                <div id="copyright">
                    <span>Copyright © 2016 - Todos os direitos reservados</span>
                    <span id="assinatura"><a href="http://www.ph2.com.br" target="_blank">Ph2 Full Creativity</a></span>
                </div>
            </div>
        </footer>
        <!-- /footer.php -->
        <script>
            $(function() {
  /* variables */
    var pull     = $('#pull');
    menu           = $('nav ol');
    menuHeight  = menu.height();
    
    $(pull).on('click', function(e) {
        e.preventDefault();
        menu.slideToggle();
    });
    $(window).resize(function(){
            var w = $(window).width();
            if(w > 320 && menu.is(':hidden')) {
                menu.removeAttr('style');
            }
    });
});

        </script>
        @stack('scripts')
    </body>
</html>