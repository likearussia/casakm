<!-- menu-institucional.php -->
<?php // para evitar erro fatal caso a variável não tenha sido definida no controller
(! isset($menu_institucional_ativo)) ? $menu_institucional_ativo = '' : ''; ?>
<ul>
    @if ($menu_institucional_ativo == 'a-casakm')
    <li id="menu-ative">
    @else
    <li>
    @endif
        <a href="{{ url('sobre/a-casakm') }}">Sobre A Casa</a>
    </li>
    
    @if ($menu_institucional_ativo == 'historia')
    <li id="menu-ative">
    @else
    <li>
    @endif
        <a href=" {{ url('sobre/historia') }}">História</a>
    </li>
    
    @if ($menu_institucional_ativo == 'missao-visao-valores')
    <li id="menu-ative">
    @else
    <li>
    @endif
        <a href="{{ url('sobre/missao-visao-valores') }}">Missão, Visão e Valores</a>
    </li>
    
    @if ($menu_institucional_ativo == 'marca')
    <li id="menu-ative">
    @else
    <li>
    @endif
        <a href="{{ url('sobre/marca') }}">A marca Casa KM</a>
    </li>
    
    @if ($menu_institucional_ativo == 'nossa-casa')
    <li id="menu-ative">
    @else
    <li>
    @endif
        <a href="{{ url('sobre/nossa-casa') }}">Da nossa casa para sua casa</a>
    </li>
    
    @if ($menu_institucional_ativo == 'borboleta')
    <li id="menu-ative">
    @else
    <li>
    @endif
        <a href="{{ url('sobre/borboleta') }}">A borboleta da Casa KM</a>
    </li>
</ul>
<!-- /menu-institucional.php -->