<ul>
    <li><a href="{{url('sobre/a-casakm')}}">Casa KM</a></li>
    <li><a href="{{url('casa-em-dia')}}">Casa em dia</a></li>
    <li><a href="{{ route('site::institucional::pagina', ['slug' => 'sustentabilidade']) }}">Sustentabilidade</a></li>
    <li><a href="{{ route('download::index') }}">Downloads</a></li>
    <li><a href="{{url('recursos-humanos')}}">Recursos Humanos</a></li>
    <li><a href="{{url('contato')}}">Contato</a></li>
</ul>
<ul>
    <li>Produtos</li>
    @if (isset($mapa_site) && isset($mapa_site['produtos']))
        @foreach ($mapa_site['produtos'] as $produto)
            <li class="mapa-text">
                <a href="{{ route('produto::ver', ['slug' => $produto->slug]) }}">{{ $produto->nome }}</a>
            </li>
        @endforeach
    @endif
</ul>
<ul>
    <li>Marcas</li>
    @if (isset($mapa_site) && isset($mapa_site['marcas']))
        @foreach ($mapa_site['marcas'] as $marca)
            <li class="mapa-text">
                <a href="{{ route('produto::por_marca', ['slug' => $marca->slug])}}">{{ $marca->nome }}</a>
            </li>
        @endforeach
    @endif
</ul>