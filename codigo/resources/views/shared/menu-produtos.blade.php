<?php // para evitar erro fatal caso a variável não tenha sido definida no controller
(! isset($menu_produto_ativo)) ? $menu_produto_ativo = '' : '';
$contador = 0;
?>

<ul id="accordion" class="accordion menu-produtos">
    @foreach ($categorias as $categoria)
        <?php
        $slugs = [];
        foreach ($categoria->itens->toArray() as $c) {
            $slugs[] = $c['slug'];
        } ?>
        @if (($menu_produto_ativo == 'index' && $contador == 0) || (in_array($menu_produto_ativo, $slugs)))
            <li class="default open menu-produtos-uso">
        @else
            <li class="menu-produtos-uso">
        @endif
                <div class="link"></i>{{ strtoupper($categoria->descricao) }}<i class="fa fa-chevron-down"></i></div>
                <ul class="submenu">
                    @foreach ($categoria->itens as $item)
                        <?php $url_para_uso = route('produto::por_uso', ['slug' => $item->slug]); ?>
                        @if (Request::url() == $url_para_uso)
                            <li class="ativo"><img src="{{ asset("images/{$item->imagem}") }}" alt="sala"/><a href="{{ $url_para_uso }}">{{ strtoupper($item->descricao) }}</a></li>
                        @else
                            <li><img src="{{ asset("images/{$item->imagem}") }}" alt="sala"/><a href="{{ route('produto::por_uso', ['slug' => $item->slug]) }}">{{ strtoupper($item->descricao) }}</a></li>
                        @endif
                    @endforeach
                </ul>
            </li>
        <?php $contador++ ; ?>
    @endforeach
    
    @if ($menu_produto_ativo == 'marcas')
    <li class="default open menu-produtos-marca">
    @else
    <li class="menu-produtos-marca">
    @endif
        <div class="link">Marcas<i class="fa fa-chevron-down"></i></div>
        <ul class="submenu">
            @foreach ($marcas as $marca)
                <?php $url_para_marca = route('produto::por_marca', ['slug' => $marca->slug]); ?>
                @if (Request::url() == $url_para_marca)
                    <li class="ativo"><a href="{{ $url_para_marca }}">{{ strtoupper($marca->nome) }}</a></li>
                @else
                    <li><a href="{{ $url_para_marca }}">{{ strtoupper($marca->nome) }}</a></li>
                @endif
            @endforeach
        </ul>
    </li>
</ul>