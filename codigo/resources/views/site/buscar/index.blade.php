@extends('layouts.site')
@push('css')
    <link href="{{ asset('css/interna.css') }}" rel="stylesheet">
@endpush


@section('conteudo')
    <div id="content-banner">
        <div id="nav-breadcrumb">
            <div class="centralizar">
                <div class="step-indicator">
                    <a class="step completed" href="{{ url('/') }}">Home</a>
                    <a class="step completed" href="#">Busca</a>
                    <a class="step step-azul">{{Request::input('termo')}}</a>
                </div>
            </div>
        </div>

        <div class="centralizar">
            <div id="content-busca">
            <div id="content-busca-left">
                <p>Resultado da busca</p>
            </div>
                <div id="content-busca-right">
                    <p>{{ $total_resultados }} RESULTADOS PARA: <span>{{Request::input('termo')}}</span></p>
                </div>
                
            
            </div>
            <div id="content-left">

            </div>
            <div id="content-right" class="busca-box">
                @forelse ($resultados as $resultado)
                    <div class="content-produto-01 box-produto">
                        <h1>{{ $resultado['titulo'] }}</h1>
                        @if ($resultado['tipo'] == 'produto')
                            <span class="marcador"> <a href="{{ route('produto::index') }}">Produtos</a> </span>
                        @elseif ($resultado['tipo'] == 'post')
                            <span class="marcador"> <a href="{{ route('blog::index') }}">Blog</a> </span>
                        @endif
                        <div class="produto-imagem">
                            @if ($resultado['tipo'] == 'produto')
                                <img src="{{ asset("$dir_imagem_produto/{$resultado['imagem']}") }}" alt="{{ $resultado['titulo'] }}"/>
                            @elseif ($resultado['tipo'] == 'post')
                                <img src="{{ asset("$dir_imagem_post/{$resultado['imagem']}") }}" alt="{{ $resultado['titulo'] }}"/>
                            @endif
                        </div>
                        {{--
                        <span>
                            {{ str_limit(strip_tags($resultado['texto']), 50) }}
                        </span>
                        --}}
                        @if ($resultado['tipo'] == 'produto')
                            <i class="hovicon effect-6"><a href="{{ route('produto::ver', ['slug' =>$resultado['slug']]) }}">+</a></i>
                        @elseif ($resultado['tipo'] == 'post')
                            <i class="hovicon effect-6"><a href="{{ route('blog::post::visualizar', ['slug' =>$resultado['slug']]) }}">+</a></i>
                        @endif
                    </div>
                @empty
                    <p>Nenhum resultado encontrado</p>
                @endforelse
                {{-- Paginacao --}}
                {!! $resultados->appends(['termo' => Request::input('termo')])->render() !!}
            </div>
        </div>
    </div>
@endsection

@push('scripts-cabecalho')
   
@endpush

@push('scripts')
    <script>
        $('.control').click(function () {
            $('body').addClass('mode-search');
            $('.input-search').focus();
        });
        $('.icon-close').click(function () {
            $('body').removeClass('mode-search');
        });    
    </script>
@endpush
