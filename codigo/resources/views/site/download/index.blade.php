@extends('layouts.site')
@push('css')
<link href="{{ asset('css/interna.css') }}" rel="stylesheet">
@endpush

<?php /* @section('borboleta')
<a href="{{url('/')}}"><img src="{{ asset('images/logo.png') }}" alt="logo"/></a>
@endsection */ ?>

@section('conteudo')    

     <div id="content-banner">
            <img src="{{ asset('images/banner-downloads.jpg') }}"/>
            <div id="nav-breadcrumb">
                <div class="centralizar">
                    <div class="step-indicator">
                        <a class="step completed" href="{{url('/')}}">Home</a>
                        <a class="step step-azul" href="#">Downloads</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="centralizar institucional-top">
        <div id="centralizar-box">Nesta seção você tem acesso ao download de logos, catálogos e FISPQ.</div>
            <div id="content-left">
                <div id="menu-institucional">
                   <h2>Área Restrita</h2>
                   <ul>
                       <li style="float:left">
                           <p>
                               Área exclusiva para acesso aos representantes e gerentes regionais de vendas.
                               Para entrar insira seu código abaixo.
                           </p>
                           <p>
                               Caso deseje obter um código, <a href="{{ route('contato_inicio') }}">entre em contato.</a>.
                           </p>
                           <div class="login">
                               @include('shared.mensagem')
                               @include('flash::message')
                               
                               @if (! $esta_logado)
                                {{ Form::open(['route' => ['download::login'], 'class' => 'login-container']) }}
                                    <p><input type="text" name="codigo" placeholder="Código" required></p>
                                    <p><input type="submit" value="Entrar"></p>
                                {!! Form::close() !!}
                                @else
                                    <form action="{{ route('download::logoff') }}">
                                        <input type="submit" value="Sair">
                                    </form>
                                @endif
                           </div>
                       </li>
                   </ul>
                </div>
            </div>
            <div id="content-right" class="institucional downloads">
            <?php $categoria_antiga = ""; ?>
                @forelse($downloads as $download)
                <?php if ($download->categoria != $categoria_antiga) : ?>
                     <h2>{{ $download->categoria }}</h2>
                <?php else : ?>
                    <!-- nome categoria -->
                <?php endif; $categoria_antiga = $download->categoria; ?>
                    <ul>
                        <li>
                            <h3>{{ $download->nome }}</h3>
                            <a href="{{ route('download::download', ['slug' => $download->slug]) }}"><span><img src="images/icon-download.png" class="icon-download" alt="icon" >fazer download</span></li></a>
                        </li>
                        </ul>
                    @empty
                        <li><h3>Não há downloads cadastrados</h3></li>
                        </ul>
                    @endforelse

                    <br/>
            </div>
        </div>
@endsection

@push('scripts-cabecalho')

@endpush
@push('scripts')
<script src="{{ asset('dist/js/interna.js') }}" type="text/javascript"></script>
@endpush
