#LAYOUT2#
Nome: {!! $dados['form']['nome'] or '' !!}
Endereço: {!! $dados['form']['endereco_logradouro'] or '' !!}
Numero: {!! $dados['form']['endereco_numero'] or '' !!}
Complemento: {!! $dados['form']['endereco_complemento'] or '' !!}
Bairro: {!! $dados['form']['endereco_bairro'] or '' !!}
Cidade: {!! $dados['form']['cidade'] or '' !!}
UF: {!! $dados['form']['uf'] or '' !!}
CEP: {!! $dados['form']['cep'] or '' !!}
Data de nascimento: {!! $dados['form']['nascimento'] or '' !!}
Sexo: {!! $dados['form']['sexo'] or '' !!}
DDD Residencial:
Telefone Residencial: {!! $dados['form']['telefone'] or '' !!}
DDD Comercial:
Telefone Comercial:
DDD Celular:
Telefone Celular: {!! $dados['form']['telefone_celular'] or '' !!}
DDD Fax: 
Telefone Fax: 
E-mail: {!! $dados['form']['email'] or '' !!}
Mensagem: {!! $dados['form']['mensagem'] or '' !!}