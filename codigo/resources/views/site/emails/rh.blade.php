Nome: {!! $dados['form']['nome'] or '' !!}
Endereço: {!! $dados['form']['endereco_logradouro'] or '' !!}
Cidade: {!! $dados['form']['cidade'] or '' !!}
UF: {!! $dados['form']['uf'] or '' !!}
CEP: {!! $dados['form']['cep'] or '' !!}
Fone: {!! $dados['form']['telefone'] or '' !!}
E-mail: {!! $dados['form']['email'] or '' !!}
Mensagem: {!! $dados['form']['mensagem'] or '' !!}
Assunto: {!! $dados['form']['assunto'] or '' !!}
Complemento: {!! $dados['form']['endereco_complemento'] or '' !!}
Bairro: {!! $dados['form']['endereco_bairro'] or '' !!}
Data de nascimento: {!! $dados['form']['nascimento'] or '' !!}
Outros telefones: {!! $dados['form']['telefone_celular'] or '' !!}
Sexo: {!! $dados['form']['sexo'] or '' !!}