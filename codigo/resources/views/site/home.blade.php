@extends('layouts.site')
@push('css')
 <link href="{{ asset('css/index.css') }}" rel="stylesheet">
@endpush

@section('conteudo')
    <div id="content">
        <div class="container">

            <div class="block-media">
                <div class="carousel-images slider-nav">
                    @foreach ($banners_topo as $banner)
                        <div>
                            <img src="{{ asset("$dir_img_banner_topo/$banner->banner") }}" alt="{{ $banner->texto }}">
                        </div>
                    @endforeach
                </div>
            </div><!-- /.block-media -->
            <div class="block-text">
                <div class="carousel-text slider-for">
                    @foreach ($banners_topo as $banner)
                        <div>
                            <h2>{{ $banner->texto }}</h2>
                            @if ($banner->link_nova_aba == 1)
                                <a href="{{ $banner->link_url }}" target="_blank" ><span>{{ $banner->link_texto }}</span></a>
                            @else
                                <a href="{{ $banner->link_url }}"><span>{{ $banner->link_texto }}</span></a>
                            @endif
                        </div>
                    @endforeach
                </div>
            </div><!-- /.block-text -->
        </div><!-- /.container -->
        <div id="content-banner">
            <a href="{{url('/produtos')}}"><img src="{{ asset('images/banner-produtos.jpg') }}" alt="banner produto"></a>
        </div>

        <!-- footer.php -->
        <div id="content-blog">
            <h1>Casa em dia</h1>
            <div id="blog-desc">Sabe aquele segredinho passado de mãe para a filha? Nós também temos algumas dicas para te ajudar com a sua casa, promovendo o bem-estar e o conforto que você merece. Confira as sugestões que preparamos da nossa casa para sua casa.<br/><br/></div>
            <div id="owl-demo">
                @foreach ($posts as $post)
                    <div class="item">
                        <div class="blog-img">
                            @if(isset($post->miniatura))
                            <img src="{{'/miniaturas/' . $post->miniatura }}" alt="Owl Image">
                            @else
                            <img src="{{ $diretorio_imagens . '/' . $post->imagem_destaque }}" alt="Owl Image">
                            @endif
                        </div>
                        <div class="content-txt">
                            <h2>{{ $post->titulo }}</h2>
                           <a href="{{ route('blog::post::visualizar', ['slug' => $post->slug]) }}"><span>Saiba Mais</span></a>
                        </div>
                    </div>
                @endforeach
            </div>
            <h3 class="text-right"><a href="{{url('casa-em-dia')}}">Veja mais dicas <i class="fa fa-plus-circle" aria-hidden="true"></i></a></h3>
        </div>
    </div>
@endsection

@push('scripts-cabecalho')

@endpush

@push('scripts')
<script>
    $('.control').click( function(){
        $('body').addClass('mode-search');
        $('.input-search').focus();
        });
        $('.icon-close').click( function(){
        $('body').removeClass('mode-search');
        });
        // carousel
        $('.slider-for').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: true,
            fade: true,
            dots: true,
            asNavFor: '.slider-nav',
            autoplaySpeed: 7000,
            speed: 900
        });
        $('.slider-nav').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            asNavFor: '.slider-for',
            arrows: false,
            centerMode: true,
            fade: true,
            focusOnSelect: true,
            autoplay: true,
            autoplaySpeed: 7000,
            speed: 900
        });
        // carousel
        $(document).ready(function() {
        $("#owl-demo").owlCarousel({
        
        // Most important owl features
        items: 3,
        itemsDesktop: [1199, 3],
        itemsDesktopSmall: [979, 2],
        itemsTablet: [768, 2],
        itemsMobile: [479, 1],
        
        //Autoplay
        autoPlay: false, //Set AutoPlay to 3 seconds
        stopOnHover: true,
        
        //Basic Speeds
        slideSpeed : 500,
        paginationSpeed : 800,
        rewindSpeed : 1000,
        
        // Navigation
        navigation : false,
        navigationText : ["prev","next"],
        rewindNav : true,
        scrollPerPage : false,
        
        //Pagination
        pagination : false,
        paginationNumbers: false,
        
        // CSS Styles
        baseClass : "owl-carousel",
        theme : "owl-theme",
        
        //Auto height
        autoHeight : false,
        
        //Mouse Events
        dragBeforeAnimFinish : true,
        mouseDrag : true,
        touchDrag : true,
        
        //Transitions
        transitionStyle : false,
        });
        });

</script>
@endpush
