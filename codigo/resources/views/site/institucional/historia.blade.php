@push('css')
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
@endpush

<section class="cd-horizontal-timeline">
    <div class="timeline">
        <div class="events-wrapper">
            <div class="events">
                <ol>
                    <li><a href="#0" data-date="01/01/1979" class="selected"><span class="time-wrapper"><span class="time">1979</span></span></a></li>
                    <li><a href="#0" data-date="04/01/1979"><span class="time-wrapper"><span class="time">1983</span></span></a></li>
                    <li><a href="#0" data-date="14/01/1979"><span class="time-wrapper"><span class="time">1994</span></span></a></li>
                    <li><a href="#0" data-date="18/01/1979"><span class="time-wrapper"><span class="time">1996</span></span></a></li>
                    <li><a href="#0" data-date="20/01/1979"><span class="time-wrapper"><span class="time">1997</span></span></a></li>
                    <li><a href="#0" data-date="22/01/1979"><span class="time-wrapper"><span class="time">1998</span></span></a></li>
                    <li><a href="#0" data-date="31/01/1979"><span class="time-wrapper"><span class="time">2001</span></span></a></li>
                    <li><a href="#0" data-date="02/02/1979"><span class="time-wrapper"><span class="time">2002</span></span></a></li>
                    <li><a href="#0" data-date="05/02/1979"><span class="time-wrapper"><span class="time">2004</span></span></a></li>
                    <li><a href="#0" data-date="15/02/1979"><span class="time-wrapper"><span class="time">2010</span></span></a></li>
                    <li><a href="#0" data-date="23/02/1979"><span class="time-wrapper"><span class="time">2013</span></span></a></li>
                    <li><a href="#0" data-date="27/02/1979"><span class="time-wrapper"><span class="time">2016</span></span></a></li>
                </ol>

                <span class="filling-line" aria-hidden="true"></span>
            </div> <!-- .events -->
        </div> <!-- .events-wrapper -->

        <ul class="cd-timeline-navigation">
            <li><a href="#0" class="prev inactive">prev</a></li>
            <li><a href="#0" class="next">next</a></li>
        </ul> <!-- .cd-timeline-navigation -->
    </div> <!-- .timeline -->

    <div class="events-content">
        <ol>
            <li class="selected" data-date="01/01/1979">
                <div class="col-meio1">
                    <img src="{{ asset('images/historia-tacolac.jpg') }}">
                </div>
                <div class="col-meio2">
                    <span class="flag">Tacolac</span>
                    <p>Em 1979, a Casa KM inovou e lançou o seu primeiro produto, a cera líquida de alta performance Tacolac, que renova a aparência dos pisos de madeira. </p>
                </div>
            </li>

            <li data-date="04/01/1979">
                <div class="col-meio1">
                    <img src="{{ asset('images/historia-coquel.jpg') }}">
                </div>
                <div class="col-meio2">
                    <span class="flag">Coquel</span>
                    <p>Em 1983, pensando no cuidado com as fibras dos tecidos e no tempo de vida das roupas, a Casa KM lançou o lava-roupas líquido COQUEL. </p>
                </div>
            </li>

            <li data-date="14/01/1979">
                <div class="row">
                    <div class="col-meio1">
                        <img src="{{ asset('images/historia-brilho.jpg') }}">
                    </div>
                    <div class="col-meio2">
                        <span class="flag">Brilho fácil</span>
                        <p>Em 1994, para facilitar o trabalho diário, a Casa KM apresentou a cera líquida de alta performance Brilho Fácil. Uma inovação! Com sistema antipó e efeito antiderrapante. </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-meio1">
                        <img src="{{ asset('images/historia-tombom.jpg') }}">
                    </div>
                    <div class="col-meio2">
                        <span class="flag">Tom bom</span>
                        <p>Em 1994, a hidratação e aparência do couro nunca mais foi a mesma. A Casa KM lançou a linha de tratamento de Couro Tom Bom líquido. </p>
                    </div>
                </div>
            </li>


            <li data-date="18/01/1979">
                <div class="col-meio1">
                    <img src="{{ asset('images/historia-tacolacsuper.png') }}">
                </div>
                <div class="col-meio2">
                    <span class="flag">Tacolac Super</span>
                    <p>Em 1996, a linha Tacolac cresceu e o tratamento dos pisos de madeira nunca mais foi o mesmo. A Casa KM lançou a cera líquida de alta performance Tacolac Super. </p>
                </div>
            </li>

            <li data-date="20/01/1979">
                <div class="col-meio1">
                    <img src="{{ asset('images/historia-limpavidros.png') }}">
                </div>
                <div class="col-meio2">
                    <span class="flag">Limpa Vidros</span>
                    <p>Em 1997, ninguém mais podia perder tempo com a limpeza dos vidros. Foi então que a Casa KM ofereceu uma solução: o Limpa Vidros.</p>
                </div>
            </li>

            <li data-date="22/01/1979">
                <div class="row">
                    <div class="col-meio1">
                        <img src="{{ asset('images/historia-amacitel.jpg') }}"> 
                    </div>
                    <div class="col-meio2">
                        <span class="flag">Amacitel</span>
                        <p>Em 1998, pensando na maciez, no perfume agradável e duradouro nas roupas, a Casa KM desenvolveu o amaciante de roupas Amacitel.</p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-meio1">
                        <img src="{{ asset('images/historia-removedordeceras.png') }}"></div>
                    <div class="col-meio2">
                        <span class="flag">Removedor de Ceras</span>
                        <p>Em 1998, para remover a sujidade e as ceras dos pisos, a Casa KM apresentou mais uma inovação, o Removedor de Ceras, facilitando ainda mais o cuidado com os pisos de madeiras. </p>
                    </div>
                </div>
            </li>


            <li data-date="31/01/1979">
                <div class="row">
                    <div class="col-meio1"><img src="{{ asset('images/historia-casaeperfume.png') }}">
                    </div>
                    <div class="col-meio2">
                        <span class="flag">Casa & Perfume</span>
                        <p>Marcando a virada do século, em 2001, inspirado na perfumaria fina, a Casa KM lançou o que havia de mais moderno no mercado, a linha de Limpadores Perfumados Casa & Perfume. A limpeza de pisos, azulejos e superfícies laváveis nunca mais foi a mesma.</p>
                        </p>
                    </div>
                </div>

                <div class="row">
                    <div class="col-meio1">
                        <img src="{{ asset('images/historia-vidamacia.jpg') }}"> </div>
                    <div class="col-meio2">
                        <span class="flag">Vida Macia &nbsp;<br/> Lava-Roupas e Amaciante</span>
                        <p>Em 2001, pensando no momento especial que é a chegada de um bebê, a Casa KM criou a linha de Lavanderia Infantil, nasceu então o Vida Macia Lava-Roupas e o Vida Macia Amaciante. </p>
                    </div> 
                </div>
            </li>



            <li data-date="02/02/1979">
                <div class="col-meio1">
                    <img src="{{ asset('images/historia-tombom-creme.png') }}"> </div>
                <div class="col-meio2">
                    <span class="flag">Tom Bom Creme</span>
                    <p>Em 2002, a família Tom Bom cresceu! O Tom Bom creme passou a hidratar e renovar a aparência do couro dos sapatos. </p>
                </div>

            </li>

            <li data-date="05/02/1979">
                <div class="col-meio1">
                    <img src="{{ asset('images/historia-coquelgranulado.png') }}"> </div>
                <div class="col-meio2"><span class="flag">Sabão em Pó COQUEL Granulado</span>
                    <p>Em 2004, o que era bom ficou ainda melhor. Para integrar a linha COQUEL, a Casa KM apresentou ao mercado o Sabão em Pó COQUEL Coco Natural, que deixa uma agradável fragrância de coco e ainda cuida das roupas. </p>
                </div>
            </li>

            <li data-date="15/02/1979">
                <div class="col-meio1"><img src="{{ asset('images/historia-multiuso.jpg') }}"></div>
                <div class="col-meio2"><span class="flag">Multiuso Gatilho</span>
                    <p>Os tempos mudaram e a limpeza precisava ser eficiente na remoção de gorduras, sujeiras pesadas, poeiras e fuligens. Além de limpar, era necessário perfumar. Foi então que a Casa KM inovou e lançou o limpador Multiuso. </p> </div>


            </li>

            <li data-date="23/02/1979">
                <div class="col-meio1"><img src="{{ asset('images/historia-amacitel-super.jpg') }}"></div>
                <div class="col-meio2"><span class="flag">Amacitel Super</span>
                    <p>Em 2013, a linha Amacitel ganhou reforço. A maciez e o perfume nas roupas também. A Casa KM lançou o Amacitel Super 500 ml. O amaciante concentrado com cápsulas ativas, que deixam as roupas muito mais perfumadas, além de render o mesmo que um amaciante de 2 L.</p>
                </div>

            </li>

            <li data-date="27/02/1979">
                <div class="row">
                    <div class="col-meio1">     
                        <img src="{{ asset('images/historia-sumper.jpg') }}"> </div>
                    <div class="col-meio2"><span class="flag">Lava-Roupas Sumper Super</span>
                        <p>Em 2016, a Casa KM lançou o Sumper Super. O lava-roupas líquido e concentrado da Casa KM. Ele veio revolucionar o mercado! Com rendimento de 30 lavagens, limpa e perfuma as roupas ao mesmo tempo. Um sucesso!</p> </div>

                </div>


                <div class="row">
                    <div class="col-meio1"><img src="{{ asset('images/historia-coquelnatureza.jpg') }}"> </div>
                    <div class="col-meio2"><span class="flag">COQUEL Natureza Limpa</span>
                        <p>2016 segue como o ano da inovação. A linha COQUEL cresceu ainda mais. A Casa KM lançou então o Lava – Roupas COQUEL NATUREZA Limpa 1 kg. Com agradável fragrância de coco, garante qualidade e alta performance nas lavagens com apenas um enxágue, além de não agredir a natureza. </p>
                    </div>
                </div>


            </li>


        </ol>
    </div> <!-- .events-content -->
</section>

@push('scripts-cabecalho')
    <script src="{{ asset('dist/js/main.js') }}" type="text/javascript"></script>
    <script src="{{ asset('dist/js/stopExecutionOnTimeout-ddaa1eeb67d762ab8aad46508017908c.js') }}" type="text/javascript"></script>
@endpush