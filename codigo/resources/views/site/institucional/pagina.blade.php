@extends('layouts.site')

@push('css')
    <link href="{{ asset('css/interna.css') }}" rel="stylesheet">
@endpush

@section('conteudo')
  <div id="content-banner">
        <img src="{{ asset("$diretorio_imagens/{$conteudo->banner}") }}"/>
        <div id="nav-breadcrumb">
            <div class="centralizar">
                <div class="step-indicator">
                    <a class="step completed" href="{{url('/')}}">Home</a>
                    <a class="step step-azul" href="#">{{ $conteudo->titulo }}</a>
                </div>
            </div>
        </div>
    </div>
    <div class="centralizar institucional-top">
        <div id="content-left">
            <div id="menu-institucional">
                <h2>{{ $conteudo->titulo }}</h2>
            </div>
        </div>
        
        <div id="content-right" class="institucional">
            {!! $conteudo->texto !!}
        </div>
        
        {{-- desculpe por isso :( --}}
        @if ($conteudo->id == 7)
            @include('site.institucional.sustentabilidade')
        @endif
    </div>
@endsection

@push('scripts')
    <script src="{{ asset('dist/js/interna.js') }}" type="text/javascript"></script> 
@endpush
