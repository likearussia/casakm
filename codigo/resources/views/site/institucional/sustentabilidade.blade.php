<div class="content-full">   
    <div class="centralizar">
        <h2 class="green">Em resumo, a Casa KM pratica a sustentabilidade com:</h2>
        <div id="box-sustentabilidade">
            <ul>
                <li><img src="{{ asset('images/icon-sustentavel-01.png') }}" alt="sustentabilidade"/><div class="box-texto">Uso de sistemas de tratamento e reaproveitamento da água.</div></li>
                <li><img src="{{ asset('images/icon-sustentavel-04.png') }}" alt="sustentabilidade"/><div class="box-texto">Reutilização de sobras de matéria-prima.</div></li>
                <li><img src="{{ asset('images/icon-sustentavel-07.png') }}" alt="sustentabilidade"/><div class="box-texto">Não há descarte de esgoto ou resíduos químicos em rios, córregos ou lagos.</div></li>
                <li><img src="{{ asset('images/icon-sustentavel-03.png') }}" alt="sustentabilidade"/><div class="box-texto">Uso racional da água e da energia elétrica.</div></li>
                <li><img src="{{ asset('images/icon-sustentavel-05.png') }}" alt="sustentabilidade"/><div class="box-texto">Uso de materiais recicláveis para a confecção de embalagens dos produtos.</div></li>
                <li><img src="{{ asset('images/icon-sustentavel-08.png') }}" alt="sustentabilidade"/><div class="box-texto">Uso de filtros que retém os poluentes emitidos em determinadas fases da produção industrial.</div></li>
                <li><img src="{{ asset('images/icon-sustentavel-06.png') }}" alt="sustentabilidade"/><div class="box-texto">Reciclagem do lixo sólido.</div></li>
                <li><img src="{{ asset('images/icon-sustentavel-09.png') }}" alt="sustentabilidade"/><div class="box-texto">Respeito total às leis ambientais do país.</div></li>
                <li><img src="{{ asset('images/icon-sustentavel-02.png') }}" alt="sustentabilidade"/><div class="box-texto">Uso de práticas de produção que garantam a total segurança dos funcionários no ambiente de trabalho.</div></li>
            </ul>
        </div>
    </div>
</div>


@push('scripts')
    <script src="{{ asset('dist/js/interna.js') }}" type="text/javascript"></script>
@endpush
