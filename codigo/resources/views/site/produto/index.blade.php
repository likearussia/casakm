@extends('layouts.site')
@push('css')
 <link href="{{ asset('css/interna.css') }}" rel="stylesheet">
@endpush

<?php /* @section('borboleta')
<a href="{{url('/')}}"><img src="{{ asset('images/logo.png') }}" alt="logo"/></a>
@endsection */ ?>

@section('conteudo')
    <div id="content-banner">
        <img src="{{ asset('images/banner-produtos-interno.jpg') }}"/>
        <div id="nav-breadcrumb">
            <div class="centralizar">
                <div class="step-indicator">
                    <a class="step completed" href="{{ url('/') }}">Home</a>
                    <a class="step step-azul" href="{{ route('produto::index') }}">Produtos</a>
                </div>
            </div>
        </div>
    </div>
     <div class="centralizar">
            <div id="centralizar-box">A Casa KM sempre busca inspiração na natureza para desenvolver produtos que proporcionem bem-estar, alta perfumação, limpeza, aconchego e aquela sensação única de reconhecimento de estar em casa.
                <br/><strong>Navegue pelas seções e conheça nossos produtos.</strong></div>
            <div id="content-left">
                @include('shared.menu-produtos', ['categorias' => $categorias, 'marcas' => $marcas])

            </div>
            <div id="content-right">
                <?php $i = 1;
                foreach ($produtos as $produto):
                    if ($i % 2 == 0) {
                        $classe = '02';
                    } else {
                        $classe = '01';
                    }
                    ?>
                    <div class="content-produto-<?= $classe ?>">
                        <h1>{{ $produto->nome }}</h1>
                        <div class="produto-imagem">
                            <img src="{{ asset("$diretorio_imagens/{$produto->imagem}") }}" alt="{{ $produto->nome }}"/>
                        </div>
                        <i class="hovicon effect-6"><a href="{{ route('produto::ver', ['slug' => $produto->slug]) }}">+</a></i>
                    </div>
                    <?php $i++;
                endforeach; ?>
                
                {{-- Paginacao --}}
                {!! $produtos->links() !!}
            </div>
        </div>
@endsection

@push('scripts-cabecalho')

@endpush

@push('scripts')
<script src="{{ asset('dist/js/interna.js') }}" type="text/javascript"></script>
 <script>
        // menu
        $(function () {
            var Accordion = function (el, multiple) {
                this.el = el || {};
                this.multiple = multiple || false;
                // Variables privadas
                var links = this.el.find('.link');
                // Evento
                links.on('click', {el: this.el, multiple: this.multiple}, this.dropdown)
            }
            Accordion.prototype.dropdown = function (e) {
                var $el = e.data.el;
                $this = $(this),
                        $next = $this.next();
                $next.slideToggle();
                $this.parent().toggleClass('open');
                if (!e.data.multiple) {
                    $el.find('.submenu').not($next).slideUp().parent().removeClass('open');
                }
                ;
            }
            var accordion = new Accordion($('#accordion'), false);
        });
    </script>
@endpush
