@extends('layouts.site')
@push('css')
 <link href="{{ asset('css/interna.css') }}" rel="stylesheet">
@endpush

<?php /* @section('borboleta')
<a href="{{url('/')}}"><img src="{{ asset('images/logo.png') }}" alt="logo"/></a>
@endsection */ ?>

@section('conteudo')
<div id="content-banner">
    <img src="{{ asset("$diretorio_imagens_banners/{$produto->imagem_banner}") }}"/>
    <div id="nav-breadcrumb">
        <div class="centralizar">
            <div class="step-indicator">
                <a class="step completed" href="{{ url('/') }}">Home</a>
                <a class="step completed" href="{{url('/produtos')}}">Produtos</a>
                <a class="step step-azul" href="#">{{ $produto->nome }}</a>
            </div>
        </div>
    </div>
</div>
<div id="titulo-produtos">
    <div id="titulo-centralizar">
        <h1>{!! $produto->nome !!}</h1>
        <h2>{!! $produto->descricao_curta !!}</h2>
    </div>
</div>
<article id="produtos-descricao">
    <div class="centralizar">
        <section id="produto-imagem">
            <img src="{{ asset("$diretorio_imagens_packs/{$produto->imagem_pack}") }}" alt="{!! $produto->nome !!}"/>
            
            <!--modal-->
           

            </section>
            <section id="produto-descricao">
                {!! $produto->descricao_longa !!}
                <h2>Características do produto</h2>
                <ul>
                    {!! $produto->caracteristicas !!}
                </ul>
                <div class="produto-infos">
                    <h2>Compartilhar</h2>
                     <ol class="rrssb-buttons clearfix">
          
                  <li class="rrssb-facebook">
                    <!--  Replace with your URL. For best results, make sure you page has the proper FB Open Graph tags in header: https://developers.facebook.com/docs/opengraph/howtos/maximizing-distribution-media-content/ -->
                    <a href="https://www.facebook.com/sharer/sharer.php?u=http://casakm.sili.com.br/" class="popup">
                     <img src="{{ asset('images/icon-social-facebook.png') }}" alt="facebook"/>
                    </a>
                  </li>
                  <li class="rrssb-twitter">
                    <!-- Replace href with your Meta and URL information  -->
                    <a href="https://twitter.com/intent/tweet?text=Veja só essa dica incrível"
                    class="popup">
                      <img src="{{ asset('images/icon-social-twitter.png') }}" alt="facebook"/>
                    </a>
                  </li>
                  <li class="rrssb-instagram">
                      <li><a href="http://instagram.com/casakm"><img src="{{ asset('images/icon-social-instagram.png') }}" alt="instagram"/></a></li>
                  </li>
                </ol>
                </div>
                <div class="produto-infos">
                    <h2>Pode ser usado:</h2>
                    <div class="produtos-usado">
                        @foreach ($produto->usos as $uso)
                        @foreach ($itens_uso as $item)
                        @if ($item->id == $uso->produtos_usos_categoria_item_id)
                        <div data-tooltip="{{ $item->descricao }}" class="produtos-usado__dot produtos-usado__dot--active"><img src="{{ asset("images/{$item->imagem}") }}" alt="sala"/></div>
                        @endif
                        @endforeach
                        @endforeach
                    </div>
                </div>
            </section>
        </div>
    </article>
    <div id="bg-content-left">        
        <div id="content-left-centralizar">
            <ul id="accordion" class="accordion">
                    <li class="default open">
                        <div class="link"></i>Informações de uso<i class="fa fa-chevron-down"></i></div>
                        <ul class="submenu">
                             <li>{!! $produto->informacoes_uso !!}</li>
                        </ul>
                    </li>
                    <li>
                        <div class="link"></i>Informações de segurança<i class="fa fa-chevron-down"></i></div>
                        <ul class="submenu">
                            <li>
                                {!! $produto->informacoes_seguranca !!}
                            </li>
                        </ul>
                    </li>
                    <li>
                </ul>
        </div>
    </div>
    <div id="produtos-disponivel">
        <h2>Disponível em</h2>
        
        <div class="centralizar">
            <ul>
                @forelse ($versoes as $versao)
                <li>
                    <h2>{{ $versao->nome }}</h2>
                    <img src="{{ asset("$diretorio_imagens_versoes/$versao->imagem") }}" />
                    <a href="{{ asset("$diretorio_imagens_versoes/$versao->imagem") }}" target='_blank'><div class='overlay'> <span class='plus'>Download Imagem</span></div></a>
                </li>
                @empty
                &nbsp;
                @endforelse
            </ul>
        </div>
    </div>

    <div id="produtos-relacionados">
        @if ($similares)
        <h2>Linha de produtos</h2>
        <ul>
            @foreach ($similares as $similar)
            <li>
                <h1>{{ $similar->nome }} </h1>
                <div class="produto-imagem">
                    <img src="{{ asset("$diretorio_imagens/{$similar->imagem}") }}" alt="{{ $similar->nome }}"/>
                </div>
                <i class="hovicon effect-6"><a href="{{ url("produtos/{$similar->slug}") }}">+</a></i>
            </li>
            @endforeach
        </ul>
        @endif
    </div>
            @endsection
            @push('scripts-cabecalho')
            @endpush
            @push('scripts')
            <script src="{{ asset('dist/js/interna.js') }}" type="text/javascript"></script>
 <script>
        // menu
        $(function () {
            var Accordion = function (el, multiple) {
                this.el = el || {};
                this.multiple = multiple || false;
                // Variables privadas
                var links = this.el.find('.link');
                // Evento
                links.on('click', {el: this.el, multiple: this.multiple}, this.dropdown)
            }
            Accordion.prototype.dropdown = function (e) {
                var $el = e.data.el;
                $this = $(this),
                        $next = $this.next();
                $next.slideToggle();
                $this.parent().toggleClass('open');
                if (!e.data.multiple) {
                    $el.find('.submenu').not($next).slideUp().parent().removeClass('open');
                }
                ;
            }
            var accordion = new Accordion($('#accordion'), false);
        });
    </script>
            @endpush
