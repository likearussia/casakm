@extends('layouts.site')
@push('css')
 <link href="{{ asset('css/interna.css') }}" rel="stylesheet">
@endpush

<?php /* @section('borboleta')
<a href="{{url('/')}}"><img src="{{ asset('images/logo.png') }}" alt="logo"/></a>
@endsection */ ?>


@section('conteudo')
<div id="content-banner">
    <img src="{{ asset('images/banner-rh.jpg') }}"/>
    <div id="nav-breadcrumb">
        <div class="centralizar">
            <div class="step-indicator">
                <a class="step completed" href="#">Home</a>
                <a class="step step-azul" href="#">Recursos Humanos</a>
            </div>
        </div>
    </div>
</div>
<div class="centralizar institucional-top">
    <div id="content-left">
        <div id="menu-institucional">
            <h2>Recursos Humanos</h2>
            @include('shared.menu-recursos')

        </div>
    </div>
    <div id="content-right" class="institucional">
        <h2 id="titulo-institucional">Recursos Humanos</h2>
        <p>
            A Casa KM também investe seus cuidados em pessoas. Somos uma empresa que exerce os valores que acredita e procura pessoas com os mesmos ideais: convicção, criatividade, garra, integridade e paixão.<br/><br/>
            Em nosso ambiente de trabalho esperamos contar com novas ideias, além da disposição para enfrentar grandes desafios, assim, todos podem aprender e se desenvolver juntos.<br/><br/>
            Se você tem paixão, gosta de desafios e reconhece em si os nossos valores, venha trabalhar com a gente. <strong>Preencha os campos abaixo e envie o seu currículo!</strong><br/>
        </p>
            
            @include('shared.erro-validacao')
            @include('shared.mensagem')
            @include('flash::message')
        <div id="form-contato">
            {{ Form::open(['route' => ['rh_enviar'], 'files' => true]) }}
                <div class="input-01">
                    {{ Form::text('nome', null, ['placeholder' => 'Nome completo', 'required' => 'required', 'maxlength' => '80']) }}
                </div>
                <div class="input-02-left">
                    <input type="email" placeholder="Seu e-mail" name="email" value="{{ old('email') }}" required required maxlength="80" />
                </div>
                <div class="input-02-right">
                    <label>Sexo</label>
                    <label>Masculino</label><input type="radio" name="sexo" value="Masculino"/>
                    <label>Feminino</label><input type="radio" name="sexo" value="Feminino" />
                </div>
                <div class="clean"></div>
                <div class="input-03-left">
                    <input type="text" placeholder="Preencha seu Telefone"  id="telefone" name="telefone" value="{{ old('telefone') }}" maxlength="20" />
                </div>
                <div class="input-03-middle">
                    <input type="text" placeholder="Preencha seu Celular"  id="celular" name="telefone_celular" value="{{ old('telefone_celular') }}" maxlength="20" />
                </div>

                <div class="input-03-right">
                    <input type="text" placeholder="Data de Nascimento" id="data" name="nascimento" value="{{ old('nascimento') }}" />
                </div>
                <div class="input-04-left">
                    <input name="cep" type="number" id="cep" value="" size="10" maxlength="8"
                           onblur="pesquisacep(this.value);" placeholder="CEP" name="cep" value="{{ old('cep') }}" class="cep"/>
                </div>
                <div class="input-04-right">
                    <input type="text" id="rua" placeholder="Endereço" name="endereco_logradouro" value="{{ old('endereco_logradouro') }}" maxlength="45" />
                </div>
                <div class="clean"></div>
                <div class="input-03-left">
                    <input type="text" placeholder="Número" name="endereco_numero" value="{{ old('endereco_numero') }}" maxlength="4" />
                </div>
                <div class="input-03-middle">
                    <input type="text" placeholder="Complemento" name="endereco_complemento" value="{{ old('endereco_complemento') }}" />
                </div>
                <div class="input-03-right">
                    <input type="text" id="bairro" size="40" placeholder="Bairro" name="endereco_bairro" value="{{ old('endereco_bairro') }}" />
                </div>
                <div class="clean"></div>
                <div class="input-03-left">
                    <input type="text" id="cidade" size="40"  placeholder="Cidade" name="cidade" value="{{ old('cidade') }}" maxlength="60" />
                </div>
                <div class="input-03-middle">
                    <input type="text" id="uf" size="2" placeholder="Estado" name="uf" value="{{ old('uf') }}" maxlength="2"/>
                </div>
                <div class="input-03-right">
                    {{ Form::select('assunto', $departamentos, null,
                                        ['placeholder' => 'Selecione o departamento', 'class' => 'custom-select', 'required']) }}
                </div>
                <div class="input-01">
                    <textarea name="mensagem" id="textarea" placeholder="Outras informações" required maxlength="4000">{{ old('mensagem') }}</textarea>
                </div>
                <div class="input-01" id="curriculo">
                    <label for="file">Anexe seu curriculo</label>
                    <input type="file" name="curriculo" id="file" />
                </div>
                <div class="input-01">
                    <input type="submit" name="enviar" value="enviar">
                </div>
            {!! Form::close() !!}
        </div> <!-- /form-contato -->

        </p>
    </div>
</div>
@endsection

@push('scripts')
    <script src="{{ asset('dist/js/mascara.js') }}" type="text/javascript"></script>
    <script src="{{ asset('dist/js/interna.js') }}" type="text/javascript"></script>
    <script>
        $(document).ready(function(){
           
           $('#file').change(function(){
               var file = $(this).val().split('\\');
               
               $('#curriculo').append("<div class='fileName'>"+file[2]+"<a class='removeC'> x </a></div>");
               setTimeout(function(){
               $('.removeC').click(function(){
                    $('#file').val('');
                    
                    $('.fileName').remove();
                 });    
               },300);
           });
           
           
           
           
        });
    // <![CDATA[
    jQuery(function($) {
        $.mask.definitions['~']='[+-]';
        //Inicio Mascara Telefone
        $('#celular').focusout(function() {
            var phone, element;
            element = $(this);
            element.unmask();
            phone = element.val().replace(/\D/g, '');
            if(phone.length > 10) {
                element.mask("(99) 99999-999?9");
            } else {
                element.mask("(99) 9999-9999?9");
            }
        }).trigger('focusout');
        //Fim Mascara Telefone
        $("#telefone").mask("(99)9999-9999");
        $("#data").mask("99/99/9999");
    });
    // ]]>
    function limpa_formulario_cep() {
        //Limpa valores do formulário de cep.
        document.getElementById('rua').value=("");
        document.getElementById('bairro').value=("");
        document.getElementById('cidade').value=("");
        document.getElementById('uf').value=("");
    }
    function meu_callback(conteudo) {
        if (!("erro" in conteudo)) {
        //Atualiza os campos com os valores.
            document.getElementById('rua').value=(conteudo.logradouro);
            document.getElementById('bairro').value=(conteudo.bairro);
            document.getElementById('cidade').value=(conteudo.localidade);
            document.getElementById('uf').value=(conteudo.uf);
        } //end if.
        else {
            //CEP não Encontrado.
            limpa_formulario_cep();
            alert("CEP não encontrado.");
        }
    }

    function pesquisacep(valor) {
        //Nova variável "cep" somente com dígitos.
        var cep = valor.replace(/\D/g, '');
        //Verifica se campo cep possui valor informado.
        if (cep != "") {
            //Expressão regular para validar o CEP.
            var validacep = /^[0-9]{8}$/;
            //Valida o formato do CEP.
            if(validacep.test(cep)) {
                //Preenche os campos com "..." enquanto consulta webservice.
                document.getElementById('rua').value="...";
                document.getElementById('bairro').value="...";
                document.getElementById('cidade').value="...";
                document.getElementById('uf').value="...";
                //Cria um elemento javascript.
                var script = document.createElement('script');
                //Sincroniza com o callback.
                script.src = '//viacep.com.br/ws/'+ cep + '/json/?callback=meu_callback';
                //Insere script no documento e carrega o conteúdo.
                document.body.appendChild(script);
            } //end if.
            else {
                //cep é inválido.
                limpa_formulario_cep();
                alert("Formato de CEP inválido.");
            }
        } //end if.
        else {
            //cep sem valor, limpa formulário.
            limpa_formulario_cep();
        }
    };
    </script>
@endpush
