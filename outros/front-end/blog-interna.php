<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Casa KM - Da nossa casa, para sua casa.</title>
        <!-- Bootstrap -->
        <link href="dist/css/geral.css" rel="stylesheet">
        <link href="dist/css/interna.css" rel="stylesheet">
        <link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <script src="dist/js/jquery.min.js" type="text/javascript"></script>
        <script src="dist/js/slick.min.js" type="text/javascript"></script>
        <script src="dist/js/jquery-ui.js" type="text/javascript"></script>
        <script src="dist/js/galleria-1.4.2.min.js"></script>
        <script src="https://use.typekit.net/bux4bzx.js"></script>
        <script>try{Typekit.load({ async: true });}catch(e){}</script>
    </head>
    <body>
        <?php include "header.php";?>
        <div id="content-banner">
            <div id="nav-breadcrumb">
                <div class="centralizar">
                    <div class="step-indicator">
                        <a class="step completed" href="#">Home</a>
                        <a class="step step-rosa" href="#">Casa em dia</a>
                    </div>
                </div>
            </div>
        </div>
        <div id="titulo-blog">
            <div id="titulo-centralizar">
                <h2>dicas</h2>
                <h1>Lava-Roupas concentrado Sumper super</h1>
                <h3>28/05/2016</h3>
            </div>
        </div>
        <article id="produtos-descricao">
            <div id="bg-content-left">
                <div id="content-left-centralizar">
                    <p>Lembrando daquela música que diz: Moro em um país tropical abençoado e bonito por natureza... os cuidados com o que é nosso, com a cidade na qual vivemos e com a proteção do meio ambiente, são o início para se ter uma vida mais saudável para nós, nossos familiares, amigos e para a vizinhança. Afinal a limpeza e o cuidado do nosso lar é fundamental pra essa qualidade de vida, saúde e aconchego, mas o que está além dela também influencia diretamente em nosso dia a dia. Repensar na forma como fazemos nossa parte em prol da conservação do meio ambiente, sustentabilidade e no respeito que temos pelo nosso planeta faz bem pra gente e para as futuras gerações.
                    Existem alguns pequenos hábitos que se começarmos em casa a modificar, ajudam e muito a preservar a natureza. Alguns deles são, por exemplo, trocar aquele velho hábito de encher o tanque de água para encher o balde, deixar a torneira ligada até o balde transbordar ou ainda usar a mangueira desperdiçando água por um longo período. Além do cuidado com o planeta, que deveria ser um hábito, precisamos nos atentar aos últimos acontecimentos como o racionamento de água.</p>
                    <div class="content">
                        <!-- Adding gallery images. We use resized thumbnails here for better performance, but it’s not necessary -->
                        <div id="galleria">
                            <a href="http://upload.wikimedia.org/wikipedia/commons/thumb/a/a2/Biandintz_eta_zaldiak_-_modified2.jpg/800px-Biandintz_eta_zaldiak_-_modified2.jpg">
                                <img
                                src="http://upload.wikimedia.org/wikipedia/commons/thumb/a/a2/Biandintz_eta_zaldiak_-_modified2.jpg/100px-Biandintz_eta_zaldiak_-_modified2.jpg",
                                data-big="http://upload.wikimedia.org/wikipedia/commons/thumb/a/a2/Biandintz_eta_zaldiak_-_modified2.jpg/1280px-Biandintz_eta_zaldiak_-_modified2.jpg"
                                data-title="Biandintz eta zaldiak"
                                data-description="Horses on Bianditz mountain, in Navarre, Spain."
                                >
                            </a>
                            <a href="http://upload.wikimedia.org/wikipedia/commons/thumb/6/6c/Athabasca_Rail_at_Brule_Lake.jpg/800px-Athabasca_Rail_at_Brule_Lake.jpg">
                                <img
                                src="http://upload.wikimedia.org/wikipedia/commons/thumb/6/6c/Athabasca_Rail_at_Brule_Lake.jpg/100px-Athabasca_Rail_at_Brule_Lake.jpg",
                                data-big="http://upload.wikimedia.org/wikipedia/commons/thumb/6/6c/Athabasca_Rail_at_Brule_Lake.jpg/1280px-Athabasca_Rail_at_Brule_Lake.jpg"
                                data-title="Athabasca Rail"
                                data-description="The Athabasca River railroad track at the mouth of Brulé Lake in Alberta, Canada."
                                >
                            </a>
                            <a href="http://upload.wikimedia.org/wikipedia/commons/thumb/1/1f/Back-scattering_crepuscular_rays_panorama_1.jpg/1280px-Back-scattering_crepuscular_rays_panorama_1.jpg">
                                <img
                                src="http://upload.wikimedia.org/wikipedia/commons/thumb/1/1f/Back-scattering_crepuscular_rays_panorama_1.jpg/100px-Back-scattering_crepuscular_rays_panorama_1.jpg",
                                data-big="http://upload.wikimedia.org/wikipedia/commons/thumb/1/1f/Back-scattering_crepuscular_rays_panorama_1.jpg/1400px-Back-scattering_crepuscular_rays_panorama_1.jpg"
                                data-title="Back-scattering crepuscular rays"
                                data-description="Picture of the day on Wikimedia Commons 26 September 2010."
                                >
                            </a>
                            <a href="http://upload.wikimedia.org/wikipedia/commons/thumb/7/75/Interior_convento_3.jpg/800px-Interior_convento_3.jpg">
                                <img
                                src="http://upload.wikimedia.org/wikipedia/commons/thumb/7/75/Interior_convento_3.jpg/120px-Interior_convento_3.jpg",
                                data-big="http://upload.wikimedia.org/wikipedia/commons/thumb/7/75/Interior_convento_3.jpg/1400px-Interior_convento_3.jpg"
                                data-title="Interior convento"
                                data-description="Interior view of Yuriria Convent, founded in 1550."
                                >
                            </a>
                            <a href="http://upload.wikimedia.org/wikipedia/commons/thumb/6/6b/Oxbow_Bend_outlook_in_the_Grand_Teton_National_Park.jpg/800px-Oxbow_Bend_outlook_in_the_Grand_Teton_National_Park.jpg">
                                <img
                                src="http://upload.wikimedia.org/wikipedia/commons/thumb/6/6b/Oxbow_Bend_outlook_in_the_Grand_Teton_National_Park.jpg/100px-Oxbow_Bend_outlook_in_the_Grand_Teton_National_Park.jpg",
                                data-big="http://upload.wikimedia.org/wikipedia/commons/thumb/6/6b/Oxbow_Bend_outlook_in_the_Grand_Teton_National_Park.jpg/1280px-Oxbow_Bend_outlook_in_the_Grand_Teton_National_Park.jpg"
                                data-title="Oxbow Bend outlook"
                                data-description="View over the Snake River to the Mount Moran with the Skillet Glacier."
                                >
                            </a>
                            <a href="http://upload.wikimedia.org/wikipedia/commons/thumb/c/c9/Hazy_blue_hour_in_Grand_Canyon.JPG/800px-Hazy_blue_hour_in_Grand_Canyon.JPG">
                                <img
                                src="http://upload.wikimedia.org/wikipedia/commons/thumb/c/c9/Hazy_blue_hour_in_Grand_Canyon.JPG/100px-Hazy_blue_hour_in_Grand_Canyon.JPG",
                                data-big="http://upload.wikimedia.org/wikipedia/commons/thumb/c/c9/Hazy_blue_hour_in_Grand_Canyon.JPG/1280px-Hazy_blue_hour_in_Grand_Canyon.JPG"
                                data-title="Hazy blue hour"
                                data-description="Hazy blue hour in Grand Canyon. View from the South Rim."
                                >
                            </a>
                            <a href="http://upload.wikimedia.org/wikipedia/commons/thumb/f/f4/2909_vallon_moy_res.jpg/800px-2909_vallon_moy_res.jpg">
                                <img
                                src="http://upload.wikimedia.org/wikipedia/commons/thumb/f/f4/2909_vallon_moy_res.jpg/100px-2909_vallon_moy_res.jpg",
                                data-big="http://upload.wikimedia.org/wikipedia/commons/thumb/f/f4/2909_vallon_moy_res.jpg/1280px-2909_vallon_moy_res.jpg"
                                data-title="Haute Severaisse valley"
                                data-description="View of Haute Severaisse valley and surrounding summits from the slopes of Les Vernets."
                                >
                            </a>
                            <a href="http://upload.wikimedia.org/wikipedia/commons/thumb/4/46/Bohinjsko_jezero_2.jpg/800px-Bohinjsko_jezero_2.jpg">
                                <img
                                src="http://upload.wikimedia.org/wikipedia/commons/thumb/4/46/Bohinjsko_jezero_2.jpg/100px-Bohinjsko_jezero_2.jpg",
                                data-big="http://upload.wikimedia.org/wikipedia/commons/thumb/4/46/Bohinjsko_jezero_2.jpg/1280px-Bohinjsko_jezero_2.jpg"
                                data-title="Bohinj lake"
                                data-description="Bohinj lake (Triglav National Park, Slovenia) in the morning."
                                >
                            </a>
                            <a href="http://upload.wikimedia.org/wikipedia/commons/thumb/3/32/Bowling_Balls_Beach_2_edit.jpg/800px-Bowling_Balls_Beach_2_edit.jpg">
                                <img
                                src="http://upload.wikimedia.org/wikipedia/commons/thumb/3/32/Bowling_Balls_Beach_2_edit.jpg/100px-Bowling_Balls_Beach_2_edit.jpg",
                                data-big="http://upload.wikimedia.org/wikipedia/commons/thumb/3/32/Bowling_Balls_Beach_2_edit.jpg/1280px-Bowling_Balls_Beach_2_edit.jpg"
                                data-title="Bowling Balls"
                                data-description="Mendocino county, California, USA."
                                >
                            </a>
                        </div>
                    </div>
                    <script>
                    // Load the classic theme
                    Galleria.loadTheme('dist/js/galleria.classic.min.js');
                    // Initialize Galleria
                    Galleria.run('#galleria');
                    </script>
                </div>
            </div>
            <div id="bg-content-right">
                <section class="right-box">
                    <h2>Compartilhar</h2>
                </section>
                <section class="right-box">
                    <h2>Tags</h2>
                    <script type="text/javascript">
                    //nuvem tag
                    var tags = ["cms", "javascript", "js", "ASP.NET MVC", ".net", ".net", "css", "wordpress", "xaml", "js", "http", "web", "asp.net", "asp.net MVC", "ASP.NET MVC", "wp", "javascript", "js", "cms", "html","http", "javascript", "http", "http", "CMS","Java","C#","C#","GitHub","CSS","HTML5","css","HTML5","LINQ","Oracle","https","PHP","PHP","PHP","NoSQL","Android","Android","Cacao","vpn","AJAX","XML","XMl","JSON","XML","C++","Web","Canvas","API","Module","Ftp","URI","URI","URL","XTML","KineticJS","Cloud","VPN","Remote","ASP.NET"];
                    createTagCloud(tags,15,34);
                    function createTagCloud(tags, minFontSize, maxFontSize) {
                    var countedWords = countWords(tags);
                    writeTags(countedWords, minFontSize, maxFontSize);
                    
                    function countWords(tags) {
                    var countedWords={};
                    var maxOccurances = 1;
                    
                    for(var i = 0; i < tags.length; i++) {
                    var word = tags[i].toLowerCase();
                    
                    var currentCount = countedWords[word];
                    
                    if (currentCount) {
                    currentCount++;
                    } else {
                    currentCount = 1;
                    }
                    countedWords[word] = currentCount;
                    }
                    return countedWords;
                    }
                    
                    function calculateHeight(maximalOccurances, occurances, minFontSize, maxFontSize) {
                    var heigth;
                    
                    var minOccurances = 1;
                    var fontSizeDifference = maxFontSize - minFontSize;
                    var occurancesDifference = maximalOccurances - minOccurances;
                    
                    var heigth = Math.floor(minFontSize + (occurances - minOccurances)*(fontSizeDifference/occurancesDifference));
                    
                    return heigth;
                    }
                    
                    function writeTags(countedWords, minFontSize, maxFontSize) {
                    var container = document.createElement("div");
                    var maximalOccurances = findMaximalOccurances(countedWords);
                    
                    for (word in countedWords) {
                    var occurances = countedWords[word];
                    var heigth = calculateHeight(maximalOccurances, occurances, minFontSize, maxFontSize);
                    var transperancy =        calculateTransperancy(maximalOccurances,occurances,0.5,1);
                    var newSpan =
                    document.createElement("span");
                    newSpan.innerHTML = word;
                    newSpan.style.fontSize = heigth + "px";
                    newSpan.style.opacity = transperancy;
                    container.appendChild(newSpan);
                    }
                    document.body.appendChild(container);
                    }
                    
                    function findMaximalOccurances(countedWords){
                    var maxOccurances = 1;
                    
                    for (i in countedWords) {
                    if(countedWords[i] > maxOccurances) {
                    maxOccurances  = countedWords[i];
                    }
                    }
                    
                    return maxOccurances;
                    }
                    
                    function calculateTransperancy(maximalOccurances, occurances,minTran, maxTran) {
                    
                    
                    var minOccurances = 1;
                    var tranDifference = maxTran - minTran;
                    var occurancesDifference = maximalOccurances - minOccurances;
                    
                    var transperacy;
                    transperacy = (minTran + (occurances - minOccurances)*(tranDifference/occurancesDifference));
                    
                    return transperacy;
                    }
                    }
                    </script>
                </section>
            </div>
        </article>
        <?php include "footer-produtos.php";?>
        
        <script>
        $('.control').click( function(){
        $('body').addClass('mode-search');
        $('.input-search').focus();
        });
        $('.icon-close').click( function(){
        $('body').removeClass('mode-search');
        });
        </script>
    </body>