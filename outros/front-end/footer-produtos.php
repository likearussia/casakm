

<footer>
  <div class="centralizar">
      <div id="footer-logo"></div>
      <div id="mapa-site">
        <ol>
          <li>Mapa do Site</li>
          <li>Políticas de Privacidade</li>
          <li>Termos de Uso</li>
        </ol>
      </div>
      <div id="footer-social">
        <h3>CASA KM REDES SOCIAIS</h3>
        <ol>
          <li><img src="images/icon-footer-facebook.png" alt="facebook"/>/casakm</li>
          <li><img src="images/icon-footer-instagram.png" alt="facebook"/>/casakm</li>
          <li><img src="images/icon-footer-youtube.png" alt="facebook"/>/casakm</li>
          <li><img src="images/icon-footer-twitter.png" alt="facebook"/>/casakm</li>
      </div>
      <div id="copyright">
          <span>Copyright © 2016 - Todos os direitos reservados</span>
          <span id="assinatura">Ph2 Full Creativity</span>
      </div>
  </div>
</footer>