<div id="content-blog">
  <h1>Casa em dia</h1>
  <div id="blog-desc">Sabe aquele segredinho passado de mãe para a filha? Nós também temos algumas dicas para te ajudar com a sua casa, promovendo o bem-estar e o conforto que você merece. Confira as sugestões que preparamos da nossa casa para sua casa.<br/><br/></div>
  <div id="owl-demo">
    <div class="item">
      <div class="blog-img">
        <img src="http://www.placehold.it/300x250" alt="Owl Image">
      </div>
      <div class="content-txt">
        <h2>Da nossa cassa, para sua casa.</h2>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
        <span>Saiba Mais</span>
      </div>
    </div>
    <div class="item">
      <div class="blog-img">
        <img src="http://www.placehold.it/300x250" alt="Owl Image">
      </div>
      <div class="content-txt">
        <h2>Da nossa cassa, para sua casa.</h2>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
        <span>Saiba Mais</span>
      </div>
    </div>
    <div class="item">
      <div class="blog-img">
        <img src="http://www.placehold.it/300x250" alt="Owl Image">
      </div>
      <div class="content-txt">
        <h2>Da nossa cassa, para sua casa.</h2>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
        <span>Saiba Mais</span>
      </div>
    </div>
    <div class="item">
      <div class="blog-img">
        <img src="http://www.placehold.it/300x250" alt="Owl Image">
      </div>
      <div class="content-txt">
        <h2>Da nossa cassa, para sua casa.</h2>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
        <span>Saiba Mais</span>
      </div>
    </div>
    <div class="item">
      <div class="blog-img">
        <img src="http://www.placehold.it/300x250" alt="Owl Image">
      </div>
      <div class="content-txt">
        <h2>Da nossa cassa, para sua casa.</h2>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
        <span>Saiba Mais</span>
      </div>
    </div>
    <div class="item">
      <div class="blog-img">
        <img src="http://www.placehold.it/300x250" alt="Owl Image">
      </div>
      <div class="content-txt">
        <h2>Da nossa cassa, para sua casa.</h2>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
        <span>Saiba Mais</span>
      </div>
    </div>
    <div class="item">
      <div class="blog-img">
        <img src="http://www.placehold.it/300x250" alt="Owl Image">
      </div>
      <div class="content-txt">
        <h2>Da nossa cassa, para sua casa.</h2>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
        <span>Saiba Mais</span>
      </div>
    </div>
  </div>
  <div class="centralizar">
    <button>Ver Dicas</button>
  </div>
</div>
</div>
<footer>
<div class="centralizar">
  <div id="footer-logo"></div>
  <div id="mapa-site">
    <ol>
      <li>Mapa do Site</li>
      <li>Políticas de Privacidade</li>
      <li>Termos de Uso</li>
    </ol>
  </div>
  <div id="footer-social">
    <h3>CASA KM REDES SOCIAIS</h3>
    <ol>
      <li><img src="images/icon-footer-facebook.png" alt="facebook"/>/casakm</li>
      <li><img src="images/icon-footer-instagram.png" alt="facebook"/>/casakm</li>
      <li><img src="images/icon-footer-youtube.png" alt="facebook"/>/casakm</li>
      <li><img src="images/icon-footer-twitter.png" alt="facebook"/>/casakm</li>
    </div>
    <div id="copyright">
      <span>Copyright © 2016 - Todos os direitos reservados</span>
      <span id="assinatura">Ph2 Full Creativity</span>
    </div>
  </div>
</footer>