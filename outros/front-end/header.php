<header>
  <article id="header-color">
    <div id="color-01"></div>
    <div id="color-02"></div>
    <div id="color-03"></div>
  </article>
  <div class="centralizar">
    <a href="index.php"><img src="images/logo.gif" alt="logo"/></a>
    <div id="nav-right">
      <div id="nav-social">
        <ol>
          <li><img src="images/icon-social-facebook.png" alt="facebook"/></li>
          <li><img src="images/icon-social-instagram.png" alt="instagram"/></li>
          <li><img src="images/icon-social-youtube.png" alt="facebook"/></li>
          <li><img src="images/icon-social-twitter.png" alt="facebook"/></li>
        </ol>
      </div>
      <nav>
        <ol>
          <li><a href="casakm.php">Casa KM</a></li></li>
          <li><a href="produtos.php">Produtos</a></li>
          <li><a href="casaemdia.php">Casa em dia<a/></li>
          <li><a href="sustentabilidade.php">Sustentabilidade</a></li>
          <li>Downloads</li>
          <li><a href="recursos-humanos.php">Recursos Humanos</a></li></li>
          <li>Contato</li>
          <li>
            <div class='control' tabindex='1'>
              <div class='btn'></div>
              <i class='icon-search ion-search'></i>
            </div>
          </div>
          <i class='icon-close ion-ios-close-empty'></i>
          <div class='form'>
            <input class='input-search' placeholder='Digite a sua busca' type='text'>
          </div>
        </li>
      </ol>
    </nav>
  </div>
</div>
</header>