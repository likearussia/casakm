<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Casa KM - Da nossa casa, para sua casa.</title>
        <!-- Bootstrap -->
        <link href="dist/css/geral.css" rel="stylesheet">
        <link href="dist/css/interna.css" rel="stylesheet">
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <script src="dist/js/jquery.min.js" type="text/javascript"></script>
        <script src="dist/js/slick.min.js" type="text/javascript"></script>
        <script src="http://www.nba.com/.element/media/2.0/teamsites/hornets/assets/owl-slider/js/owl.carousel.js" type="text/javascript"></script>
        <script src="dist/js/jquery-ui.js" type="text/javascript"></script>
        <script src="https://use.typekit.net/bux4bzx.js"></script>
        <script>try{Typekit.load({ async: true });}catch(e){}</script>
    </head>
    <body>
        <?php include "header.php";?>
        
        <div id="content-banner">
            <img src="images/banner-institucional.jpg"/>
            <div id="nav-breadcrumb">
                <div class="centralizar">
                    <div class="step-indicator">
                        <a class="step completed" href="#">Home</a>
                        <a class="step step-azul" href="#">A Casa KM</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="centralizar institucional-top">
            <div id="content-left">
                <div id="menu-institucional">
                    <h2>A Casa KM</h2>
                    <?php include "menu-institucional.php";?>
                    
                </div>
            </div>
            <div id="content-right" class="institucional">
                <h2 id="titulo-institucional">História</h2>
                <p>
                    Somos uma empresa familiar, nosso negócio começou dentro de casa. Foram muitos os momentos de conversas e reuniões para, juntos, decidirmos como seria nossa empresa e como gostaríamos que nossos produtos fossem levados até a casa dos nossos clientes.<br/><br/>
                    Assim, em 1979 iniciamos nossas atividades sob o nome Sun Life, com a cera Tacolac. Ela foi o primeiro produto da Casa KM. Em 1983, mudamos o nome da empresa para Coquel Industria Química Ltda., e lançamos o lava-roupas Coquel. Sua receptividade pelo público foi tão positiva que entendemos nossa missão: levar à sua casa produtos que atendam às suas expectativas em inovação, qualidade e bem-estar. Em 1991, viemos para Paulínia, então sob o nome de K&M Industria Química Ltda. E aqui estamos desde então, e chegamos a ser Casa KM.<br/><br/>
                    <strong>Nossas linhas de produtos ganharam novos destaques que conquistaram sem próprio espaço no mercado.</strong>
                </p>
                <ul class="timeline">
                    <!-- Item 1 -->
                    <li>
                        <div class="direction-r">
                            <div class="flag-wrapper">
                                <span class="hexa"></span>
                                <span class="flag">Tacolac</span>
                                <span class="time-wrapper"><span class="time">1979</span></span>
                            </div>
                            <div class="desc">Lorem ipsum Nisi labore aute do aute culpa magna nulla voluptate exercitation deserunt proident.</div>
                        </div>
                    </li>
                    <!-- Item 2 -->
                    <li>
                        <div class="direction-l">
                            <div class="flag-wrapper">
                                <span class="hexa"></span>
                                <span class="flag">Coquel</span>
                                <span class="time-wrapper"><span class="time">1983</span></span>
                            </div>
                            <div class="desc">Lorem ipsum In ut sit in dolor nisi ex magna eu anim anim tempor dolore aliquip enim cupidatat laborum dolore.</div>
                        </div>
                    </li>
                    <!-- Item 3 -->
                    <li>
                        <div class="direction-r">
                            <div class="flag-wrapper">
                                <span class="hexa"></span>
                                <span class="flag">Tom Bom Liquido e Brilho Fácil</span>
                                <span class="time-wrapper"><span class="time">1994</span></span>
                            </div>
                            <div class="desc">Lorem ipsum Minim labore Ut cupidatat quis qui deserunt proident fugiat pariatur cillum cupidatat reprehenderit sit id dolor consectetur ut.</div>
                        </div>
                    </li>
                    <!-- Item 3 -->
                    <li>
                        <div class="direction-l">
                            <div class="flag-wrapper">
                                <span class="hexa"></span>
                                <span class="flag">Tacolac Super</span>
                                <span class="time-wrapper"><span class="time">1996</span></span>
                            </div>
                            <div class="desc">Lorem ipsum Minim labore Ut cupidatat quis qui deserunt proident fugiat pariatur cillum cupidatat reprehenderit sit id dolor consectetur ut.</div>
                        </div>
                    </li>
                    <!-- Item 3 -->
                    <li>
                        <div class="direction-r">
                            <div class="flag-wrapper">
                                <span class="hexa"></span>
                                <span class="flag">Limpa Vidros</span>
                                <span class="time-wrapper"><span class="time">1997</span></span>
                            </div>
                            <div class="desc">Lorem ipsum Minim labore Ut cupidatat quis qui deserunt proident fugiat pariatur cillum cupidatat reprehenderit sit id dolor consectetur ut.</div>
                        </div>
                    </li>
                    <!-- Item 3 -->
                    <li>
                        <div class="direction-l">
                            <div class="flag-wrapper">
                                <span class="hexa"></span>
                                <span class="flag">Amacitel e Removedor de Ceras</span>
                                <span class="time-wrapper"><span class="time">1998</span></span>
                            </div>
                            <div class="desc">Lorem ipsum Minim labore Ut cupidatat quis qui deserunt proident fugiat pariatur cillum cupidatat reprehenderit sit id dolor consectetur ut.</div>
                        </div>
                    </li>
                    <!-- Item 3 -->
                    <li>
                        <div class="direction-r">
                            <div class="flag-wrapper">
                                <span class="hexa"></span>
                                <span class="flag">Casa&Perfume</span>
                                <span class="time-wrapper"><span class="time">2001</span></span>
                            </div>
                            <div class="desc">Lorem ipsum Minim labore Ut cupidatat quis qui deserunt proident fugiat pariatur cillum cupidatat reprehenderit sit id dolor consectetur ut.</div>
                        </div>
                    </li>
                    <!-- Item 3 -->
                    <li>
                        <div class="direction-r">
                            <div class="flag-wrapper">
                                <span class="hexa"></span>
                                <span class="flag">Vida Macia Amaciante e Lava Roupas</span>
                                <span class="time-wrapper"><span class="time">2001</span></span>
                            </div>
                            <div class="desc">Lorem ipsum Minim labore Ut cupidatat quis qui deserunt proident fugiat pariatur cillum cupidatat reprehenderit sit id dolor consectetur ut.</div>
                        </div>
                    </li>
                    <!-- Item 3 -->
                    <li>
                        <div class="direction-l">
                            <div class="flag-wrapper">
                                <span class="hexa"></span>
                                <span class="flag">Tom Bom Creme</span>
                                <span class="time-wrapper"><span class="time">2002</span></span>
                            </div>
                            <div class="desc">Lorem ipsum Minim labore Ut cupidatat quis qui deserunt proident fugiat pariatur cillum cupidatat reprehenderit sit id dolor consectetur ut.</div>
                        </div>
                    </li>
                    <!-- Item 3 -->
                    <li>
                        <div class="direction-r">
                            <div class="flag-wrapper">
                                <span class="hexa"></span>
                                <span class="flag">Sabão em Pó Coquel Granulado</span>
                                <span class="time-wrapper"><span class="time">2004</span></span>
                            </div>
                            <div class="desc">Lorem ipsum Minim labore Ut cupidatat quis qui deserunt proident fugiat pariatur cillum cupidatat reprehenderit sit id dolor consectetur ut.</div>
                        </div>
                    </li>
                    <!-- Item 3 -->
                    <li>
                        <div class="direction-l">
                            <div class="flag-wrapper">
                                <span class="hexa"></span>
                                <span class="flag">MultiUso Gatilho</span>
                                <span class="time-wrapper"><span class="time">2010</span></span>
                            </div>
                            <div class="desc">Lorem ipsum Minim labore Ut cupidatat quis qui deserunt proident fugiat pariatur cillum cupidatat reprehenderit sit id dolor consectetur ut.</div>
                        </div>
                    </li>
                    <!-- Item 3 -->
                    <li>
                        <div class="direction-r">
                            <div class="flag-wrapper">
                                <span class="hexa"></span>
                                <span class="flag">Amacitel Super</span>
                                <span class="time-wrapper"><span class="time">2013</span></span>
                            </div>
                            <div class="desc">Lorem ipsum Minim labore Ut cupidatat quis qui deserunt proident fugiat pariatur cillum cupidatat reprehenderit sit id dolor consectetur ut.</div>
                        </div>
                    </li>
                    <!-- Item 3 -->
                    <li>
                        <div class="direction-l">
                            <div class="flag-wrapper">
                                <span class="hexa"></span>
                                <span class="flag">Lava Roupas Sumper Super</span>
                                <span class="time-wrapper"><span class="time">2016</span></span>
                            </div>
                            <div class="desc">Lorem ipsum Minim labore Ut cupidatat quis qui deserunt proident fugiat pariatur cillum cupidatat reprehenderit sit id dolor consectetur ut.</div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
        
        <?php include "footer-produtos.php";?>
        
        <script>
        $('.control').click( function(){
        $('body').addClass('mode-search');
        $('.input-search').focus();
        });
        $('.icon-close').click( function(){
        $('body').removeClass('mode-search');
        });
        // carousel
        $('.slider-for').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        fade: true,
        dots: true,
        asNavFor: '.slider-nav'
        });
        $('.slider-nav').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        asNavFor: '.slider-for',
        arrows: false,
        centerMode: true,
        focusOnSelect: true,
        autoplay: true,
        });
        // carousel
        $(document).ready(function() {
        $("#owl-demo").owlCarousel({
        
        // Most important owl features
        items: 4,
        itemsDesktop: [1199, 3],
        itemsDesktopSmall: [979, 3],
        itemsTablet: [768, 2],
        itemsMobile: [479, 1],
        
        //Autoplay
        autoPlay: false, //Set AutoPlay to 3 seconds
        stopOnHover: true,
        
        //Basic Speeds
        slideSpeed : 200,
        paginationSpeed : 800,
        rewindSpeed : 1000,
        
        // Navigation
        navigation : false,
        navigationText : ["prev","next"],
        rewindNav : true,
        scrollPerPage : false,
        
        //Pagination
        pagination : false,
        paginationNumbers: false,
        
        // CSS Styles
        baseClass : "owl-carousel",
        theme : "owl-theme",
        
        //Auto height
        autoHeight : false,
        
        //Mouse Events
        dragBeforeAnimFinish : true,
        mouseDrag : true,
        touchDrag : true,
        
        //Transitions
        transitionStyle : false,
        });
        });
        // menu
        $(function() {
        var Accordion = function(el, multiple) {
        this.el = el || {};
        this.multiple = multiple || false;
        // Variables privadas
        var links = this.el.find('.link');
        // Evento
        links.on('click', {el: this.el, multiple: this.multiple}, this.dropdown)
        }
        Accordion.prototype.dropdown = function(e) {
        var $el = e.data.el;
        $this = $(this),
        $next = $this.next();
        $next.slideToggle();
        $this.parent().toggleClass('open');
        if (!e.data.multiple) {
        $el.find('.submenu').not($next).slideUp().parent().removeClass('open');
        };
        }
        var accordion = new Accordion($('#accordion'), false);
        });
        </script>
    </body>