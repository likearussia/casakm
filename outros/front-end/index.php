<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Casa KM - Da nossa casa, para sua casa.</title>

   <!-- Bootstrap -->
    <link href="dist/css/geral.css" rel="stylesheet">
    <link href="dist/css/index.css" rel="stylesheet">
    <link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="http://www.nba.com/.element/media/2.0/teamsites/hornets/assets/owl-slider/css/owl.carousel.css">
    <link rel="stylesheet" href="http://www.nba.com/.element/media/2.0/teamsites/hornets/assets/owl-slider/css/owl.theme.css">
    <link rel="stylesheet" href="http://www.nba.com/.element/media/2.0/teamsites/hornets/assets/owl-slider/css/owl.transitions.css">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="dist/js/jquery.min.js" type="text/javascript"></script>
     <script src="dist/js/slick.min.js" type="text/javascript"></script>
<script src="http://www.nba.com/.element/media/2.0/teamsites/hornets/assets/owl-slider/js/owl.carousel.js" type="text/javascript"></script>

     <script src="dist/js/jquery-ui.js" type="text/javascript"></script>
     <script src="https://use.typekit.net/bux4bzx.js"></script>
      <script>try{Typekit.load({ async: true });}catch(e){}</script>

  </head>
  <body>
       <?php include "header.php";?>
   
<div id="content">
 <div class="container">
  
  <div class="block-media">
    <div class="carousel-images slider-nav">
      <div>
        <img src="images/banner-01.png" alt="1">
      </div>
      <div>
        <img src="images/banner-01.png" alt="2">
      </div>
      <div>
        <img src="images/banner-01.png" alt="3">
      </div>
    </div>
  </div><!-- /.block-media -->
       <div class="block-text">
    <div class="carousel-text slider-for">
      <div>
        <h2>asasasa</h2>
        <span>Saiba Mais</span>
      </div>
      <div>
        <h2>asasasa</h2>
        <span>Saiba Mais</span>
      </div>
      <div>
       <h2>asasasa</h2>
        <span>Saiba Mais</span>
      </div>
    </div>
  </div><!-- /.block-text -->
  
</div><!-- /.container -->
<div id="content-banner">
  <img src="images/banner-produtos.png"/>
</div>


<?php include "footer.php";?>


 




     <script>
       $('.control').click( function(){
  $('body').addClass('mode-search');
  $('.input-search').focus();
});

$('.icon-close').click( function(){
  $('body').removeClass('mode-search');
});
// carousel

$('.slider-for').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    fade: true,
    dots: true,
    asNavFor: '.slider-nav'

});
$('.slider-nav').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    asNavFor: '.slider-for',
    arrows: false,
    centerMode: true,
    focusOnSelect: true,
    autoplay: true,
});
// carousel
$(document).ready(function() {

  $("#owl-demo").owlCarousel({
    
    // Most important owl features
    items: 4,
    itemsDesktop: [1199, 3],
    itemsDesktopSmall: [979, 3],
    itemsTablet: [768, 2],
    itemsMobile: [479, 1],
   
    //Autoplay
    autoPlay: false, //Set AutoPlay to 3 seconds
    stopOnHover: true,
 
    //Basic Speeds
    slideSpeed : 200,
    paginationSpeed : 800,
    rewindSpeed : 1000,
 
    // Navigation
    navigation : false,
    navigationText : ["prev","next"],
    rewindNav : true,
    scrollPerPage : false,
 
    //Pagination
    pagination : false,
    paginationNumbers: false,
 
    // CSS Styles
    baseClass : "owl-carousel",
    theme : "owl-theme",
 
    //Auto height
    autoHeight : false,
 
    //Mouse Events
    dragBeforeAnimFinish : true,
    mouseDrag : true,
    touchDrag : true,
 
    //Transitions
    transitionStyle : false,

  });

});
     </script>

  </body>