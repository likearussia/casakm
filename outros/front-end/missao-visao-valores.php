<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Casa KM - Da nossa casa, para sua casa.</title>
        <!-- Bootstrap -->
        <link href="dist/css/geral.css" rel="stylesheet">
        <link href="dist/css/interna.css" rel="stylesheet">
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <script src="dist/js/jquery.min.js" type="text/javascript"></script>
        <script src="dist/js/slick.min.js" type="text/javascript"></script>
        <script src="http://www.nba.com/.element/media/2.0/teamsites/hornets/assets/owl-slider/js/owl.carousel.js" type="text/javascript"></script>
        <script src="dist/js/jquery-ui.js" type="text/javascript"></script>
        <script src="https://use.typekit.net/bux4bzx.js"></script>
        <script>try{Typekit.load({ async: true });}catch(e){}</script>
    </head>
    <body>
        <?php include "header.php";?>
        
        <div id="content-banner">
            <img src="images/banner-institucional.jpg"/>
            <div id="nav-breadcrumb">
                <div class="centralizar">
                    <div class="step-indicator">
                        <a class="step completed" href="#">Home</a>
                        <a class="step step-azul" href="#">A Casa KM</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="centralizar institucional-top">
            <div id="content-left">
                <div id="menu-institucional">
                    <h2>A Casa KM</h2>
                    <?php include "menu-institucional.php";?>
                    
                </div>
            </div>
            <div id="content-right" class="institucional">
                <h2 id="titulo-institucional">Missão, Visão e Valores</h2>
                <p>
                    <h3>MISSÃO</h3><br/>
                    Levar à sua casa produtos que atendam às suas expectativas em inovação, qualidade e bem-estar, com boas práticas na área de negócios, sustentabilidade e conduta ética.<br/><br/>
                    <h3>VISÃO</h3><br/>
                    Ser a referência no mercado interno, assumindo a liderança com reconhecimento e valorização de nossos produtos.<br/><br/>
                    <h3>VALORES</h3><br/>
                    <strong>Convicção</strong><br/>
                    Acreditamos no que somos, criamos, produzimos e divulgamos.<br/><br/>
                    <strong>Criatividade</strong><br/>
                    Buscamos permanentemente soluções e inovações para nosso negócio.<br/><br/>
                    <strong>Garra</strong><br/>
                    Somos persistentes e otimistas para alcançar o resultado desejado.<br/><br/>
                    <strong>Integridade</strong><br/>
                    Somos norteados pelas premissas e procedimentos da nossa empresa, por isso nossa conduta é sempre agir com respeito, responsabilidade e disciplina perante clientes internos e externos.<br/><br/>
                    <strong>Paixão</strong><br/>
                    Somos apaixonados por nosso trabalho, e isso se reflete em tudo o que fazemos para o mercado.<br/><br/>
                </p>
            </div>
        </div>
        
        <?php include "footer-produtos.php";?>
        
        <script>
        $('.control').click( function(){
        $('body').addClass('mode-search');
        $('.input-search').focus();
        });
        $('.icon-close').click( function(){
        $('body').removeClass('mode-search');
        });
        // carousel
        $('.slider-for').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        fade: true,
        dots: true,
        asNavFor: '.slider-nav'
        });
        $('.slider-nav').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        asNavFor: '.slider-for',
        arrows: false,
        centerMode: true,
        focusOnSelect: true,
        autoplay: true,
        });
        // carousel
        $(document).ready(function() {
        $("#owl-demo").owlCarousel({
        
        // Most important owl features
        items: 4,
        itemsDesktop: [1199, 3],
        itemsDesktopSmall: [979, 3],
        itemsTablet: [768, 2],
        itemsMobile: [479, 1],
        
        //Autoplay
        autoPlay: false, //Set AutoPlay to 3 seconds
        stopOnHover: true,
        
        //Basic Speeds
        slideSpeed : 200,
        paginationSpeed : 800,
        rewindSpeed : 1000,
        
        // Navigation
        navigation : false,
        navigationText : ["prev","next"],
        rewindNav : true,
        scrollPerPage : false,
        
        //Pagination
        pagination : false,
        paginationNumbers: false,
        
        // CSS Styles
        baseClass : "owl-carousel",
        theme : "owl-theme",
        
        //Auto height
        autoHeight : false,
        
        //Mouse Events
        dragBeforeAnimFinish : true,
        mouseDrag : true,
        touchDrag : true,
        
        //Transitions
        transitionStyle : false,
        });
        });
        // menu
        $(function() {
        var Accordion = function(el, multiple) {
        this.el = el || {};
        this.multiple = multiple || false;
        // Variables privadas
        var links = this.el.find('.link');
        // Evento
        links.on('click', {el: this.el, multiple: this.multiple}, this.dropdown)
        }
        Accordion.prototype.dropdown = function(e) {
        var $el = e.data.el;
        $this = $(this),
        $next = $this.next();
        $next.slideToggle();
        $this.parent().toggleClass('open');
        if (!e.data.multiple) {
        $el.find('.submenu').not($next).slideUp().parent().removeClass('open');
        };
        }
        var accordion = new Accordion($('#accordion'), false);
        });
        </script>
    </body>