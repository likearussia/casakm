<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Casa KM - Da nossa casa, para sua casa.</title>
    <!-- Bootstrap -->
    <link href="dist/css/geral.css" rel="stylesheet">
    <link href="dist/css/interna.css" rel="stylesheet">
    <link rel="stylesheet" href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="http://www.nba.com/.element/media/2.0/teamsites/hornets/assets/owl-slider/css/owl.carousel.css">
    <link rel="stylesheet" href="http://www.nba.com/.element/media/2.0/teamsites/hornets/assets/owl-slider/css/owl.theme.css">
    <link rel="stylesheet" href="http://www.nba.com/.element/media/2.0/teamsites/hornets/assets/owl-slider/css/owl.transitions.css">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="dist/js/jquery.min.js" type="text/javascript"></script>
    <script src="dist/js/slick.min.js" type="text/javascript"></script>
    <script src="http://www.nba.com/.element/media/2.0/teamsites/hornets/assets/owl-slider/js/owl.carousel.js" type="text/javascript"></script>
    <script src="dist/js/jquery-ui.js" type="text/javascript"></script>
    <script src="https://use.typekit.net/bux4bzx.js"></script>
    <script>try{Typekit.load({ async: true });}catch(e){}</script>
  </head>
  <body>
    <?php include "header.php";?>
    
    <div id="content-banner">
      <img src="images/banner-02.jpg"/>
      <div id="nav-breadcrumb">
        <div class="centralizar">
          <div class="step-indicator">
            <a class="step completed" href="#">Home</a>
            <a class="step completed" href="#">Produtos</a>
            <a class="step step-azul" href="#">Lava-Roupas Concentrado Sumper Super</a>
          </div>
        </div>
      </div>
    </div>
    <div id="titulo-produtos">
      <div id="titulo-centralizar">
        <h1>Lava-Roupas concentrado Sumper super</h1>
        <h2>A linha de produtos da Casa KM cresceu</h2>
      </div>
    </div>
    <article id="produtos-descricao">
      <div id="bg-content-left">
        <div id="content-left-centralizar">
          <p><strong>Limpeza profunda com perfume delicado... Da nossa casa para sua casa.</strong><br/>
          Sumper Super é o lava-roupas perfeito para quem não abre mão da limpeza, do perfume, da praticidade e economia. Sua fórmula exclusiva remove as manchas mais difíceis e seus agentes biodegradáveis protegem os tecidos e reavivam as cores. E por ser concentrado, rende mundo mais.</p>
          <h2>Características do produto</h2>
          <ol>
            <li>Concentrado – rende mais</li>
            <li>Rendimento de 30 lavagens</li>
            <li>Dupla ação: limpa e perfuma as roupas</li>
            <li>Facilita a remoção de manchas</li>
            <li>Embalagem transparente, para realmente ver o produto</li>
            <li>Ergonômico e fácil de manusear</li>
            <li>Tampa dosadora: lavagem na medida certa, sem desperdício</li>
          </ol>
          <ul id="accordion" class="accordion">
            <li>
              <div class="link"></i>Informações de uso<i class="fa fa-chevron-down"></i></div>
              <ul class="submenu">
                <li><a href="#">Sala</a></li>
                <li><a href="#">Quarto</a></li>
                <li><a href="#">Cozinha</a></li>
                <li><a href="#">Banheiro</a></li>
                <li><a href="#">Lavanderia</a></li>
              </ul>
            </li>
            <li class="default open">
              <div class="link"></i>Superfícies<i class="fa fa-chevron-down"></i></div>
              <ul class="submenu">
                <li><strong>CONSERVE FORA DO ALCANCE DAS CRIANÇAS E DOS ANIMAIS DOMÉSTICOS.</strong><br/>
                  Não ingerir. Evite inalação ou aspiração, contato com os olhos e contato com a pele. Depois de utilizar esse produto, lave e seque as mãos. Em caso de contato com os olhos e a pele, lave imediatamente com água em abundância. Em caso de ingestão, não provoque vômito e consulte imediatamente o Centro de Intoxicações ou o médico levando o rótulo do produto.<br/>
                  <strong>Telefone de Emergência Médica (19) 3521 6700/ 3521 7555.</strong><br/>
                Não reutilize a embalagem vazia.</li>
              </ul>
            </li>
            <li>
              <div class="link">Marcas<i class="fa fa-chevron-down"></i></div>
              <ul class="submenu">
                <li><a href="#">Casa & Perfume</a></li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
      <div id="bg-content-right">
        <section class="right-box">
          <h2>Poder ser usado:</h2>
          <ol>
            <li>asassa</li>
            <li>asassa</li>
            <li>asassa</li>
          </ol>
        </section>
        <section class="right-box">
          <h2>Disponivel em</h2>
        </section>
        <section class="right-box">
          <h2>Compartilhar</h2>
          
        </section>
      </div>
    </article>
    <div id="produtos-relacionados">
      <h2>Produtos similares</h2>
      <ul>
        <li>
          <h1>Casa & Perfume</h1>
          <div class="produto-imagem">
            <img src="images/produto-01.png" alt="casaperfume"/>
          </div>
          <i class="hovicon effect-6">+</i>
        </li>
        <li>
          <h1>Casa & Perfume</h1>
          <div class="produto-imagem">
            <img src="images/produto-01.png" alt="casaperfume"/>
          </div>
          <i class="hovicon effect-6">+</i>
        </li>
        <li><h1>Casa & Perfume</h1>
          <div class="produto-imagem">
            <img src="images/produto-01.png" alt="casaperfume"/>
          </div>
        <i class="hovicon effect-6">+</i></li>
        <li><h1>Casa & Perfume</h1>
          <div class="produto-imagem">
            <img src="images/produto-01.png" alt="casaperfume"/>
          </div>
        <i class="hovicon effect-6">+</i></li>
      </ul>
    </div>
    <?php include "footer.php";?>
    
    <script>
    $('.control').click( function(){
    $('body').addClass('mode-search');
    $('.input-search').focus();
    });
    $('.icon-close').click( function(){
    $('body').removeClass('mode-search');
    });
    // carousel
    $('.slider-for').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    fade: true,
    dots: true,
    asNavFor: '.slider-nav'
    });
    $('.slider-nav').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    asNavFor: '.slider-for',
    arrows: false,
    centerMode: true,
    focusOnSelect: true,
    autoplay: true,
    });
    // carousel
    $(document).ready(function() {
    $("#owl-demo").owlCarousel({
    
    // Most important owl features
    items: 4,
    itemsDesktop: [1199, 3],
    itemsDesktopSmall: [979, 3],
    itemsTablet: [768, 2],
    itemsMobile: [479, 1],
    
    //Autoplay
    autoPlay: false, //Set AutoPlay to 3 seconds
    stopOnHover: true,
    
    //Basic Speeds
    slideSpeed : 200,
    paginationSpeed : 800,
    rewindSpeed : 1000,
    
    // Navigation
    navigation : false,
    navigationText : ["prev","next"],
    rewindNav : true,
    scrollPerPage : false,
    
    //Pagination
    pagination : false,
    paginationNumbers: false,
    
    // CSS Styles
    baseClass : "owl-carousel",
    theme : "owl-theme",
    
    //Auto height
    autoHeight : false,
    
    //Mouse Events
    dragBeforeAnimFinish : true,
    mouseDrag : true,
    touchDrag : true,
    
    //Transitions
    transitionStyle : false,
    });
    });
    // menu
    $(function() {
    var Accordion = function(el, multiple) {
    this.el = el || {};
    this.multiple = multiple || false;
    // Variables privadas
    var links = this.el.find('.link');
    // Evento
    links.on('click', {el: this.el, multiple: this.multiple}, this.dropdown)
    }
    Accordion.prototype.dropdown = function(e) {
    var $el = e.data.el;
    $this = $(this),
    $next = $this.next();
    $next.slideToggle();
    $this.parent().toggleClass('open');
    if (!e.data.multiple) {
    $el.find('.submenu').not($next).slideUp().parent().removeClass('open');
    };
    }
    var accordion = new Accordion($('#accordion'), false);
    });
    </script>
  </body>