<!DOCTYPE html>
<html lang="pt-br">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Casa KM - Da nossa casa, para sua casa.</title>
    <!-- Bootstrap -->
    <link href="dist/css/geral.css" rel="stylesheet">
    <link href="dist/css/interna.css" rel="stylesheet">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="dist/js/jquery.min.js" type="text/javascript"></script>
    <script src="dist/js/slick.min.js" type="text/javascript"></script>
    <script src="http://www.nba.com/.element/media/2.0/teamsites/hornets/assets/owl-slider/js/owl.carousel.js" type="text/javascript"></script>
    <script src="dist/js/jquery-ui.js" type="text/javascript"></script>
    <script src="https://use.typekit.net/bux4bzx.js"></script>
    <script>try{Typekit.load({ async: true });}catch(e){}</script>
  </head>
  <body>
    <?php include "header.php";?>
    
    <div id="content-banner">
      <img src="images/banner-sustentabilidade.jpg"/>
      <div id="nav-breadcrumb">
        <div class="centralizar">
          <div class="step-indicator">
            <a class="step completed" href="#">Home</a>
            <a class="step step-green" href="#">Sustentabilidade</a>
          </div>
        </div>
      </div>
    </div>
    <div id="content-interna">
        <div class="centralizar">
            <p>
                Sustentabilidade empresarial é um conjunto de ações que uma empresa toma, visando o respeito ao meio ambiente e o desenvolvimento sustentável da sociedade. Logo, para que uma empresa seja considerada sustentável ambientalmente e socialmente, ela deve adotar atitudes éticas, práticas que visem seu crescimento econômico, sem agredir o meio ambiente e também colaborar para o desenvolvimento da sociedade.
Há alguns anos a Casa KM tem adotado práticas sustentáveis para o meio ambiente e a sociedade ao seu entorno.<br/><br/>
Todo o efluente gerado na empresa (industrial e sanitário) é tratado em empresas contratadas por nós, e que seguem rigorosamente as exigências legais de transporte e descarte. Nenhum efluente é descartado no meio ambiente, ou na rede de esgoto.
O cuidado com os resíduos sólidos são os mesmos. Papelão, plásticos, vidro, metal, etc., são destinados a uma cooperativa, também homologada pelos órgãos competentes, e com todas as licenças necessárias.<br/><br/>
O descarte de embalagens pós-consumo é uma questão global preocupante, principalmente nas grandes metrópoles. Em busca da destinação adequada, em 2010, a Casa KM passou a participar do projeto <strong>“Dê a Mão para o Futuro”</strong> - Reciclagem, Trabalho e Renda, uma parceria com a Abipla - Associação Brasileira das Indústrias de Produtos de Limpeza e Afins, da qual a Casa KM é uma associada.<br/> <br/> 
Temos previsão para investimentos em uma estação de tratamento químico e sanitário, além de aquisição de novos compressores para economia de energia elétrica.

            </p>
            <img src="images/logo-sustentavel.png" alt="sustentabilidade" id="image-sutentabilidade"/>
        </div>

    </div>  
     <div class="centralizar">
            <h2 class="green">Em resumo, a Casa KM pratica a sustentabilidade com:</h2>
            <div id="box-sustentabilidade">
                <ul>
                <li><img src="images/icon-sustentavel-01.png" alt="sustentabilidade"/><div class="box-texto">Uso de sistemas de tratamento e reaproveitamento da água.</div></li>
                <li><img src="images/icon-sustentavel-04.png" alt="sustentabilidade"/><div class="box-texto">Reutilização de sobras de matéria-prima.</div></li>
                <li><img src="images/icon-sustentavel-09.png" alt="sustentabilidade"/><div class="box-texto">Não há descarte de esgoto ou resíduos químicos em rios, córregos ou lagos.</div></li>
                <li><img src="images/icon-sustentavel-02.png" alt="sustentabilidade"/><div class="box-texto">Uso racional da água e da energia elétrica.</div></li>
                <li><img src="images/icon-sustentavel-06.png" alt="sustentabilidade"/><div class="box-texto">Uso de materiais recicláveis para a confecção de embalagens dos produtos.</div></li>
                <li><img src="images/icon-sustentavel-08.png" alt="sustentabilidade"/><div class="box-texto">Uso de filtros que retém os poluentes emitidos em determinadas fases da produção industrial.</div></li>
                <li><img src="images/icon-sustentavel-06.png" alt="sustentabilidade"/><div class="box-texto">Reciclagem do lixo sólido.</div></li>
                <li><img src="images/icon-sustentavel-05.png" alt="sustentabilidade"/><div class="box-texto">Respeito total às leis ambientais do país.</div></li>
                <li><img src="images/icon-sustentavel-03.png" alt="sustentabilidade"/><div class="box-texto">Respeito total às leis ambientais do país.</div></li>
            </ul>
            </div>
        </div>
</body>
   
    <?php include "footer-produtos.php";?>
    
    <script>
    $('.control').click( function(){
    $('body').addClass('mode-search');
    $('.input-search').focus();
    });
    $('.icon-close').click( function(){
    $('body').removeClass('mode-search');
    });
    // carousel
    $('.slider-for').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    fade: true,
    dots: true,
    asNavFor: '.slider-nav'
    });
    $('.slider-nav').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    asNavFor: '.slider-for',
    arrows: false,
    centerMode: true,
    focusOnSelect: true,
    autoplay: true,
    });
    // carousel
    $(document).ready(function() {
    $("#owl-demo").owlCarousel({
    
    // Most important owl features
    items: 4,
    itemsDesktop: [1199, 3],
    itemsDesktopSmall: [979, 3],
    itemsTablet: [768, 2],
    itemsMobile: [479, 1],
    
    //Autoplay
    autoPlay: false, //Set AutoPlay to 3 seconds
    stopOnHover: true,
    
    //Basic Speeds
    slideSpeed : 200,
    paginationSpeed : 800,
    rewindSpeed : 1000,
    
    // Navigation
    navigation : false,
    navigationText : ["prev","next"],
    rewindNav : true,
    scrollPerPage : false,
    
    //Pagination
    pagination : false,
    paginationNumbers: false,
    
    // CSS Styles
    baseClass : "owl-carousel",
    theme : "owl-theme",
    
    //Auto height
    autoHeight : false,
    
    //Mouse Events
    dragBeforeAnimFinish : true,
    mouseDrag : true,
    touchDrag : true,
    
    //Transitions
    transitionStyle : false,
    });
    });
    // menu
    $(function() {
    var Accordion = function(el, multiple) {
    this.el = el || {};
    this.multiple = multiple || false;
    // Variables privadas
    var links = this.el.find('.link');
    // Evento
    links.on('click', {el: this.el, multiple: this.multiple}, this.dropdown)
    }
    Accordion.prototype.dropdown = function(e) {
    var $el = e.data.el;
    $this = $(this),
    $next = $this.next();
    $next.slideToggle();
    $this.parent().toggleClass('open');
    if (!e.data.multiple) {
    $el.find('.submenu').not($next).slideUp().parent().removeClass('open');
    };
    }
    var accordion = new Accordion($('#accordion'), false);
    });
    </script>
  </body>